<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Global Stokçunuz</title>




    {!! HTML::script('js/jquery-1.11.1.min.js') !!}



    <!-- Latest compiled and minified CSS -->

    {!! HTML::style('css/all.css'); !!}

    {!! HTML::style('css/style.css'); !!}

    {!! Html::style('font-awesome/css/font-awesome.css') !!}

    {!! Html::style('css/owl.carousel.css') !!}

    <!-- Google Web Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Oswald:400,700,300" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400" rel="stylesheet" type="text/css">

    {!! HTML::style('css/magnific-popup.css')!!}
    {!! HTML::style('css/responsive.css')!!}
    {!! HTML::style('admin_assets/bower_components/bootstrap-rating/css/star-rating.css') !!}

    <!--[if lt IE 9]>
    {!! Html::script('js/ie8-responsive-file-warning.js') !!}
    <![endif]-->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    {!! HTML::style('admin_assets/bower_components/summernote-master/dist/summernote.css')!!}

    {!! HTML::style('css/datepicker.css')!!}

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" media="screen">


    <![endif]-->




</head>
<body>
    @include('partials.header')
<div id="container">
    @yield('content')
</div>

    {!! HTML::script('js/jquery-migrate-1.2.1.min.js') !!}

    {!! HTML::script('js/owl.carousel.min.js') !!}

    {!! HTML::script('js/bootstrap.js') !!}

    {!! HTML::script('js/custom.js') !!}

    {!! HTML::script('js/jquery.magnific-popup.min.js') !!}

    {!! HTML::script('js/jquery.cslider.js') !!}

    {!! HTML::script('js/modernizr.custom.28468.js') !!}

    {!! HTML::script('admin_assets/bower_components/summernote-master/dist/summernote.js')!!}

    {!! HTML::script('admin_assets/bower_components/summernote-master/lang/summernote-tr-TR.js')!!}

    {!! HTML::script('js/quantity.js') !!}

    {!! HTML::script('admin_assets/bower_components/bootstrap-rating/js/star-rating.min.js') !!}

    {!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.min.js')!!}

    {!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/locales/bootstrap-datepicker.tr.min.js')!!}

    {!! HTML::script('//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js')!!}

    <!--Select2-->
    {!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js')!!}

    <script>
        $(function(){
           //alış tutarı hesapla productfull.blade.php

            $(document).on('change','input#quant',function(){

                var fiyat = $(this).attr('data-tutar');

                var tutar = $(this).val() * fiyat;

                $('span#tutar').html(tutar);

                $('input#odenecek').val(tutar);
            });

            //rating productfull.blade

            $('#rating-input').rating({
                min: 0,
                max: 5,
                step: 1,
                size: 'xs',
                showClear: true,
                starCaptions: {
                    0.5: '0.5 Yıldız',
                    1: '1 Yıldız',
                    1.5: '1.5 Yıldız',
                    2: '2 Yıldız',
                    2.5: '2.5 Yıldız',
                    3: '3 Yıldız',
                    3.5: '3.5 Yıldız',
                    4: '4 Yıldız',
                    4.5: '4.5 Yıldız',
                    5: '5 Yıldız'
                },
                clearCaption: 'Oylanmadı'
            });


            $(document).on('change','select.city',function(){

                var city_code = $(this).val();

                var town_container ='<option value="0">MERKEZ</option>';

                $.ajax({
                           type: 'GET',
                           url: '/fpage/citytown',
                           data: {city_code:city_code},
                           success: function (msg) {

                               $.each($.parseJSON(msg), function(idx, obj) {
                                   town_container +='<option value="'+ obj.id+'">'+ obj.name+'</option>';
                               });

                               $("select[name='city_town']").html(town_container);

                           }
                            });
            });

            $('.summernote').summernote({
                focus: true,                // set focus to editable area after initializing summernote
                height:300,
                lang: 'tr-TR',
                onKeyup: function(e) {
                    $('textarea#msg').html($(".summernote").code());
                }
            });

            $('textarea#imalatci_profil').summernote({
                focus: true,                // set focus to editable area after initializing summernote
                height:300,
                lang: 'tr-TR',
            });

            $('textarea.note-codable').css('display','none');


            $('#dpYears').datepicker({
                format:'dd/mm/yyyy',
                language:'tr'
            });

            $('#dpYears2').datepicker({
                format:'dd/mm/yyyy',
                language:'tr'
            });

            function toggleChevron(e) {
                $(e.target)
                        .prev('.panel-heading')
                        .find("i.indicator")
                        .toggleClass('fa fa-chevron-down fa fa-chevron-up');
            }
            $('#accordion').on('hidden.bs.collapse', toggleChevron);
            $('#accordion').on('shown.bs.collapse', toggleChevron);

            $(".fancybox").fancybox({
                openEffect: "none",
                closeEffect: "none",
                closeBtn: true,
                modal:true
            });


            $('span.dogrulama_span').mouseover(function(){
                var id = $(this).attr('data-user');
                $('div#dogrulama_'+id).css('display','block');

            }).mouseout(function(){
                var id = $(this).attr('data-user');
                $('div#dogrulama_'+id).css('display','none');
            });


            $('select#service_category,select#service_subcategory').select2({width:'100%'});


            $(document).on('change','select#service_category',function(){

                var service_category_id = $(this).val();

                $('div#service_sub').html('<img src="/images/AjaxLoader.gif" style="max-width=80px;max-height=80px"/>');

                $.ajax({
                    type: 'GET',
                    url: '/fpage/servicesubcategory',
                    data: {service_category_id :service_category_id },
                    success: function (msg) {
                        $('div#service_sub').html(msg);
                    }
                });
            });


        });
    </script>


    @include('partials.footer')

</body>
</html>