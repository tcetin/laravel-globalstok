<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Global Stokçu Yönetim Paneli</title>

    <!-- Bootstrap CSS -->
    {!! HTML::style('metro_admin/css/bootstrap.min.css') !!}
    <!-- jQuery UI -->
    {!! HTML::style('metro_admin/css/jquery-ui.css') !!}
    <!-- Font awesome CSS -->
    {!! HTML::style('metro_admin/css/font-awesome.min.css') !!}
    <!-- Custom CSS -->
    {!! HTML::style('metro_admin/css/style.css') !!}
    <!-- Widgets stylesheet -->
    {!! HTML::style('metro_admin/css/widgets.css') !!}

    {!! HTML::style('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css') !!}

    <!--file input plugin-->
    {!! HTML::style('admin_assets/bootstrap-fileinput-master/css/fileinput.css') !!}

    <!--Fancybox-->

    {!! HTML::style('admin_assets/fancybox/source/jquery.fancybox.css') !!}

    <!--datatables css-->
    {!! HTML::style('admin_assets/bower_components/datatables/media/css/jquery.dataTables.min.css') !!}

    <!--fresh bootstrap table-->

    {!! HTML::style('admin_assets/bower_components/bootstrap_table/dist/bootstrap-table.min.css') !!}

</head>

<body>

        @include('partials.admin.navbar_toplinks')
        <!-- /.navbar-top-links -->

        <!--side bar-->
        @include('partials.admin.sidebar')
        <!--/ sidebar-->
        <!-- /.navbar-static-side -->

    <!-- Page Content -->
    <div class="mainbar">
            @yield('content')

    </div>

<!-- jQuery -->
{!! HTML::script('admin_assets/bower_components/jquery/dist/jquery.min.js')!!}

<!-- Javascript files -->
<!-- jQuery -->
{!! HTML::script('metro_admin/js/jquery.js')!!}
<!-- Bootstrap JS -->
{!! HTML::script('metro_admin/js/bootstrap.min.js')!!}
<!-- jQuery UI -->
{!! HTML::script('metro_admin/js/jquery-ui.min.js')!!}
<!-- jQuery Flot -->
{!! HTML::script('metro_admin/js/excanvas.min.js')!!}

{!! HTML::script('metro_admin/js/sparklines.js')!!}
<!-- Respond JS for IE8 -->
{!! HTML::script('metro_admin/js/respond.min.js')!!}
<!-- HTML5 Support for IE -->
{!! HTML::script('metro_admin/js/html5shiv.js')!!}
<!-- Custom JS -->
{!! HTML::script('metro_admin/js/custom.js')!!}

<!-- Script for this page -->
{!! HTML::script('metro_admin/js/sparkline-index.js')!!}
<!--Select2-->
{!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js')!!}

<!--Fancybox-->
{!! HTML::script('admin_assets/fancybox/source/jquery.fancybox.js') !!}

<!--file input plg-->
{!! HTML::script('admin_assets/bootstrap-fileinput-master/js/plugins/canvas-to-blob.js')!!}
{!! HTML::script('admin_assets/bootstrap-fileinput-master/js/fileinput.js')!!}
{!! HTML::script('admin_assets/bootstrap-fileinput-master/js/fileinput_locale_tr.js')!!}


<!--datatables js-->
{!! HTML::script('admin_assets/bower_components/datatables/media/js/jquery.dataTables.min.js')!!}


<!-- Mask Money-->
{!! HTML::script('admin_assets/bower_components/jquery-maskmoney-master/dist/jquery.maskMoney.min.js')!!}

{!! HTML::script('https://code.highcharts.com/highcharts.js')!!}
{!! HTML::script('https://code.highcharts.com/modules/exporting.js')!!}
{!! HTML::script('https://code.highcharts.com/modules/drilldown.js')!!}

<!--Bootstrap table-->
{!! HTML::script('admin_assets/bower_components/bootstrap_table/dist/bootstrap-table.js')!!}
{!! HTML::script('admin_assets/bower_components/bootstrap_table/dist/locale/bootstrap-table-tr-TR.min.js')!!}
{!! HTML::script('admin_assets/bower_components/bootstrap_table/src/extensions/export/bootstrap-table-export.js')!!}
{!! HTML::script('//rawgit.com/hhurz/tableExport.jquery.plugin/master/tableExport.js')!!}

            <script type="text/javascript">
            var $table = $('table#imalatcilar,table#service'),
                    $alertBtn = $('#alertBtn'),
                    full_screen = false,
                    window_height;

            $().ready(function(){

                window_height = $(window).height();
                table_height = window_height - 20;


                $table.bootstrapTable({
                    toolbar: ".toolbar",

                    showRefresh: true,
                    search: true,
                    showToggle: true,
                    showExport:true,
                    showColumns: true,
                    pagination: true,
                    striped: true,
                    sortable: true,
                    height: table_height,
                    pageSize: 25,
                    pageList: [25,50,100],

                    formatShowingRows: function(pageFrom, pageTo, totalRows){
                        //do nothing here, we don't want to show the text "showing x of y from..."
                    },
                    formatRecordsPerPage: function(pageNumber){
                        return pageNumber + " rows visible";
                    },
                    icons: {
                        refresh: 'fa fa-refresh',
                        toggle: 'fa fa-th-list',
                        columns: 'fa fa-columns',
                        detailOpen: 'fa fa-plus-circle',
                        detailClose: 'fa fa-minus-circle'
                    }
                });



                $(window).resize(function () {
                    $table.bootstrapTable('resetView');
                });
            });


            function operateFormatter(value, row, index) {
                return [
                    '<a rel="tooltip" title="Like" class="table-action like" href="javascript:void(0)" title="Like">',
                    '<i class="fa fa-heart"></i>',
                    '</a>',
                    '<a rel="tooltip" title="Edit" class="table-action edit" href="javascript:void(0)" title="Edit">',
                    '<i class="fa fa-edit"></i>',
                    '</a>',
                    '<a rel="tooltip" title="Remove" class="table-action remove" href="javascript:void(0)" title="Remove">',
                    '<i class="fa fa-remove"></i>',
                    '</a>'
                ].join('');
            }

        </script>


        <script>
            $(function(){

               $('table#tedarikciler').dataTable({
                language :{
                    "sInfo": "Toplam _TOTAL_ kayıttan  _START_  ile  _END_  arası kayıt gösteriliyor.",
                            "infoEmpty": "Herhangi bir kayıt bulunamadı.",
                            "sSearch" : "Ara:",
                            "sZeroRecords": "Herhangi bir kayıt bulunamadı.",
                            "paginate": {
                        "first":      "İlk Kayıt",
                                "last":       "Son kayıt",
                                "next":       "Sonraki",
                                "previous":   "Önceki"
                    },
                    "sLengthMenu": " _MENU_ "

                }

            });


                $(document).on('click','button.resim_onayla',function(){
                    var element = $(this);
                    var resim_id = $(this).attr('data-resim');
                    var response_div = $('div[data-resim="'+$(this).attr('data-resim')+'"]');
                    response_div.html('<img src="/images/ajax-loader.gif" style="max-width=80px;max-height=80px"/>');
                    $.ajax({
                        type: 'GET',
                        url: '/admin/verifyimage',
                        data: {id: resim_id},
                        success: function (msg) {
                            response_div.html(msg);

                            element.removeClass('btn btn-success btn-xs resim_onayla');
                            element.addClass('btn btn-danger btn-xs resim_onay_kaldir');

                            element.text("Onayı Kaldır");

                            response_div.removeClass('alert alert-danger');
                            response_div.addClass('alert alert-success');
                        }
                    });
                });




                $(document).on('click','button.resim_onay_kaldir',function(){
                    var element = $(this);
                    var resim_id = $(this).attr('data-resim');
                    var response_div = $('div[data-resim="'+$(this).attr('data-resim')+'"]');
                    response_div.html('<img src="/images/ajax-loader.gif" style="max-width=80px;max-height=80px"/>');
                    $.ajax({
                        type: 'GET',
                        url: '/admin/unverifyimage',
                        data: {id: resim_id},
                        success: function (msg) {
                            response_div.html(msg);

                            element.removeClass('btn btn-danger btn-xs resim_onay_kaldir');
                            element.addClass('btn btn-success btn-xs resim_onayla');

                            element.text("Onayla");

                            response_div.removeClass('alert alert-success');
                            response_div.addClass('alert alert-danger');
                        }
                    });
                });


                $(document).on('click','button.delete_prop', function () {

                    var prop_id = $(this).attr('data-prop');

                    var responseDiv = $('p#prop_del_'+prop_id);

                    responseDiv.html('<img src="/images/AjaxLoader.gif" style="max-width=80px;max-height=80px;margin-left: 200px"/>');

                    $.ajax({
                        type: 'GET',
                        url: '/admin/subcategorypropdelete',
                        data: {id: prop_id},
                        success: function (msg) {
                            responseDiv.html(msg);

                            setTimeout(function(){window.location.reload()},2000)
                        }
                    });

                });

                $(document).on('click','button.edit_prop', function () {

                    var prop_id = $(this).attr('data-prop');

                    var responseDiv = $('p#edit_prop_'+prop_id);

                    var name = $('input#edittext_'+prop_id).val();


                    responseDiv.html('<img src="/images/AjaxLoader.gif" style="max-width=80px;max-height=80px;margin-left: 200px"/>');

                    $.ajax({
                        type: 'GET',
                        url: '/admin/subcategorypropedit',
                        data: {id:prop_id,name:name},
                        success: function (msg) {
                            responseDiv.html(msg);
                            setTimeout(function(){window.location.reload()},2000)
                        }
                    });

                });






            });
        </script>



</body>

</html>
