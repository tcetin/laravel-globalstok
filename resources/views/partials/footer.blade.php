<!-- Footer Section Starts -->
<footer id="footer-area">
    <!-- Footer Links Starts -->
    <div class="footer-links">
        <!-- Container Starts -->
        <div class="container">
            <!-- Information Links Starts -->
            <div class="col-md-2 col-sm-6">
                <h5>Kurumsal</h5>
                <ul>
                    <li><a href="about.html">Hakkımızda</a></li>
                    <li><a href="#">Gizlilik Politikası</a></li>
                    <li><a href="#">Kullanım Koşulları</a></li>
                    <li><a href="#">Destek</a></li>
                    <li><a href="#">İletişim</a></li>
                </ul>
            </div>
            <!-- Information Links Ends -->
            <!-- My Account Links Starts -->
            <div class="col-md-2 col-sm-6">
                <h5>Hizmetlerimiz</h5>
                <ul>
                    <li><a href="#">Toptan Ürün İlanı</a></li>
                    <li><a href="#">Reklam</a></li>
                    <li><a href="#">Tasarım</a></li>
                </ul>
            </div>
            <!-- My Account Links Ends -->
            <!-- Customer Service Links Starts -->
            <div class="col-md-2 col-sm-6">
                <h5>Mağazalar</h5>
                <ul>
                    @if(Auth::check() && Auth::user()->rol==2)
                     <li><a href="/firma/{{Auth::user()->id}}">Mağazam</a></li>
                    @else
                    <li><a href="/user/imalatcigiris">Mağazam</a></li>
                    @endif
                    <li><a href="#">Neden Mağaza?</a></li>
                    <li><a href="#">Nasıl Mağaza Açarım?</a></li>
                </ul>
            </div>
            <!-- Customer Service Links Ends -->
            <!-- Follow Us Links Starts -->
            <div class="col-md-2 col-sm-6">
                <h5>Bizi takip Edin</h5>
                <ul>
                    <li><a href="#">Facebook</a></li>
                    <li><a href="#">Twitter</a></li>
                    <li><a href="#">Linkedin</a></li>
                </ul>
            </div>
            <!-- Follow Us Links Ends -->
            <!-- Contact Us Starts -->
            <div class="col-md-4 col-sm-12 last">
                <h5>İletişim Bilgileri</h5>
                <ul>
                    <li><a href="http://globalstok.com ">GlobalStok.com </a></li>
                    <li>
                        Adalet Mh. 1594/1 Sk. No:17 Kızılata Sitesi C Blok Kat:1 D:3 Manavkuyu Bayraklı/İzmir
                    </li>
                    <li>
                        Eposta: <a href="mailto:globalstokdestek@gmail.com">globalstokdestek@gmail.com</a>
                    </li>
                </ul>
                <h4 class="lead">
                   <i class="fa fa-phone-square"></i> Müşteri Hizmetleri <br/><span>0(232) 422-0132</span>
                </h4>
            </div>
            <!-- Contact Us Ends -->
        </div>
        <!-- Container Ends -->
    </div>
    <!-- Footer Links Ends -->
    <!-- Copyright Area Starts -->
    <div class="copyright">
        <!-- Container Starts -->
        <div class="container">
            <!-- Starts -->
            <p class="pull-left">
                &copy;2016 Tüm hakları saklıdır.   <a href="http://globalstok.com ">GlobalStok.com </a>
            </p>
        </div>
        <!-- Container Ends -->
    </div>
    <!-- Copyright Area Ends -->
</footer>
<!-- Footer Section Ends -->