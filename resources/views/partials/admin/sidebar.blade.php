<div class="sidebar">
    <div class="sidebar-dropdown"><a href="#">Gezinim</a></div>
    <div class="sidebar-inner">
        <!-- Search form -->
        <br>
        <!--- Sidebar navigation -->
        <!-- If the main navigation has sub navigation, then add the class "has_submenu" to "li" of main navigation. -->
        <ul class="navi">

            <li class="nred">
                <a  href="{{ url('/') }}" target="_blank"><i class="fa fa-external-link fa-fw"></i> Web Anasayfa</a>
            </li>
            <li class="nred">
                <a  href="{{ url('/admin') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>

            <li class="nred">
                <a  href="{{ url('/admin/users') }}"><i class="fa fa-user fa-fw"></i> Kullanıcı İşlemleri</a>
            </li>

            <li class="nred">
                <a  href="{{ url('/admin/category') }}"><i class="fa fa-tasks fa-fw"></i> Kategori İşlemleri</a>
            </li>

            <li class="nred">
                <a  href="{{ url('/admin/servicecategory') }}"><i class="fa fa-tasks fa-fw"></i>Hizmet Kategorileri</a>
            </li>

            <li class="nred">
                <a  href="{{ url('/admin/products') }}"><i class="fa fa-tasks fa-fw"></i> Ürün İşlemleri</a>
            </li>
            <li class="nred">
                <a  href="{{ url('/admin/mainslider') }}"><i class="fa fa-cog fa-fw"></i> Ana Slider Yönetimi</a>
            </li>

        </ul>
    </div>
</div>