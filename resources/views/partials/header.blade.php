<!-- Header Section Starts -->
<header id="header-area" class="home">
    <!-- Header Top Starts -->
    <div class="header-top">
        <div class="container">
            <div class="row">

                <!-- Header Links Starts -->
                <div class="col-sm-12 col-xs-12">
                    <div class="header-links">


                        @if(Auth::check() && Auth::user()->rol==1)
                        <ul class="nav navbar-nav pull-right">
                            <li class="dropdown" >
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle">{{ Auth::user()->email }}<b class="caret"></b></a>
                                <ul class="dropdown-menu" style="background-color: #F0F0F0!important;">
                                    <li><a href="/admin" target="_blank" style="color: #967E4B !important;"> <i class="fa fa-sign-in fa-fw" title="Login"></i> Yönetici Paneli</a></li>
                                    <li><a href="/user/logout" style="color: #967E4B !important;"> <i class="fa fa-sign-in fa-fw" title="Login"></i> Çıkış Yap</a></li>
                                </ul>
                            </li>
                        </ul>
                        @endif

                        @if(Auth::check() && Auth::user()->rol==2 && Auth::user()->verified==1 && Auth::user()->active==1)
                                <ul class="nav navbar-nav pull-right">
                                    <li class="dropdown" >
                                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">{{ Auth::user()->email }}<b class="caret"></b></a>
                                        <ul class="dropdown-menu" style="background-color: #F0F0F0!important;">
                                            <li><a href="/imalatci" target="_blank" style="color: #967E4B !important;"> <i class="fa fa-sign-in fa-fw" title="Login"></i> İmalatçı Paneli</a></li>
                                            <li><a href="/user/logout" style="color: #967E4B !important;"> <i class="fa fa-sign-in fa-fw" title="Login"></i> Çıkış Yap</a></li>
                                        </ul>
                                    </li>
                                </ul>
                       @endif

                       @if(Auth::check() && Auth::user()->rol==3 && Auth::user()->verified==1 && Auth::user()->active==1)
                                <ul class="nav navbar-nav pull-right">
                                    <li class="dropdown" >
                                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">{{ Auth::user()->email }}<b class="caret"></b></a>
                                        <ul class="dropdown-menu" style="background-color: #F0F0F0!important;">
                                            <li><a href="/user/logout" style="color: #967E4B !important;"> <i class="fa fa-sign-in fa-fw" title="Login"></i> Çıkış Yap</a></li>
                                        </ul>
                                    </li>
                                </ul>
                       @endif

                       @if(!Auth::check())
                        <ul class="nav navbar-nav pull-right">
                            <li class="dropdown" >
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle">Giriş Yap <b class="caret"></b></a>
                                <ul class="dropdown-menu" style="background-color: #F0F0F0!important;">
                                    <li><a href="/user/imalatcigiris" style="color: #3498db !important;"> <i class="fa fa-sign-in fa-fw" title="Login"></i> İmalatçı Girişi </a></li>
                                    <li><a href="/user/perakendecigiris" style="color: #3498db !important;"> <i class="fa fa-sign-in fa-fw" title="Login"></i> Perakendeci Girişi</a></li>
                                    <li><a href="/user" style="color: #3498db !important;"> <i class="fa fa-sign-in fa-fw" title="Login"></i> Kullanıcı Girişi</a></li>
                                </ul>
                            </li>
                        </ul>
                        @endif


                        @if(!Auth::check())
                        <ul class="nav navbar-nav pull-right">
                            <li>
                                <a href="/register">
                                    <i class="fa fa-user fa-fw" title="Register"></i>
										<span class="hidden-sm hidden-xs">
											Kayıt Ol
										</span>
                                </a>
                            </li>
                        </ul>
                        @endif
                    </div>
                </div>
                <!-- Header Links Ends -->
            </div>
        </div>
    </div>
    <!-- Header Top Ends -->
    <!-- Main Header Starts -->
    <div class="main-header">
        <div class="container">
            <div class="row">
                <!-- Logo Starts -->
                <div class="col-sm-6">
                    <div id="logo">
                        Global Stok LOGO
                    </div>
                </div>
                <!-- Logo Starts -->
                 <!-- Search Starts -->
                <div class="col-sm-6">
                    <div id="search">
                        <div class="input-group">
                            <input type="text" class="form-control input-lg" placeholder="Arama Yapınız">
							  <span class="input-group-btn">
								<button class="btn btn-lg" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
							  </span>
                        </div>
                    </div>
                </div>
                <!-- Search Ends -->
            </div>
        </div>
    </div>
    <!-- Main Header Ends -->
    <!-- Main Menu Starts -->
     @include('partials.categories_menu')
    <!-- Main Menu Ends -->
</header>
<!-- Header Section Ends -->