<style>
    div.list-group-item .submenu{
        position: absolute;
        display: none;
        top:0;
        left: 100%;
        width:100%;
        z-index: 1;
    }

    div.list-group-item:hover .submenu{
        display:block;
        right:0;;
    }
</style>

<h3 class="side-heading">Kategoriler</h3>
<div class="list-group categories">

    @foreach(App\Category::all() as $category)

    <div class="list-group-item">
        <i class="fa fa-chevron-right"></i>
        <a href="/productlist/{{$category->id}}/1/1" target="_blank"> {{ $category->name }}</a>

        <div class="list-group submenu">
            @foreach(App\Category::find($category->id)->subcategories as $subcategory)
            <a href="/productsublist/{{$category->id}}/{{$subcategory->id}}/1/1" class="list-group-item" target="_blank">{{ $subcategory->name }}</a>
            @endforeach
        </div>

    </div>




    @endforeach

</div>