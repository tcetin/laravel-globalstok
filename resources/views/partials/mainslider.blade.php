@foreach(App\MainsliderStatu::all() as $statu)
    @if($statu->statu==1)
        <div id="da-slider" class="da-slider">

            @foreach(App\Mainslider::all() as $slide)
                @if($slide->state==1)
                    <div class="da-slide">
                        <h2>{{ $slide->baslik }}</h2>
                        <p>{{$slide->metin}}</p>
                        <a href="/anaslide/{{$slide->id}}" class="da-link">{{ $slide->button_text }}</a>
                        <div class="da-img">{!! HTML::image($slide->photo_path) !!}</div>
                    </div>
                @endif

            @endforeach
            <nav class="da-arrows">
                <span class="da-arrows-prev"></span>
                <span class="da-arrows-next"></span>
            </nav>
        </div>
        </div>
    @endif

@endforeach
