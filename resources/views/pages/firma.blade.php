@extends('app')

@section('content')
    <style>
        div.list-group-item .submenu{
            position: absolute;
            display: none;
            top:0;
            left: 100%;
            width:100%;
            z-index: 1;
        }

        div.list-group-item:hover .submenu{
            display:block;
            right:0;;
        }

    </style>

    <div id="main-container" class="container">
        <div class="row">
            <div class="col-md-12">
                @include('flash::message')
            </div>
        </div>
        <div class="row">
            <!-- Sidebar Starts -->
            <div class="col-md-3">
                <!-- Categories Links Starts -->

                <h3 class="side-heading">Firmanın Ürünleri</h3>
                <div class="list-group categories">

                    @foreach($categories as $category)

                        <div class="list-group-item">
                            <i class="fa fa-chevron-right"></i>
                            <a href="/productlist/{{$category->id}}/1/1" target="_blank"> {{ $category->name }}</a>

                            <div class="list-group submenu">
                                @foreach(App\Category::find($category->id)->subcategories as $subcategory)
                                    <a href="/productsublist/{{$category->id}}/{{$subcategory->id}}/1/1" class="list-group-item" target="_blank">{{ $subcategory->name }}</a>
                                @endforeach
                            </div>

                        </div>




                    @endforeach

                </div>
                <!-- Categories Links Ends -->
                <!-- Banner #2 Starts -->

                <!-- Banner #2 Ends -->
            </div>
            <!-- Sidebar Ends -->
            <!-- Primary Content Starts -->
            <div class="col-md-9">
                <!-- Breadcrumb Starts -->
                <ol class="breadcrumb">
                    <li><a href="/">Anasayfa</a></li>
                    <li class="active"><a href="/firma/{{$imalatci_id}}">Firma</a></li>
                </ol>
                <!-- Breadcrumb Ends -->
                <!-- Main Heading Starts -->
                <h2 class="main-heading2" style="background-color: #f8f8f8">
                        @if(App\User::find($imalatci_id)->logo_verified && File::exists(App\User::find($imalatci_id)->logo))
                            <img src="/{{App\User::find($imalatci_id)->logo}}" alt=""/>
                        @endif

                        <a href="/firma/{{$imalatci_id}}">
                            {{App\User::find($imalatci_id)->unvan}}
                        </a>

                        @if(App\User::find($imalatci_id)->verified==1)
                            <span class="label label-warning pull-right" style="font-size: small"><strong><i class="fa fa-check"></i> Doğrulanmış Firma </strong></span>
                        @endif

                </h2>
                <!-- Main Heading Ends -->
                <div class="bs-example">
                    <ul class="nav nav-tabs" id="myTab">
                        <li class="active"><a href="#sectionA">Firma Bilgileri</a></li>
                        <li><a href="#sectionB">İletişim Bilgileri</a></li>
                        <li><a href="#sectionC">Bayim Ol</a></li>
                        <li><a href="#sectionD">Şikayet/Tavsiye</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="sectionA" class="tab-pane fade in active">

                            <h3>Firma Bilgileri</h3><hr/>
                            <div class="row product-info full">
                                <!-- Left Starts -->
                                <div class="col-sm-6 images-block">
                                    @foreach(App\CompanyImage::where(['imalatci_id'=>$imalatci_id,'verified'=>1])->take(1)->get() as $img)
                                    <a href="/{{$img->path}}">
                                        <img src="/{{$img->path}}" alt="Image" class="img-responsive thumbnail">
                                    </a>
                                    @endforeach
                                    <ul class="list-unstyled list-inline">
                                        @foreach(App\CompanyImage::where(['imalatci_id'=>$imalatci_id,'verified'=>1])->get() as $img)
                                            <li style="max-height: 100px;max-width: 100px">
                                                <a href="/{{$img->path}}">
                                                    <img src="/{{$img->path}}" alt="Image" class="img-responsive thumbnail">
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <!-- Left Ends -->
                                <!-- Right Starts -->
                                <div class="col-sm-6 product-details">
                                    <div class="panel-smart">
                                        @foreach(App\User::where('id',$imalatci_id)->get() as $imalatci)
                                            <!-- Product Name Starts -->
                                            <h2>{{$imalatci->unvan}}</h2>
                                            <!-- Product Name Ends -->
                                            <hr>
                                            <!-- Manufacturer Starts -->
                                            <ul class="list-unstyled manufacturer">
                                                <li><span>Firmanın Ünvanı:</span> {{$imalatci->unvan}}</li>
                                                <li>
                                                    <span>Firmanın Kuruluş Tarihi:</span> {{$imalatci->established_year}}
                                                </li>
                                                <li><span>Firma Çalışan Sayısı:</span> {{$imalatci->employees_number}}</li>
                                            </ul>
                                            <!-- Manufacturer Ends -->
                                            @endforeach
                                        <!-- Available Options Ends -->
                                    </div>
                                </div>
                                <!-- Right Ends -->
                            </div>
                            <h3>Firma Yetkilisi</h3><hr/>
                            <div class="product-info full">
                                <div class="product-details">
                                    <div class="panel-smart">
                                        <ul class="list-unstyled manufacturer">
                                        @foreach(App\User::where('id',$imalatci_id)->get() as $imalatci)
                                            <li> <span>Firma Yetkilisi:</span>{{$imalatci->yetkili}} </li>
                                            @if($imalatci->yetkili_resim_verified==1)
                                            <li> <span>Firma Yetkilisi Fotoğraf: <img src="/{{$imalatci->yetkili_resim}}" class="thumbnail" alt=""/></span> </li>
                                            @endif
                                        @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <h3>Firmanın Mevcut Sertifikaları</h3><hr/>
                            <div id="ser">
                                @if(App\CertificateImage::where(['imalatci_id'=>$imalatci_id,'verified'=>1])->count()>0)

                                        <ul class="list-unstyled list-inline">
                                            @foreach(App\CertificateImage::where(['imalatci_id'=>$imalatci_id,'verified'=>1])->get() as $image)
                                                <li style="max-height: 600px;max-width: 400px">
                                                    <a href="/{{$image->path}}">
                                                        <img src="/{{$image->path}}" alt="Image" class="img-responsive" />
                                                    </a>
                                                </li>
                                            @endforeach

                                        </ul>
                                @else
                                    <div class="alert alert-info" role="alert">Şuan için gösterilebilecek herhangi bir sertifika yok.</div>
                                @endif
                            </div> <!-- row / end -->


                            <h3>Firma Profili</h3><hr/>
                            <div class="panel-smart">
                                {!!App\User::find($imalatci_id)->imalatci_profil!!}
                            </div>
                       </div>
                        <div id="sectionB" class="tab-pane fade">
                            <h3>Firma İletişim Bilgileri</h3><hr/>
                            <div class="product-info full">
                                <div class="product-details">
                                    <div class="panel-smart">
                                        <ul class="list-unstyled manufacturer">
                                            @foreach(App\User::where('id',$imalatci_id)->get() as $imalatci)
                                            <li class="nostyle"><span>Eposta:</span> {{$imalatci->email}}</li>
                                            <li><span>Telefon:</span> {{$imalatci->telefon}}</li>
                                            <li><span>Fax:</span> {{$imalatci->fax}}</li>
                                            <li><span>Yetkili  GSM:</span> {{$imalatci->yetkili_cep}}</li>
                                            <li><span>İl/İlçe:</span> {{App\City::find($imalatci->city_code)->city}}/{{$imalatci->city_town==0 ? "MERKEZ" : App\CityTown::find($imalatci->city_town)->name}}</li>
                                            <li><span>Adres:</span> {{$imalatci->adres}}</li>
                                            @if($imalatci->working_hours or "")
                                            <li><span>Çalışma Saatleri:</span>{{$imalatci->working_hours}}</li>
                                            @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="sectionC" class="tab-pane fade">
                            <h3>Bayim Ol</h3><hr/>
                            <div class="panel-smart">
                                @if(Auth::check())
                                <p> Firmanın bayiliğini yapmak istiyorsanız <strong>Bayin Olmak İstiyorum</strong> butonuna tıklayarak firmaya isteğinizle ilgili mesaj iletebilirsiniz.</p>
                                <div class="cart-button button-group">

                                        <button type="button" data-toggle="modal" data-target="#bayi_modal" class="btn btn-danger"><i class="fa fa-comment"></i> Bayin  Olmak İstiyorum</button>
                                </div>
                                @else
                                    <div class="alert alert-danger" role="alert"><i class="fa fa-exclamation-triangle"></i>
                                        Bayilik isteğinde bulunabilmeniz için <strong>üye</strong> olmanız gerekmektedir.Zaten üye iseniz
                                        <a href="http://globalstok.dev/user/perakendecigiris">Giriş</a> yapmanız gerekmektedir.
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div id="sectionD" class="tab-pane fade">
                            <h3>Şikayet/Tavsiye</h3><hr/>

                            <div class="panel-smart">
                            @if(Auth::check())
                            <form action="/messages/sikayet" method="POST">
                                {!! csrf_field()!!}
                                <div class="modal-body">
                                    <input type="hidden" name="imalatci_email" value="{{App\User::find($imalatci_id)->email}}"/>
                                    <input type="hidden" name="perakendeci_id" value="{{Auth::user()->id}}"/>
                                    <input type="hidden" name="imalatci_id" value="{{$imalatci_id}}"/>
                                    <div class="form-group">
                                        <label for="email" class="control-label">Perakendeci Eposta</label>
                                        <input type="email" class="form-control" name="email" id="email" value="{{Auth::user()->email}}" readonly>
                                    </div>

                                    <div class="form-group">
                                        <label for="email" class="control-label">Konu</label>
                                        <input type="text" name="subject" class="form-control" value=""/>
                                    </div>

                                    <div style="display: none">
                                        <textarea name="msg" id="msg"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="email" class="control-label">Mesaj</label>
                                        <textarea class="summernote" name="mesaj" id="mesaj" cols="30" rows="10"></textarea>
                                    </div>


                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Kapat</button>
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-send-o"></i> Gönder</button>
                                </div>
                            </form>
                            @else
                                <div class="alert alert-danger" role="alert"><i class="fa fa-exclamation-triangle"></i>
                                    Şikayet veya tavsiyede bulunabilmeniz için <strong>üye</strong> olmanız gerekmektedir.Zaten üye iseniz
                                    <a href="http://globalstok.dev/user/perakendecigiris">Giriş</a> yapmanız gerekmektedir.
                                </div>
                            @endif
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <!-- Primary Content Ends -->
        </div>
    </div>

    @if(Auth::check())
        <div id="bayi_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Bayin Olmak İstiyorum</h4>
                    </div>
                    <form action="/messages/bayiolmsg" method="POST">
                        {!! csrf_field()!!}
                        <div class="modal-body">
                                <input type="hidden" name="imalatci_email" value="{{App\User::find($imalatci_id)->email}}"/>
                                <input type="hidden" name="perakendeci_id" value="{{Auth::user()->id}}"/>
                                <input type="hidden" name="imalatci_id" value="{{$imalatci_id}}"/>

                                <div class="form-group">
                                    <label for="email" class="control-label">Perakendeci Eposta</label>
                                    <input type="email" class="form-control" name="email" id="email" value="{{Auth::user()->email}}" readonly>
                                </div>

                                <div class="form-group">
                                    <label for="email" class="control-label">Konu</label>
                                    <input type="text" name="subject" class="form-control" value="Bayin olmak istiyorum."/>
                                </div>

                                <div style="display: none">
                                    <textarea name="msg" id="msg"></textarea>
                                </div>

                                <div class="form-group">
                                    <label for="email" class="control-label">Mesaj</label>
                                    <textarea class="summernote" name="mesaj" id="mesaj" cols="30" rows="10"></textarea>
                                </div>

                                <div class="form-group">
                                    <label for="email" class="control-label">
                                        <input type="checkbox" name="kartvizit" checked/> <abbr title="Ad Soyad,Email ve Adres Bilgileri"><b>Kartvizit</b></abbr> bilgilerimi imalatçı/tedarikçiyle paylaşmayı kabul ediyorum.
                                    </label>

                                </div>


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Kapat</button>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-send-o"></i> Gönder</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif


    <!-- Main Container Ends -->

@endsection

@endsection