@extends('app')

@section('content')


@include('partials.mainslider')


    <!-- Main Container Starts -->
    <div id="main-container" class="container">
        <div class="row">
            <!-- Sidebar Starts -->
            <div class="col-md-3">
                <!-- Categories Links Starts -->
                @include('partials.categories_sidebar')
                <!-- Categories Links Ends -->
                <!-- Side Banner #1 Starts -->

                <!-- Side Banner #1 Ends -->
                <!-- Bestsellers Links Starts -->
                <h3 class="side-heading">Hizmetler</h3>

                <!-- Bestsellers Links Ends -->
            </div>
            <!-- Sidebar Ends -->
            <!-- Primary Content Starts -->
            <div class="col-md-9">
                <!-- Latest Products Starts -->
                <section class="product-carousel">
                    <!-- Heading Starts -->
                    <h2 class="product-head">Anasayfa Vitrini</h2>
                    <!-- Heading Ends -->
                    <!-- Products Row Starts -->
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- Product Carousel Starts -->

                            <div id="owl-product" class="owl-carousel">

                                @foreach(App\Product::where(['vitrin'=>1,'verified'=>1,'published'=>1])->get() as $vitrin)
                                <!-- Product #7 Starts -->
                                <div class="item">
                                    <div class="product-col">
                                        <div class="image">
                                            @foreach(App\ProductImage::where(['product_id'=>$vitrin->id,'image_type'=>1])->get() as $image)
                                            <img src="/{{$image->path}}" alt="product" class="img-responsive" />
                                            @endforeach
                                        </div>
                                        <div class="caption">
                                            <h4><a href="/productfull/{{$vitrin->id}}" target="_blank">{{$vitrin->title}}</a></h4>
                                            <div class="price">
                                                <span class="price-new">{{$vitrin->price}} {{ App\Currency::find($vitrin->currency_id)->name }}</span>
                                                <!-- <span class="price-old">$249.50</span>-->
                                            </div>
                                            <div class="cart-button button-group">
                                                <div class="cart-button button-group">
                                                    <a href="/productfull/{{$vitrin->id}}" target="_blank" class="btn btn-cart">
                                                        Ürünü Detayını İncele
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @endforeach
                                <!-- Product #7 Ends -->

                            </div>

                            <!-- Product Carousel Ends -->
                        </div>
                    </div>
                    <!-- Products Row Ends -->
                </section>
                <!-- Latest Products Ends -->

                <!--En Son Eklenen ürünler Starts-->

                <section class="products-list">
                    <!-- Heading Starts -->
                    <h2 class="product-head">En Son Eklenen Ürünler</h2>
                    <!-- Heading Ends -->
                    <!-- Products Row Starts -->
                    <div class="row">
                        <!-- Product #1 Starts -->
                        @foreach($product1 as $product)
                        <div class="col-md-4 col-sm-6">
                            <div class="product-col">
                                <div class="image">
                                    @foreach(App\ProductImage::where(['product_id'=>$product->id,'image_type'=>1,'verified'=>1])->get() as $image)
                                        {!!HTML::image($image->path,null,array('class'=>'img-responsive'))!!}
                                    @endforeach
                                </div>
                                <div class="caption">
                                    <h4><a href="/productfull/{{$product->id}}" target="_blank">{{$product->title}}</a></h4>
                                    <div class="price">
                                        <span class="price-new">{{$product->price}} {{ App\Currency::find($product->currency_id)->name }}</span>
                                       <!-- <span class="price-old">$249.50</span>-->
                                    </div>
                                    <div class="cart-button button-group">
                                        <a href="/productfull/{{$product->id}}" target="_blank" class="btn btn-cart">
                                            Ürünü Detayını İncele
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <!-- Product #1 Ends -->
                    </div>
                    <!-- Products Row Ends -->
                </section>

                <!--En Son Eklenen Ürünler Ends-->

                <!---En Son Eklenen Firmalar Starts-->

                 <section class="products-list">
                    <!-- Heading Starts -->
                <h2 class="product-head">En Son Eklenen Firmalar</h2>
                <!-- Heading Ends -->
                <!-- Products Row Starts -->
                <div class="row">
                    @foreach(App\User::where(['rol'=>2,'confirmed'=>1])->orderBy('id','desc')->take(3)->get() as $imalatci)
                    <div class="col-md-4 col-sm-6">
                        @if($imalatci->verified==1)
                            <span data-user="{{$imalatci->id}}" id="span_dogrulama_{{$imalatci->id}}" class="label label-success pull-right dogrulama_span"><strong><i class="fa fa-check"></i></strong></span>
                        @endif
                        <div class="product-col">
                            <div class="caption">
                                @if($imalatci->verified==1)
                                    <div id="dogrulama_{{$imalatci->id}}" class="alert alert-warning" style="display: none">
                                        <i class="fa fa-star"></i> Doğrulanmış Firma
                                    </div>
                                @endif
                                <p><a href="/firma/{{$imalatci->id}}" target="_blank">{{$imalatci->unvan}}</a></p><hr/>
                                <h4><span><abbr title="Firma Yetkilisi">{{$imalatci->yetkili}}</abbr></span></h4>

                                <div class="alert alert-info">
                                    <p><b>Cep</b> {{$imalatci->yetkili_cep}}</p>
                                    <hr class="divider">
                                    <p><b>İş</b> {{$imalatci->telefon}}</p>
                                </div>
                                <p>
                                    <abbr title="İl/İlçe">
                                        {{App\City::find($imalatci->city_code)->city}}/
                                        {{$imalatci->city_town==0 ? "MERKEZ" : App\CityTown::find($imalatci->city_town)->name}}
                                    </abbr>
                                </p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <!-- Product #1 Ends -->

                </div>
                <!-- Products Row Ends -->
                </section>

            </div>
            <!-- Primary Content Ends -->
        </div>
    </div>
    <!-- Main Container Ends -->





@stop
