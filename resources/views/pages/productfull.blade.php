@extends('app')

@section('content')
    <!-- Main Container Starts -->

    <div id="main-container" class="container">
        <!-- Breadcrumb Starts -->
        <ol class="breadcrumb">
            <li><a href="/">Anasayfa</a></li>
            <li class="active">Ürün Detayları</li>
        </ol>
        <!-- Breadcrumb Ends -->

        <div class="row">
            <div class="col-md-12">
                @include('flash::message')
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @if($errors->has())
                    <div id="form-errors">
                        <p>Bazı Hatalar Oluştu:</p>
                        <ul>
                            @foreach($errors->all() as $error)
                                <div class="alert alert-danger">{{ $error }}</div>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>

        <!-- Product Info Starts -->
        <div class="row product-info full">
            <!-- Left Starts -->
            <div class="col-sm-3 images-block">
                     @foreach(App\ProductImage::where(['product_id'=>$id,'image_type'=>1,'verified'=>1])->get() as $img)
                <a href="/{{$img->path}}">
                    <img src="/{{$img->path}}" alt="" class="img-responsive thumbnail" />
                </a>
                    @endforeach
                <ul class="list-unstyled list-inline">
                    @foreach(App\ProductImage::where(['product_id'=>$id,'image_type'=>2,'verified'=>1])->get() as $img)
                        <li style="max-width: 100px;max-height: 100px">
                            <a href="/{{$img->path}}">
                                <img src="/{{$img->path}}" alt="Image" class="img-responsive thumbnail" />
                            </a>
                        </li>
                    @endforeach

                </ul>
            </div>
            <!-- Left Ends -->
            <!-- Right Starts -->
            @foreach(App\Product::where('id',$id)->get() as $product)
            <div class="col-sm-6 product-details">
                <div class="panel-smart">
                    <!-- Product Name Starts -->
                    <h2>{{$product->title}}</h2>
                    <p>
                        {{App\City::find(App\User::find($product->imalatci_id)->city_code)->city}} -
                        {{App\User::find($product->imalatci_id)->city_town==0 ? "MERKEZ" : App\CityTown::find(App\User::find($product->imalatci_id)->city_town)->name}}
                    </p>
                    <!-- Product Name Ends -->
                    <hr />
                    <!-- Manufacturer Starts -->
                    <ul class="list-unstyled manufacturer">
                        @foreach(App\User::where(['id'=>$product->imalatci_id,'confirmed'=>1])->get() as $imalatci)
                        <li>
                            <span>Firma:</span> <a href="/firma/{{$imalatci->id}}" target="_blank">{{$imalatci->unvan}}</a>
                        </li>
                        <li><span>Ürün Kategorisi:</span>{{$productInfo['categoryName']}} / {{$productInfo['subcategoryName']}} / {{$productInfo['subprop']}}</li>
                        <li>
                            <span>Stok Durumu:</span> <strong class="{{$product->avaliability == 1 ? "label label-success" : "label label-danger" }}">{{$product->avaliability == 1 ? "Stokta Var" : "Stokta Yok" }}</strong>
                        </li>
                        <li>
                            <span>Asgari Sipariş Miktarı:</span> <strong>{{$product->min_order}}</strong>
                        </li>
                         <li>
                             <span>Ödeme Şekli:
                                 @foreach($product->payment_methods as $payment)
                                     <strong class="label label-info">{{$payment->name}}</strong>
                                 @endforeach
                             </span>

                         </li>
                         @endforeach
                    </ul>
                    <!-- Manufacturer Ends -->
                    <hr />
                    <!-- Price Starts -->
                    <div class="price">
                        <span class="price-head">Ürün Fiyatı :</span>
                        <span class="price-new">{{$product->price}} {{ App\Currency::find($product->currency_id)->name }}</span>
                        @if(Auth::check())
                        <button class="btn btn-danger btn-xs" type="button" data-toggle="modal" data-target="#modal1"><i class="fa fa-comments-o"></i> Farklı Fiyat Teklifi Almak İçin Satıcıyla İrtibata Geç</button>
                        @endif
                        <!--<span class="price-old">$249.50</span>-->
                    </div>
                    <!-- Price Ends -->
                    <hr />
                    <!-- Available Options Starts -->
                    @if($product->avaliability==1)
                    <div class="options">

                        <form action="/messages/index" method="GET">
                         {!! csrf_field() !!}
                         <input type="hidden" name="product_id" value="{{$product->id}}"/>
                        <div class="cart-button button-group">
                            <button type="submit" class="btn btn-warning">
                                <i class="fa fa-envelope-o"></i>
                                Firmayla İletişime Geç
                            </button>
                        </div>
                        </form>


                    </div>
                    @endif
                    <!-- Available Options Ends -->
                </div>
            </div>
            @endforeach


            <div class="col-sm-3">
                @foreach(App\User::where(['id'=>App\Product::find($id)->imalatci_id,'confirmed'=>1])->get() as $imalatci)
                    @if($imalatci->verified==1)
                        <span data-user="{{$imalatci->id}}" id="span_dogrulama_{{$imalatci->id}}" class="label label-success pull-right dogrulama_span"><strong><i class="fa fa-check"></i></strong></span>
                    @endif
                <div class="panel-smart">
                    @if($imalatci->verified==1)
                        <div id="dogrulama_{{$imalatci->id}}" class="alert alert-warning" style="display: none">
                            <i class="fa fa-star"></i> Doğrulanmış Firma
                        </div>
                    @endif
                        <p><a href="/firma/{{$imalatci->id}}" target="_blank">{{$imalatci->unvan}}</a></p><hr/>
                        <h4><span><abbr title="Firma Yetkilisi">{{$imalatci->yetkili}}</abbr></span></h4>

                        <div class="alert alert-info">
                            <p><b>Cep</b> {{$imalatci->yetkili_cep}}</p>
                            <hr class="divider">
                            <p><b>İş</b> {{$imalatci->telefon}}</p>
                        </div>
                        <p>
                            <abbr title="İl/İlçe">
                                {{App\City::find($imalatci->city_code)->city}}/
                                {{$imalatci->city_town==0 ? "MERKEZ" : App\CityTown::find($imalatci->city_town)->name}}
                            </abbr>
                        </p>
                        <hr/>
                        <p><a href="/product_manufacturer/{{$imalatci->id}}/1/1" target="_blank">Satıcının Tüm İlanları</a></p>

                </div>
                @endforeach
            </div>

            <!-- Right Ends -->
        </div>

        <!-- Product Info Ends -->
        <!-- Tabs Starts -->
        <div class="tabs-panel panel-smart">
            <!-- Nav Tabs Starts -->
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab-description">Ürün Açıklaması</a>
                </li>
                <li><a href="#tab-review">Ürün İle İlgili Görüşleriniz</a></li>
            </ul>
            <!-- Nav Tabs Ends -->
            <!-- Tab Content Starts -->
            <div class="tab-content clearfix">
                <!-- Description Starts -->
                <div class="tab-pane active" id="tab-description">
                    <p>{{$product->description}}</p>
                </div>
                <!-- Description Ends -->

                <!-- Review Starts -->
                <div class="tab-pane" id="tab-review">
                    <form class="form-horizontal" action="/fpage/productreview" method="POST">
                        {!! csrf_field() !!}
                        <input type="hidden" name="product_id" value="{{$product->id}}">
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-name">Ad Soyad</label>
                            <div class="col-sm-10">
                                <input type="text" name="name" value="" id="name" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-name">Eposta</label>
                            <div class="col-sm-10">
                                <input type="text" name="email" value="" id="email" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-review">Görüşleriniz</label>
                            <div class="col-sm-10">
                                <textarea name="review" rows="5" id="input-review" class="form-control"></textarea>
                                <div class="help-block">
                                    Ürün ile ilgili görüşlerinizi belirtiniz...
                                </div>
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label ratings">Ürünü Oyla</label>
                            <div class="col-sm-10">
                                <input id="rating-input" name="rating" type="number"/>
                            </div>
                        </div>
                        <div class="buttons">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" id="button-review" class="btn btn-main">
                                    <i class="fa fa-send"></i>
                                    Gönder
                                </button>
                            </div>
                        </div>
                    </form>


                </div>
                <!-- Review Ends -->
            </div>
            <!-- Tab Content Ends -->
        </div>
        <!-- Tabs Ends -->
        <!-- Related Products Starts -->
        <div class="product-info-box">
            <h4 class="heading">İlgili Ürünler</h4>
            <!-- Products Row Starts -->
            <div class="row">
                @foreach($related as $r)
                <!-- Product #1 Starts -->
                <div class="col-md-3 col-sm-6">
                    <div class="product-col">
                        <div class="image">
                            @foreach(App\ProductImage::where(['product_id'=>$r->id,'image_type'=>1,'verified'=>1])->get() as $image)
                            <img src="/{{$image->path}}" alt="product" class="img-responsive" />
                             @endforeach
                        </div>
                        <div class="caption">
                            <h4><a href="/productfull/{{$r->id}}">{{$r->title}}</a></h4>
                            <div class="price">
                                <span class="price-new">{{$r->price}} {{ App\Currency::find($r->currency_id)->name }}</span>
                            </div>
                            <div class="cart-button button-group">
                                <a href="/productfull/{{$r->id}}" target="_blank" class="btn btn-cart">
                                    Ürün Detayını İncele
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <!-- Product #1 Ends -->
            </div>
            <!-- Products Row Ends -->
        </div>
        <!-- Related Products Ends -->
    </div>

    <!--modal-1:Farklı Fiyat ALmak için Satıcıyla İrtibata Geç-->
    @if(Auth::check())
    <div id="modal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    								<div class="modal-dialog">
    								  <div class="modal-content">
    								  <div class="modal-header">
    									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    									<h4 class="modal-title">Farkı Fiyat Teklifi Almak İçin Satıcıyla İrtibata Geç</h4>
    								  </div>
                                      <form action="/messages/fiyatteklifi" method="POST">
                                          {!! csrf_field()!!}
                                          <div class="modal-body">

                                              @foreach(App\Product::where('id',$id)->get() as $product)

                                              <span class="label label-info">Ürün Fiyatı :{{$product->price}} {{ App\Currency::find($product->currency_id)->name }}</span><br>
                                              <span class="label label-info">Stok Durumu:</span> <strong class="{{$product->avaliability == 1 ? "label label-success" : "label label-danger" }}">{{$product->avaliability == 1 ? "Stokta Var" : "Stokta Yok" }}</strong>
                                              <h5><label>Ürün Stok Miktarı:<span class="badge">{{$product->amount}} {{App\AmountType::find($product->amount_type)->name}}</span></label></h5>

                                              <div class="form-group">
                                                      <label for="email" class="control-label">Satın Almak İstediğiniz Miktar</label>
                                                      <input type="text" name="miktar" class="form-control" placeholder="Satın Almak İstediğiniz Miktarı Giriniz"/>
                                             </div>

                                              <input type="hidden" name="imalatci_email" value="{{App\User::find($product->imalatci_id)->email}}"/>
                                              <input type="hidden" name="product_id" value="{{$product->id}}"/>
                                              <input type="hidden" name="perakendeci_id" value="{{Auth::user()->id}}"/>

                                              <div class="form-group">
                                                      <label for="email" class="control-label">Perakendeci Eposta</label>
                                                      <input type="email" class="form-control" name="email" id="email" value="{{Auth::user()->email}}" readonly>
                                              </div>

                                              <div class="form-group">
                                                  <label for="email" class="control-label">Konu</label>
                                                  <input type="text" name="subject" class="form-control" value="{{$product->title}} ürününüz ile ilgili farklı fiyat teklifi almak istiyorum."/>
                                              </div>

                                              <div style="display: none">
                                                  <textarea name="msg" id="msg"></textarea>
                                              </div>

                                              <div class="form-group">
                                                  <label for="email" class="control-label">Mesaj</label>
                                                  <textarea class="summernote" name="mesaj" id="mesaj" cols="30" rows="10"></textarea>
                                              </div>

                                              <div class="form-group">
                                                  <label for="email" class="control-label">
                                                      <input type="checkbox" name="kartvizit" checked/> <abbr title="Ad Soyad,Email ve Adres Bilgileri"><b>Kartvizit</b></abbr> bilgilerimi imalatçı/tedarikçiyle paylaşmayı kabul ediyorum.
                                                  </label>

                                              </div>
                                              @endforeach

                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Kapat</button>
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-send-o"></i> Gönder</button>
                                          </div>
                                      </form>
    								</div>
    								</div>
    								</div>



    @endif


    <!-- Main Container Ends -->
@endsection