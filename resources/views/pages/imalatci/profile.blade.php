@extends('master_imalatci')

@section('content')

    <div class="page-head">
        <!-- Page heading -->
        <h2 class="pull-left">İmalatçı Profili</h2>

        <div class="clearfix"></div>
    </div><!--/ Page heading ends -->



    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @include('flash::message')
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @if($errors->has())
                    <div id="form-errors">
                        <ul>
                            @foreach($errors->all() as $error)
                                <div class="alert alert-danger">{{ $error }}</div>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="widget wlightblue">
                    <div class="widget-head">
                        <div class="pull-left">Profil</div>
                        <div class="widget-icons pull-right">
                            <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="widget-content">
                        <div class="table-responsive">
                            <table class="table  table-bordered ">
                                <tbody>
                                <tr><td>Vergi Numarası</td><td>{{ Auth::user()->vergino }}</td></tr>
                                <tr><td>Ticaret Ünvanı</td><td>{{ Auth::user()->unvan }}</td></tr>
                                    @foreach($city as $c)
                                        <tr><td>İl</td><td>{{ $c->city }}</td></tr>
                                    @endforeach
                                <tr><td>İlçe</td>
                                    <td>
                                       @if(Auth::user()->city_town == 0)
                                           MERKEZ
                                       @else
                                           {{App\CityTown::find(Auth::user()->city_town)->name}}
                                       @endif
                                    </td>
                                </tr>
                                <tr> <td>Adres</td><td>{{Auth::user()->adres}}</td></tr>
                                <tr><td>Telefon</td><td>{{Auth::user()->telefon}}</td></tr>
                                <tr><td>Fax</td><td>{{Auth::user()->fax}}</td>
                                <tr><td>Eposta</td><td>{{Auth::user()->email}}</td>
                                <tr><td>Web Sitesi</td><td>{{Auth::user()->web}}</td> </tr>
                                <tr><td>Firma Logosu (64x64)</td><td>
                                        <div id="logo">
                                            @if(File::exists(Auth::user()->logo))
                                            <a href="/{{Auth::user()->logo}}" class="fancybox" data-fancybox-group="gallery-1">
                                                <img src="/{{Auth::user()->logo}}"/>
                                            </a>
                                            @endif
                                        </div>
                                        <form action="/imalatci/addlogo"  enctype="multipart/form-data" method="post">
                                            {!! csrf_field() !!}
                                        <input  name="logo" type="file" data-imalatci="{{Auth::user()->id}}" class="file-loading logo" accept="image/*" >
                                        </form>
                                    </td>
                                </tr>
                                <tr><td>Yetkilinin Resmi (64x64)</td>
                                    <td>
                                        <div id="yetkiliresim">
                                            @if(File::exists(Auth::user()->yetkili_resim))
                                                <a href="/{{Auth::user()->yetkili_resim}}" class="fancybox" data-fancybox-group="gallery-1">
                                                    <img src="/{{Auth::user()->yetkili_resim}}"/>
                                                </a>
                                            @endif
                                        </div>
                                        <form action="/imalatci/addyetkiliresim"  enctype="multipart/form-data" method="post">
                                            {!! csrf_field() !!}
                                            <input  name="yetkiliresim" type="file" data-imalatci="{{Auth::user()->id}}" class="file-loading yetkili_resim" accept="image/*"  >
                                        </form>
                                    </td>
                                </tr>
                                <tr><td>Üretici/Tedarikçi Sertifikaları (Max 5 Adet)</td>
                                    <td>
                                        <div id="my-gallery" class="gallery editable-gallery">
                                            @foreach(App\CertificateImage::where(['imalatci_id'=>Auth::user()->id])->get() as $image)
                                                <div data-image="{{$image->id}}" class="icon-remove blue delete delete-img">
                                                    @if($image->verified==0)
                                                        <button type="button" data-toggle="tooltip" title="Sertifika resmi yönetici tarafından onay bekliyor."  class="btn btn-danger btn-xs not-verified"><i class="fa fa-exclamation"></i></button>
                                                    @else
                                                        <button type="button" data-toggle="tooltip" title="Sertifika resmi yönetici tarafından onaylanmış."  class="btn btn-success btn-xs verified"><i class="fa fa-check"></i></button>
                                                    @endif
                                                    <button type="button" data-image="{{$image->id}}" class="btn btn-danger btn-xs delete-button delete-sertifika"><i class="fa fa-times"></i></button>
                                                    <a href="/{{$image->path}}" class="fancybox" data-fancybox-group="gallery-1">
                                                        {!! HTML::image($image->path,null,array('style'=>'max-width:80px;max-height:80px')) !!}
                                                    </a>
                                                </div>
                                            @endforeach

                                        </div>
                                        <input  name="sertifika[]" type="file" data-imalatci="{{Auth::user()->id}}" class="file-loading sertifika" accept="image/*" multiple="true" >
                                    </td>
                                </tr>
                                <tr><td>Şirkete Ait Resimler (Max 5 Adet)</td>
                                    <td>
                                        <div id="resim-gallery" class="gallery editable-gallery">
                                            @foreach(App\CompanyImage::where(['imalatci_id'=>Auth::user()->id])->get() as $image)
                                                <div data-image="{{$image->id}}" class="icon-remove blue delete delete-img">
                                                    @if($image->verified==0)
                                                        <button type="button" data-toggle="tooltip" title="Resim yönetici tarafından onay bekliyor."  class="btn btn-danger btn-xs not-verified"><i class="fa fa-exclamation"></i></button>
                                                    @else
                                                        <button type="button" data-toggle="tooltip" title="Resim yönetici tarafından onaylanmış."  class="btn btn-success btn-xs verified"><i class="fa fa-check"></i></button>
                                                    @endif
                                                    <button type="button" data-image="{{$image->id}}" class="btn btn-danger btn-xs delete-button delete-resim"><i class="fa fa-times"></i></button>
                                                    <a href="/{{$image->path}}" class="fancybox" data-fancybox-group="gallery-1">
                                                        {!! HTML::image($image->path,null,array('style'=>'max-width:80px;max-height:80px')) !!}
                                                    </a>
                                                </div>
                                            @endforeach

                                        </div>
                                        <input  name="resim[]" type="file" data-imalatci="{{Auth::user()->id}}" class="file-loading resim" accept="image/*" multiple="true" >
                                    </td>
                                </tr>
                                <tr><td>#</td><td><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#profileUptadeModal">Profili Güncelle</button></td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>

     <div id="profileUptadeModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     								<div class="modal-dialog modal-lg">
     								  <div class="modal-content">
     								  <div class="modal-header">
     									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
     									<h4 class="modal-title">Profil Güncelle</h4>
     								  </div>
     								  <div class="modal-body">
                                          <form action="/imalatci/updateprofile" method="POST">
                                              {!! csrf_field() !!}
                                               <div class="form-group">
                                                   <label for="control-label">Ticaret Ünvanı</label>
                                                   <input class="form-control" type="text" name="unvan" value="{{Auth::user()->unvan}}"/>
                                               </div>

                                              <div class="form-group">
                                                  <label for="control-label">İl</label>
                                                  <select class="form-control city" name="city" id="city">
                                                      <option value="" selected>İl Seçiniz</option>
                                                      @foreach($city as $c)
                                                          @foreach($cities as $city)
                                                              <option value="{{$city->city_code}}">{{$city->city}}</option>
                                                          @endforeach
                                                      @endforeach
                                                  </select>
                                              </div>

                                              <div class="form-group" >
                                                  <label for="inputFname" class="control-label">İlçe:</label>
                                                      <select name="city_town" class="form-control">
                                                          <option value="0">MERKEZ</option>
                                                      </select>
                                              </div>

                                              <div class="form-group">
                                                  <label class="control-label">Adres</label>
                                                  <textarea class="form-control" name="adres" id="adres" rows="10">{{Auth::user()->adres}}</textarea>
                                              </div>

                                              <div class="form-group">
                                                  <label class="control-label" for="">Telefon</label>
                                                  <input class="form-control" type="text" name="telefon" value="{{Auth::user()->telefon}}"/>
                                              </div>

                                              <div class="form-group">
                                                  <label class="control-label" for="">Fax</label>
                                                  <input class="form-control" type="text" name="fax" value="{{ Auth::user()->fax }}"/>
                                              </div>

                                              <div class="form-group">
                                                  <label class="control-label" for="">Eposta</label>
                                                  <input class="form-control" type="text" name="email" value="{{ Auth::user()->email }}"/>
                                              </div>

                                              <div class="form-group">
                                                  <label class="control-label" for="">Web Sitesi</label>
                                                  <input class="form-control" type="text" name="web" value="{{Auth::user()->web}}"/>
                                              </div>

                                              <div class="form-group">
                                                  <label class="control-label" for="">Çalışan Sayısı</label>
                                                  <input class="form-control" type="text" name="employees_number" value="{{Auth::user()->employees_number}}"/>
                                              </div>

                                              <div class="form-group">
                                                  <label class="control-label" for="">Çalışma Saatleri Aralığı</label>
                                                  <input class="form-control" type="text" name="working_hours" value="{{Auth::user()->working_hours}}"/>
                                              </div>

                                              <div class="form-group">
                                                  <label class="control-label">Şirket Profili</label>
                                                  <textarea name="imalatci_profil" id="imalatci_profil" class="form-control" cols="30" rows="10">
                                                      {!!Auth::user()->imalatci_profil!!}
                                                  </textarea>

                                              </div>

                                              <div class="form-group">
                                                  <label class="control-label" for="">Şifre</label>
                                                  <input type="password" class="form-control"  name="password" placeholder="Şifreniz en az 8 karakter olmalı ve alfa nümerik karakterlerden oluşmalı."/>
                                              </div>

                                              <div class="form-group">
                                                  <label class="control-label" for="">Şifre (Tekrar)</label>
                                                  <input type="password" class="form-control"  name="password_confirmation" placeholder="Şifrenizi tekrar giriniz."/>
                                              </div>


     								  </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Kapat</button>
                                            <button type="submit" class="btn btn-primary">Değişiklikleri Kaydet</button>
                                          </div>

                                          </form>
     								</div>
     								</div>
     								</div>

@endsection