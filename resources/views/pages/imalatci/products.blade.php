@extends('master_imalatci')

@section('content')
    <!-- Page heading -->
    <div class="page-head">
        <!-- Page heading -->
        <h2 class="pull-left">Ürün İşlemleri</h2>

        <div class="clearfix"></div>
    </div><!--/ Page heading ends -->

    <!-- Matter -->

        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    @if($errors->has())
                        <div id="form-errors">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <div class="alert alert-danger">{{ $error }}</div>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="widget wred">
                        <div class="widget-head">
                            <div class="pull-left">Eklenen Ürünler</div>
                            <div class="widget-icons pull-right">
                                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="widget-content medias">
                            <div class="table-responsive">
                                <table id="productList" class="table table-bordered ">
                                    <thead>
                                    <tr>
                                        <th>Ürün Başlığı</th>
                                        <th>Ürün Açıklaması</th>
                                        <th>Ürün Kategorisi</th>
                                        <th>Ürün Alt Kategorisi</th>
                                        <th>Ürün Alt Kategori Özelliği</th>
                                        <th>Ürünün Fiyatı</th>
                                        <th>Ürünün Miktarı</th>
                                        <th>Ürünün Stok Durumu</th>
                                        <th>Ürün Onay Durumu</th>
                                        <th>Ürün Yayınlanma Durumu</th>
                                        <th>Ürünün Oluşturulma Tarihi</th>
                                        <th>Ürünün Güncellenme Tarihi</th>
                                        <th>#Güncelle</th>
                                        <th>#Sil</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach(App\Product::where('imalatci_id',Auth::user()->id)->get() as $product)
                                        <tr>
                                            <td>{{ $product->title }}</td>
                                            <td>{{ $product->description }}</td>
                                            <td>{{ App\Category::find($product->category_id)->name }}</td>
                                            <td>{{ App\SubCategory::find($product->subcategory_id)->name }}</td>
                                            <td>{{App\SubProperty::find($product->subprop_id)->name}}</td>
                                            <td>{{ $product->price }} {{App\Currency::find($product->currency_id)->name}}</td>
                                            <td>{{ $product->amount}} {{ App\AmountType::find($product->amount_type)->name }}</td>
                                            <td>{{ $product->avaliability==1 ? "Stokta Var" :"Stokta Yok" }}</td>
                                            <td>{{ $product->verified == 1 ? "Ürün Onaylandı" :"Ürün Onay Bekliyor" }}</td>
                                            <td>{{ $product->published==1 ? "Ürün Yayınlandı" :"Onay Bekliyor" }}</td>
                                            <td>{{ $product->created_at }}</td>
                                            <td>{{ $product->updated_at }}</td>
                                            <td>
                                                <button data-toggle="modal" data-target="#modal_{{$product->id}}" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i> </button>
                                            </td>
                                            <td>
                                                <button class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i> </button>
                                            </td>
                                        </tr>

                                        <div  id="modal_{{$product->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="widget wgreen">
                                                                <div class="widget-head">
                                                                    <div class="pull-left">Ürün Güncelle</div>
                                                                    <div class="widget-icons pull-right">
                                                                        <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>

                                                                <div class="widget-content">
                                                                    <div class="padd">

                                                                    <form action="/imalatci/products/updateproduct" id="{{$product->id}}"  enctype="multipart/form-data" method="post">

                                                                        {!! csrf_field() !!}

                                                                        <input type="hidden" name="imalatci_id" value="{{ Auth::user()->id }}"/>

                                                                        <input type="hidden" name="id" id="product_id" value="{{$product->id}}">

                                                                        <div class="form-group">
                                                                            <label class="control-label">Ürün Başlığı</label>
                                                                             <input type="text" name="title" class="form-control" value="{{ $product->title }}">
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="control-label">Ürün Açıklaması</label>
                                                                            <textarea class="form-control"  rows="5" name="description">{{ $product->description }}</textarea>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="control-label">Ürünün Fiyatı</label>
                                                                            <input type="text" name="price" class="form-control price" value="{{$product->price}}">
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="control-label">Para Birimi</label>
                                                                            <select name="currency" id="currency" class="form-control">
                                                                                @foreach(App\Currency::all() as $currency)
                                                                                    <option value="{{$currency->id}}" {{$product->currency_id==$currency->id ? 'selected' : ''}}>{{$currency->desc}}({{$currency->name}})</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="control-label">Ürünün Stok Durumu</label>
                                                                                <select class="form-control" name="avaliability">
                                                                                    <option value="1" {{ $product->avaliability==1 ? "selected" : "" }}>Stokta Var</option>
                                                                                    <option value="0" {{ $product->avaliability==0 ? "selected" : "" }}>Stokta Yok</option>
                                                                                </select>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="control-label">Ürün Miktar Tipi</label>
                                                                                <select class="form-control" name="amount_type">
                                                                                    @foreach(App\AmountType::all() as $amount_type)
                                                                                        <option value="{{$amount_type->id}}" {{$product->amount_type==$amount_type->id ? "selected" : ""}} >{{$amount_type->name}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="control-label">Ürünün Miktarı</label>
                                                                            <input type="text" name="amount" class="form-control" value="{{ $product->amount }}">
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="control-label">Asgari Sipariş Miktarı</label>
                                                                            <input type="text" name="min_order" class="form-control" value="{{ $product->min_order }}">
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="control-label">Ürün Kategorisi</label>
                                                                                <select class="form-control" name="subcategory" id="subcategory">
                                                                                    @foreach(App\Category::all() as $category)
                                                                                        <optgroup label="{{ $category->name }}">
                                                                                            @foreach(App\Subcategory::where('category_id',$category->id)->get() as $subcategory)
                                                                                                <option value="{{$subcategory->id}}" {{ $product->subcategory_id==$subcategory->id ? "selected" :"" }}>{{$subcategory->name}}</option>
                                                                                            @endforeach
                                                                                        </optgroup>
                                                                                    @endforeach
                                                                                </select>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="control-label">Ürün Alt Kategori Özelliği</label>
                                                                            <select class="form-control" name="subprop" id="subprop">
                                                                                @foreach(App\SubProperty::where('subcategory_id',$product->subcategory_id)->get() as $prop)
                                                                                    <option value="{{$prop->id}}" {{$product->subprop_id == $prop->id ? 'selected' :'' }}>{{$prop->name}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="control-label">Ödeme Şekli</label>
                                                                            <select name="payment_method[]" id="payment_method" class="form-control" multiple>
                                                                                    @foreach(App\PaymentMethod::all() as $payment)
                                                                                        <option value="{{$payment->id}}" {{ in_array($payment->id,array_pluck($product->payment_methods,'id')) ? 'selected' : ''}}>{{$payment->name}}</option>
                                                                                    @endforeach
                                                                            </select>

                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="control-label">Ürün Ana Resmi</label> <br>

                                                                                <div id="main-gallery" class="gallery editable-gallery">
                                                                                    @foreach(App\ProductImage::where(['product_id'=>$product->id,'image_type'=>1])->get() as $image)
                                                                                         <div  data-image="{{$image->id}}" class="icon-remove blue delete delete-ana">
                                                                                             @if($image->verified==0)
                                                                                                <button type="button" data-toggle="tooltip" title="Resim yönetici tarafından onay bekliyor."  class="btn btn-danger btn-xs not-verified"><i class="fa fa-exclamation"></i></button>
                                                                                             @else
                                                                                                 <button type="button" data-toggle="tooltip" title="Resim yönetici tarafından onaylanmış."  class="btn btn-success btn-xs verified"><i class="fa fa-check"></i></button>
                                                                                             @endif
                                                                                            <button type="button" data-image="{{$image->id}}" class="btn btn-danger btn-xs delete-button delete-anaurun "><i class="fa fa-times"></i></button>
                                                                                            <a href="/{{$image->path}}" class="fancybox">
                                                                                                {!! HTML::image($image->path,null,array('style'=>'max-width:80px;max-height:80px')) !!}
                                                                                            </a>
                                                                                        </div>
                                                                                    @endforeach
                                                                                </div>


                                                                            <input id="{{$product->id}}" name="image" type="file" class="file-loading up" accept="image/*" >
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="control-label">Ürün İle ilgili Diğer Resimler</label>

                                                                            <div id="my-gallery" class="gallery editable-gallery">

                                                                                    @foreach(App\ProductImage::where(['product_id'=>$product->id,'image_type'=>2])->get() as $image)
                                                                                        <div data-image="{{$image->id}}" class="icon-remove blue delete delete-img">
                                                                                            @if($image->verified==0)
                                                                                                <button type="button" data-toggle="tooltip" title="Resim yönetici tarafından onay bekliyor."  class="btn btn-danger btn-xs not-verified"><i class="fa fa-exclamation"></i></button>
                                                                                            @else
                                                                                                <button type="button" data-toggle="tooltip" title="Resim yönetici tarafından onaylanmış."  class="btn btn-success btn-xs verified"><i class="fa fa-check"></i></button>
                                                                                            @endif
                                                                                            <button type="button" data-image="{{$image->id}}" class="btn btn-danger btn-xs delete-button delete-urun-resmi"><i class="fa fa-times"></i></button>
                                                                                            <a href="/{{$image->path}}" class="fancybox" data-fancybox-group="gallery-1">
                                                                                                {!! HTML::image($image->path,null,array('style'=>'max-width:80px;max-height:80px')) !!}
                                                                                            </a>
                                                                                        </div>
                                                                                    @endforeach

                                                                            </div>
                                                                            <input id="{{$product->id}}" name="images[]" type="file" class="file-loading ups" product_id = "{{$product->id}}" accept="image/*" multiple="true">
                                                                        </div>


                                                                        <div class="modal-footer">
                                                                            <br>
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                                                                            <button type="submit" class="btn btn-primary">Değişiklikleri Kaydet</button>
                                                                        </div>

                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <hr/>

            <div class="row">
                <div class="col-md-12">
                    <div class="widget wgreen">
                        <div class="widget-head">
                            <div class="pull-left">Yeni Ürün Ekle</div>
                            <div class="widget-icons pull-right">
                                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="widget-content">
                            <div class="padd">

                                <h6>Yeni ürün Ekleme Formu</h6>
                                <hr>
                                <!-- Form starts.  -->

                                    {!! Form::open(array('url'=>'/imalatci/products/addproduct','class'=>'form-horizontal','files'=>true)) !!}
                                        {!! csrf_field() !!}
                                    <input type="hidden" name="imalatci_id" value="{{ Auth::user()->id }}"/>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Ürün Başlığı</label>
                                        <div class="col-md-8">
                                            <input type="text" name="title" class="form-control" placeholder="Ürün başlığını giriniz.">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Ürün Açıklaması</label>
                                        <div class="col-md-8">
                                            <textarea class="form-control"  rows="5" name="description" placeholder="Ürün hakkında detaylı bir açıklama yapınız." ></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Ürünün Fiyatı</label>
                                        <div class="col-md-6">
                                            <input type="text" name="price" class="form-control price" placeholder="Ürünün fiyatını  belirleyiniz.">
                                        </div>
                                        <div class="col-md-2">
                                            <select name="currency" id="currency" class="form-control">
                                                <option value="">Para Birimi</option>
                                                @foreach(App\Currency::all() as $currency)
                                                    <option value="{{$currency->id}}">{{$currency->desc}} ({{$currency->name}})</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Ürünün Stok Durumu</label>
                                        <div class="col-md-8">
                                            <select class="form-control" name="avaliability">
                                                <option value="1">Stokta Var</option>
                                                <option value="0">Stokta Yok</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Ürün Miktar Tipi</label>
                                        <div class="col-md-8">
                                            <select class="form-control" name="amount_type">
                                                <option value="">Ürün Miktarı Tipini Seçiniz</option>
                                               @foreach(App\AmountType::all() as $amount_type)
                                                    <option value="{{$amount_type->id}}">{{$amount_type->name}}</option>
                                               @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Ürünün Miktarı</label>
                                        <div class="col-md-8">
                                            <input type="text" name="amount" class="form-control" placeholder="Ürünün Miktarını Belirleyiniz.">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Asgari Sipariş Miktarı</label>
                                        <div class="col-md-8">
                                            <input type="text" name="min_order" class="form-control" placeholder="Ürünün Asgari Sipariş Miktarını Belirleyiniz.">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Ürün Kategorisi</label>
                                        <div class="col-md-8">
                                            <select class="form-control" name="subcategory" id="subcategory">
                                                <option value="">Kategori Seçininiz...</option>
                                                @foreach(App\Category::all() as $category)
                                                    <optgroup label="{{ $category->name }}">
                                                        @foreach(App\Category::find($category->id)->subcategories as $subcategory)
                                                            <option value="{{$subcategory->id}}">{{$subcategory->name}}</option>
                                                        @endforeach
                                                    </optgroup>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Ürün Alt Kategori Özelliği</label>
                                        <div class="col-md-8" id="subprop"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Ödeme Şekli</label>
                                        <div class="col-md-8">
                                            <select name="payment_method[]" id="payment_method" class="form-control" multiple>
                                                @foreach(App\PaymentMethod::all() as $payment)
                                                    <option value="{{$payment->id}}">{{$payment->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-md-offset-2 col-md-8">
                                            <button type="submit" class="btn btn-success btn-lg">Ürünü Ekle</button>
                                        </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>



@endsection