@extends('app')

@section('content')
    <style>
        div.list-group-item .submenu{
            position: absolute;
            display: none;
            top:0;
            left: 100%;
            width:100%;
            z-index: 1;
        }

        div.list-group-item:hover .submenu{
            display:block;
            right:0;;
        }
    </style>

    <!-- Main Container Starts -->
    <div id="main-container" class="container">
        <div class="row">
            <!-- Sidebar Starts -->
            <div class="col-md-3">
                <!-- Categories Links Starts -->
                <h3 class="side-heading">{{App\Category::find($category_id)->name}}</h3>
                <div class="list-group categories">

                    @foreach(App\SubCategory::where('category_id',$category_id)->get() as $subcategory)

                        <div class="list-group-item">
                            <i class="fa fa-chevron-right"></i>
                            <a href="/productsublist/{{$category_id}}/{{$subcategory->id}}/1/1"> {{ $subcategory->name }}</a>
                            {!!$subcategory->id == $subcategory_id ? '<i class="fa fa-check"></i>' : ''!!}

                            <div class="list-group submenu">
                                    @foreach(App\SubProperty::where('subcategory_id',$subcategory->id)->get() as $prop)
                                        <a href="/productsubprop/{{$category_id}}/{{$subcategory_id}}/1/1/{{$prop->id}}" class="list-group-item" target="_blank">{{ $prop->name }}</a>
                                    @endforeach
                            </div>
                        </div>


                    @endforeach

                </div>
                <!-- Categories Links Ends -->
                <!-- Banner #1 Starts -->

                <!-- Banner #1 Ends -->
                <!-- Shopping Options Starts -->
                <h3 class="side-heading">Filtreleme</h3>
                <div class="list-group">
                    <div class="list-group-item">
                        İle Göre Filtrele
                    </div>
                    <form action="/productsublist/{{$category_id}}/{{$subcategory_id}}/{{$ordertype}}/{{$listtype}}" method="get">
                        {!! csrf_field() !!}
                        <div class="list-group-item">
                            <div class="filter-group" style="max-height: 500px;overflow-y: scroll">
                                @foreach(App\City::all() as $city)
                                    <div class="col-xs-12">
                                        <div class="col-xs-3">
                                            <input name="city[]" type="checkbox" class="form-control" value="{{$city->city_code}}" />
                                        </div>
                                        <div class="col-xs-3">
                                            <label  class="checkbox pull-left">
                                                {{$city->city}}
                                            </label>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="list-group-item">
                            <button type="submit" class="btn btn-black">Filtrele</button>
                        </div>
                    </form>
                </div>
                <!-- Shopping Options Ends -->
                <!-- Banner #2 Starts -->

                <!-- Banner #2 Ends -->
            </div>
            <!-- Sidebar Ends -->
            <!-- Primary Content Starts -->
            <div class="col-md-9">
                <!-- Breadcrumb Starts -->
                <ol class="breadcrumb">
                    <li><a href="/">Anasayfa</a></li>
                    <li><a href="/productlist/{{$category_id}}/1/1"> {{App\Category::find($category_id)->name}}</a></li>
                    <li class="active"><a href="/productsublist/{{$category_id}} / {{$subcategory_id}}/1/1"> {{App\SubCategory::find($subcategory_id)->name}}</a></li>
                </ol>
                <!-- Breadcrumb Ends -->
                <!-- Main Heading Starts -->
                <h2 class="main-heading2">
                    {{App\Category::find($category_id)->name}} / {{App\SubCategory::find($subcategory_id)->name}}
                </h2>
                <!-- Main Heading Ends -->

                <!-- Product Filter Starts -->
                <div class="product-filter">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="display">
                                <a href="/productsublist/{{$category_id}}/{{$subcategory_id}}/{{$ordertype}}/1" class="{{$listtype==1 ? 'active' : ''}}">
                                    <i class="fa fa-th-list" title="List View"></i>
                                </a>
                                <a href="/productsublist/{{$category_id}}/{{$subcategory_id}}/{{$ordertype}}/2" class="{{$listtype==2 ? 'active' : ''}}">
                                    <i class="fa fa-th" title="Grid View"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-2 text-right">
                            <label class="control-label">Sıralama</label>
                        </div>
                        <div class="col-md-3 text-right">
                            <select class="form-control" onchange="location = this.options[this.selectedIndex].value;">
                                <option value="" selected>----------------------------------</option>
                                <option value="/productsublist/{{$category_id}}/{{$subcategory_id}}/1/{{$listtype}}">Normal</option>
                                <option value="/productsublist/{{$category_id}}/{{$subcategory_id}}/2/{{$listtype}}">Fiyat Artan</option>
                                <option value="/productsublist/{{$category_id}}/{{$subcategory_id}}/3/{{$listtype}}">Fiyat Azalan</option>
                            </select>
                        </div>
                    </div>
                </div>
                <!-- Product Filter Ends -->
                <!-- Product List Display Starts -->
                @if($listtype==1)
                    <div class="row">
                        <!-- Product #1 Starts -->
                        @if($ordertype==1)
                            @foreach(isset($filtered_product) && !empty($filtered_product) ? $filtered_product :(App\Product::where(['category_id'=>$category_id,'subcategory_id'=>$subcategory_id,'verified'=>1,'published'=>1])->paginate(6)) as $product)

                                <div class="col-xs-12">
                                    <div class="product-col list clearfix">
                                        <div class="image">
                                            @foreach(App\ProductImage::where(['product_id'=>$product->id,'verified'=>1,'image_type'=>1])->get() as $image)
                                                {!! HTML::image($image->path,null,array('class'=>'img-responsive')) !!}
                                            @endforeach
                                        </div>
                                        <div class="caption">
                                            <h4><a href="/productfull/{{$product->id}}" target="_blank">{{$product->title}}</a></h4>
                                            <div class="description"></div>
                                            @foreach(App\User::where(['id'=>$product->imalatci_id,'confirmed'=>1])->get() as $imalatci)
                                                <div class="panel-smart pull-right">
                                                        @if($imalatci->verified==1)
                                                            <span data-user="{{$imalatci->id}}" id="span_dogrulama_{{$imalatci->id}}" class="label label-success pull-right dogrulama_span"><strong><i class="fa fa-check"></i></strong></span>
                                                        @endif
                                                        <p>
                                                            @if($imalatci->verified==1)
                                                                <div id="dogrulama_{{$imalatci->id}}" class="alert alert-warning" style="display: none">
                                                                    <i class="fa fa-star"></i> Doğrulanmış Firma
                                                                </div>
                                                            @endif
                                                            <a href="/firma/{{$imalatci->id}}" target="_blank">{{$imalatci->unvan}}</a>
                                                        </p><hr/>
                                                        <h4><span><abbr title="Firma Yetkilisi">{{$imalatci->yetkili}}</abbr></span></h4>

                                                        <div class="alert alert-info">
                                                            <p><b>Cep</b> {{$imalatci->yetkili_cep}}</p>
                                                            <hr class="divider">
                                                            <p><b>İş</b> {{$imalatci->telefon}}</p>
                                                        </div>
                                                        <p>
                                                            <abbr title="İl/İlçe">
                                                                {{App\City::find($imalatci->city_code)->city}}/
                                                                {{$imalatci->city_town==0 ? "MERKEZ" : App\CityTown::find($imalatci->city_town)->name}}
                                                            </abbr>
                                                        </p>
                                                        <div class="options">

                                                            <form action="/messages/index" method="GET">
                                                                {!! csrf_field() !!}
                                                                <input type="hidden" name="product_id" value="{{$product->id}}"/>
                                                                <div class="cart-button button-group">
                                                                    <button type="submit" class="btn btn-warning">
                                                                        <i class="fa fa-envelope-o"></i>
                                                                        Firmayla İletişime Geç
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>

                                                </div>
                                            @endforeach
                                            <div class="price">
                                                <span class="price-new">{{$product->price}} {{App\Currency::find($product->currency_id)->name}}</span>
                                            </div>
                                            <div class="cart-button button-group">
                                                <a  href="/productfull/{{$product->id}}" class="btn btn-cart" target="_blank">
                                                    Ürün Detaylarını İncele
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @elseif($ordertype==2)
                            @foreach( isset($filtered_product) && !empty($filtered_product) ? $filtered_product : (App\Product::where(['category_id'=>$category_id,'subcategory_id'=>$subcategory_id,'verified'=>1,'published'=>1])->orderBy('price','asc')->paginate(6)) as $product)
                                <div class="col-xs-12">
                                    <div class="product-col list clearfix">
                                        <div class="image">
                                            @foreach(App\ProductImage::where(['product_id'=>$product->id,'verified'=>1,'image_type'=>1])->get() as $image)
                                                {!! HTML::image($image->path,null,array('class'=>'img-responsive')) !!}
                                            @endforeach
                                        </div>
                                        <div class="caption">
                                            <h4><a href="/productfull/{{$product->id}}" target="_blank">{{$product->title}}</a></h4>
                                            @foreach(App\User::where(['id'=>$product->imalatci_id,'confirmed'=>1])->get() as $imalatci)
                                            <div class="panel-smart pull-right">
                                                    @if($imalatci->verified==1)
                                                        <span data-user="{{$imalatci->id}}" id="span_dogrulama_{{$imalatci->id}}" class="label label-success pull-right dogrulama_span"><strong><i class="fa fa-check"></i></strong></span>
                                                    @endif
                                                    <p>
                                                        @if($imalatci->verified==1)
                                                            <div id="dogrulama_{{$imalatci->id}}" class="alert alert-warning" style="display: none">
                                                                <i class="fa fa-star"></i> Doğrulanmış Firma
                                                            </div>
                                                        @endif
                                                        <a href="/firma/{{$imalatci->id}}" target="_blank">{{$imalatci->unvan}}</a>
                                                    </p><hr/>
                                                    <h4><span><abbr title="Firma Yetkilisi">{{$imalatci->yetkili}}</abbr></span></h4>

                                                    <div class="alert alert-info">
                                                        <p><b>Cep</b> {{$imalatci->yetkili_cep}}</p>
                                                        <hr class="divider">
                                                        <p><b>İş</b> {{$imalatci->telefon}}</p>
                                                    </div>
                                                    <p>
                                                        <abbr title="İl/İlçe">
                                                            {{App\City::find($imalatci->city_code)->city}}/
                                                            {{$imalatci->city_town==0 ? "MERKEZ" : App\CityTown::find($imalatci->city_town)->name}}
                                                        </abbr>
                                                    </p>
                                                    <div class="options">

                                                        <form action="/messages/index" method="GET">
                                                            {!! csrf_field() !!}
                                                            <input type="hidden" name="product_id" value="{{$product->id}}"/>
                                                            <div class="cart-button button-group">
                                                                <button type="submit" class="btn btn-warning">
                                                                    <i class="fa fa-envelope-o"></i>
                                                                    Firmayla İletişime Geç
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>

                                            </div>
                                            @endforeach
                                            <div class="price">
                                                <span class="price-new">{{$product->price}} {{App\Currency::find($product->currency_id)->name}}</span>
                                            </div>
                                            <div class="cart-button button-group">
                                                <a  href="/productfull/{{$product->id}}" class="btn btn-cart" target="_blank">
                                                    Ürün Detaylarını İncele
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @elseif($ordertype==3)
                            @foreach(isset($filtered_product) && !empty($filtered_product) ? $filtered_product : (App\Product::where(['category_id'=>$category_id,'subcategory_id'=>$subcategory_id,'verified'=>1,'published'=>1])->orderBy('price','desc')->paginate(6)) as $product)
                                <div class="col-xs-12">
                                    <div class="product-col list clearfix">
                                        <div class="image">
                                            @foreach(App\ProductImage::where(['product_id'=>$product->id,'verified'=>1,'image_type'=>1])->get() as $image)
                                                {!! HTML::image($image->path,null,array('class'=>'img-responsive')) !!}
                                            @endforeach
                                        </div>
                                        <div class="caption">
                                            <h4><a href="/productfull/{{$product->id}}" target="_blank">{{$product->title}}</a></h4>
                                            @foreach(App\User::where(['id'=>$product->imalatci_id,'confirmed'=>1])->get() as $imalatci)
                                            <div class="panel-smart pull-right">
                                                    @if($imalatci->verified==1)
                                                        <span data-user="{{$imalatci->id}}" id="span_dogrulama_{{$imalatci->id}}" class="label label-success pull-right dogrulama_span"><strong><i class="fa fa-check"></i></strong></span>
                                                    @endif
                                                    <p>
                                                        @if($imalatci->verified==1)
                                                            <div id="dogrulama_{{$imalatci->id}}" class="alert alert-warning" style="display: none">
                                                                <i class="fa fa-star"></i> Doğrulanmış Firma
                                                            </div>
                                                        @endif
                                                        <a href="/firma/{{$imalatci->id}}" target="_blank">{{$imalatci->unvan}}</a>
                                                    </p><hr/>
                                                    <h4><span><abbr title="Firma Yetkilisi">{{$imalatci->yetkili}}</abbr></span></h4>

                                                    <div class="alert alert-info">
                                                        <p><b>Cep</b> {{$imalatci->yetkili_cep}}</p>
                                                        <hr class="divider">
                                                        <p><b>İş</b> {{$imalatci->telefon}}</p>
                                                    </div>
                                                    <p>
                                                        <abbr title="İl/İlçe">
                                                            {{App\City::find($imalatci->city_code)->city}}/
                                                            {{$imalatci->city_town==0 ? "MERKEZ" : App\CityTown::find($imalatci->city_town)->name}}
                                                        </abbr>
                                                    </p>
                                                    <div class="options">

                                                        <form action="/messages/index" method="GET">
                                                            {!! csrf_field() !!}
                                                            <input type="hidden" name="product_id" value="{{$product->id}}"/>
                                                            <div class="cart-button button-group">
                                                                <button type="submit" class="btn btn-warning">
                                                                    <i class="fa fa-envelope-o"></i>
                                                                    Firmayla İletişime Geç
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>

                                            </div>
                                            @endforeach
                                            <div class="price">
                                                <span class="price-new">{{$product->price}} {{App\Currency::find($product->currency_id)->name}}</span>
                                            </div>
                                            <div class="cart-button button-group">
                                                <a  href="/productfull/{{$product->id}}" class="btn btn-cart" target="_blank">
                                                    Ürün Detaylarını İncele
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                @endif


                                        <!-- Product #1 Ends -->
                    </div>
                    @endif
                            <!-- Product List Display Ends -->

                    <!-- Product Grid Display Starts -->
                    @if($listtype==2)
                        <div class="row">
                            <!-- Product #1 Starts -->
                            @if($ordertype==1)
                                @foreach(isset($filtered_product) && !empty($filtered_product) ? $filtered_product :(App\Product::where(['category_id'=>$category_id,'subcategory_id'=>$subcategory_id,'verified'=>1,'published'=>1])->paginate(6)) as $product)
                                    <div class="col-md-4 col-sm-6">
                                        <div class="product-col">
                                            <div class="image">
                                                @foreach(App\ProductImage::where(['product_id'=>$product->id,'verified'=>1,'image_type'=>1])->get() as $image)
                                                    {!! HTML::image($image->path,null,array('class'=>'img-responsive')) !!}
                                                @endforeach
                                            </div>
                                            <div class="caption">
                                                <h4><a href="/productfull/{{$product->id}}" target="_blank">{{$product->title}}</a></h4>
                                                <div class="description">{{App\User::find($product->imalatci_id)->unvan}}</div>
                                                <div class="description">
                                                    {{App\City::find(App\User::find($product->imalatci_id)->city_code)->city}}/
                                                    {{App\User::find($product->imalatci_id)->city_town==0 ? "MERKEZ" : App\CityTown::find(App\User::find($product->imalatci_id)->city_town)->name}}
                                                </div>
                                                <div class="price">
                                                    <span class="price-new">{{$product->price}} {{App\Currency::find($product->currency_id)->name}}</span>
                                                </div>
                                                <div class="cart-button button-group">
                                                    <a  href="/productfull/{{$product->id}}" class="btn btn-cart" target="_blank">
                                                        Ürün Detaylarını İncele
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @elseif($ordertype==2)
                                @foreach( isset($filtered_product) && !empty($filtered_product) ? $filtered_product : (App\Product::where(['category_id'=>$category_id,'subcategory_id'=>$subcategory_id,'verified'=>1,'published'=>1])->orderBy('price','asc')->paginate(6)) as $product)
                                    <div class="col-md-4 col-sm-6">
                                        <div class="product-col">
                                            <div class="image">
                                                @foreach(App\ProductImage::where(['product_id'=>$product->id,'verified'=>1,'image_type'=>1])->get() as $image)
                                                    {!! HTML::image($image->path,null,array('class'=>'img-responsive')) !!}
                                                @endforeach
                                            </div>
                                            <div class="caption">
                                                <h4><a href="/productfull/{{$product->id}}" target="_blank">{{$product->title}}</a></h4>
                                                <div class="description">{{App\User::find($product->imalatci_id)->unvan}}</div>
                                                <div class="description">
                                                    {{App\City::find(App\User::find($product->imalatci_id)->city_code)->city}}/
                                                    {{App\User::find($product->imalatci_id)->city_town==0 ? "MERKEZ" : App\CityTown::find(App\User::find($product->imalatci_id)->city_town)->name}}
                                                </div>
                                                <div class="price">
                                                    <span class="price-new">{{$product->price}} {{App\Currency::find($product->currency_id)->name}}</span>
                                                </div>
                                                <div class="cart-button button-group">
                                                    <a  href="/productfull/{{$product->id}}" class="btn btn-cart" target="_blank">
                                                        Ürün Detaylarını İncele
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @elseif($ordertype==3)
                                @foreach(isset($filtered_product) && !empty($filtered_product) ? $filtered_product : (App\Product::where(['category_id'=>$category_id,'subcategory_id'=>$subcategory_id,'verified'=>1,'published'=>1])->orderBy('price','desc')->paginate(6)) as $product)
                                    <div class="col-md-4 col-sm-6">
                                        <div class="product-col">
                                            <div class="image">
                                                @foreach(App\ProductImage::where(['product_id'=>$product->id,'verified'=>1,'image_type'=>1])->get() as $image)
                                                    {!! HTML::image($image->path,null,array('class'=>'img-responsive')) !!}
                                                @endforeach
                                            </div>
                                            <div class="caption">
                                                <h4><a href="/productfull/{{$product->id}}" target="_blank">{{$product->title}}</a></h4>
                                                <div class="description">{{App\User::find($product->imalatci_id)->unvan}}</div>
                                                <div class="description">
                                                    {{App\City::find(App\User::find($product->imalatci_id)->city_code)->city}}/
                                                    {{App\User::find($product->imalatci_id)->city_town==0 ? "MERKEZ" : App\CityTown::find(App\User::find($product->imalatci_id)->city_town)->name}}
                                                </div>
                                                <div class="price">
                                                    <span class="price-new">{{$product->price}} {{App\Currency::find($product->currency_id)->name}}</span>
                                                </div>
                                                <div class="cart-button button-group">
                                                    <a  href="/productfull/{{$product->id}}" class="btn btn-cart" target="_blank">
                                                        Ürün Detaylarını İncele
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    @endif
                                            <!-- Product #1 Ends -->
                        </div>


                        @endif
                                <!-- Product Grid Display Ends -->

                        <!-- End Category Grid-->
                        <!-- Pagination & Results Starts -->
                        <div class="row">
                            <!-- Pagination Starts -->
                            <div class="col-sm-6 pagination-block">
                                {!! isset($filtered_product) ? $filtered_product->render() : App\Product::where(['category_id'=>$category_id,'subcategory_id'=>$subcategory_id,'verified'=>1,'published'=>1])->paginate(6)->render() !!}
                            </div>
                            <!-- Pagination Ends -->
                        </div>
                        <!-- Pagination & Results Ends -->
            </div>
            <!-- Primary Content Ends -->
        </div>
    </div>
    <!-- Main Container Ends -->

@endsection