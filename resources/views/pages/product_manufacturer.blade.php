@extends('app')

@section('content')

    <!-- Main Container Starts -->
    <div id="main-container" class="container">
        <div class="row">
            <!-- Sidebar Starts -->
            <div class="col-md-3">
                <!-- Categories Links Starts -->
                @include('partials.categories_sidebar')
            </div>
            <!-- Sidebar Ends -->
            <!-- Primary Content Starts -->
            <div class="col-md-9">
                <!-- Breadcrumb Starts -->
                <ol class="breadcrumb">
                    <li><a href="/">Anasayfa</a></li>
                    <li class="active"><a href="/product_manufacturer/{{$imalatci_id}}/1/1">Satıcı İlanları</a></li>
                </ol>
                <!-- Breadcrumb Ends -->
                <!-- Main Heading Starts -->
                <h2 class="main-heading2">
                    @if(File::exists(App\User::find($imalatci_id)->logo) && App\User::find($imalatci_id)->logo_verified)
                    <img src="/{{Auth::user()->logo}}" alt=""/>
                    @endif    
                    <a href="">{{App\User::find($imalatci_id)->unvan}}</a>
                </h2>
                <!-- Main Heading Ends -->

                <!-- Product Filter Starts -->
                <div class="product-filter">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="display">
                                <a href="/product_manufacturer/{{$imalatci_id}}/1/1" class="{{$listtype==1 ? 'active' : ''}}">
                                    <i class="fa fa-th-list" title="List View"></i>
                                </a>
                                <a href="/product_manufacturer/{{$imalatci_id}}/2/1" class="{{$listtype==2 ? 'active' : ''}}">
                                    <i class="fa fa-th" title="Grid View"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-2 text-right">
                            <label class="control-label">Sıralama</label>
                        </div>
                        <div class="col-md-3 text-right">
                            <select class="form-control" onchange="location = this.options[this.selectedIndex].value;">
                                <option value="" selected>----------------------------------</option>
                                <option value="/product_manufacturer/{{$imalatci_id}}/{{$listtype}}/1">Normal</option>
                                <option value="/product_manufacturer/{{$imalatci_id}}/{{$listtype}}/2">Fiyat Artan</option>
                                <option value="/product_manufacturer/{{$imalatci_id}}/{{$listtype}}/3">Fiyat Azalan</option>
                            </select>
                        </div>
                    </div>
                </div>
                <!-- Product Filter Ends -->
                <!-- Product List Display Starts -->
                @if($listtype==1)
                    <div class="row">
                        <!-- Product #1 Starts -->
                        @if($ordertype==1)
                            @foreach((App\Product::where(['imalatci_id'=>$imalatci_id,'verified'=>1,'published'=>1])->paginate(6)) as $product)

                                <div class="col-xs-12">
                                    <div class="product-col list clearfix">
                                        <div class="image">
                                            @foreach(App\ProductImage::where(['product_id'=>$product->id,'verified'=>1,'image_type'=>1])->get() as $image)
                                                {!! HTML::image($image->path,null,array('class'=>'img-responsive')) !!}
                                            @endforeach
                                        </div>
                                        <div class="caption">
                                            <h4><a href="/productfull/{{$product->id}}" target="_blank">{{$product->title}}</a></h4>
                                            <div class="description"></div>
                                            <div class="panel-smart pull-right">
                                                @foreach(App\User::where('id',$product->imalatci_id)->get() as $imalatci)
                                                    <p><a href="">{{$imalatci->unvan}}</a></p><hr/>
                                                    <h4><span><abbr title="Firma Yetkilisi">{{$imalatci->yetkili}}</abbr></span></h4>

                                                    <div class="alert alert-info">
                                                        <p><b>Cep</b> {{$imalatci->yetkili_cep}}</p>
                                                        <hr class="divider">
                                                        <p><b>İş</b> {{$imalatci->telefon}}</p>
                                                    </div>
                                                    <p>
                                                        <abbr title="İl/İlçe">
                                                            {{App\City::find($imalatci->city_code)->city}}/
                                                            {{$imalatci->city_town==0 ? "MERKEZ" : App\CityTown::find($imalatci->city_town)->name}}
                                                        </abbr>
                                                    </p>
                                                    <div class="options">

                                                        <form action="/messages/index" method="GET">
                                                            {!! csrf_field() !!}
                                                            <input type="hidden" name="product_id" value="{{$product->id}}"/>
                                                            <div class="cart-button button-group">
                                                                <button type="submit" class="btn btn-warning">
                                                                    <i class="fa fa-envelope-o"></i>
                                                                    Firmayla İletişime Geç
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <div class="price">
                                                <span class="price-new">{{$product->price}} {{App\Currency::find($product->currency_id)->name}}</span>
                                            </div>
                                            <div class="cart-button button-group">
                                                <a  href="/productfull/{{$product->id}}" class="btn btn-cart" target="_blank">
                                                    Ürün Detaylarını İncele
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @elseif($ordertype==2)
                            @foreach( (App\Product::where(['imalatci_id'=>$imalatci_id,'verified'=>1,'published'=>1])->orderBy('price','asc')->paginate(6)) as $product)
                                <div class="col-xs-12">
                                    <div class="product-col list clearfix">
                                        <div class="image">
                                            @foreach(App\ProductImage::where(['product_id'=>$product->id,'verified'=>1,'image_type'=>1])->get() as $image)
                                                {!! HTML::image($image->path,null,array('class'=>'img-responsive')) !!}
                                            @endforeach
                                        </div>
                                        <div class="caption">
                                            <h4><a href="/productfull/{{$product->id}}" target="_blank">{{$product->title}}</a></h4>
                                            <div class="panel-smart pull-right">
                                                @foreach(App\User::where('id',$product->imalatci_id)->get() as $imalatci)
                                                    <p><a href="">{{$imalatci->unvan}}</a></p><hr/>
                                                    <h4><span><abbr title="Firma Yetkilisi">{{$imalatci->yetkili}}</abbr></span></h4>

                                                    <div class="alert alert-info">
                                                        <p><b>Cep</b> {{$imalatci->yetkili_cep}}</p>
                                                        <hr class="divider">
                                                        <p><b>İş</b> {{$imalatci->telefon}}</p>
                                                    </div>
                                                    <p>
                                                        <abbr title="İl/İlçe">
                                                            {{App\City::find($imalatci->city_code)->city}}/
                                                            {{$imalatci->city_town==0 ? "MERKEZ" : App\CityTown::find($imalatci->city_town)->name}}
                                                        </abbr>
                                                    </p>
                                                    <p><a href="product_manufacturer/{{$imalatci->id}}" target="_blank">Satıcının Tüm İlanları</a></p>
                                                    <div class="options">

                                                        <form action="/messages/index" method="GET">
                                                            {!! csrf_field() !!}
                                                            <input type="hidden" name="product_id" value="{{$product->id}}"/>
                                                            <div class="cart-button button-group">
                                                                <button type="submit" class="btn btn-warning">
                                                                    <i class="fa fa-envelope-o"></i>
                                                                    Firmayla İletişime Geç
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <div class="price">
                                                <span class="price-new">{{$product->price}} {{App\Currency::find($product->currency_id)->name}}</span>
                                            </div>
                                            <div class="cart-button button-group">
                                                <a  href="/productfull/{{$product->id}}" class="btn btn-cart" target="_blank">
                                                    Ürün Detaylarını İncele
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @elseif($ordertype==3)
                            @foreach((App\Product::where(['imalatci_id'=>$imalatci_id,'verified'=>1,'published'=>1])->orderBy('price','desc')->paginate(6)) as $product)
                                <div class="col-xs-12">
                                    <div class="product-col list clearfix">
                                        <div class="image">
                                            @foreach(App\ProductImage::where(['product_id'=>$product->id,'verified'=>1,'image_type'=>1])->get() as $image)
                                                {!! HTML::image($image->path,null,array('class'=>'img-responsive')) !!}
                                            @endforeach
                                        </div>
                                        <div class="caption">
                                            <h4><a href="/productfull/{{$product->id}}" target="_blank">{{$product->title}}</a></h4>
                                            <div class="panel-smart pull-right">
                                                @foreach(App\User::where('id',$product->imalatci_id)->get() as $imalatci)
                                                    <p><a href="">{{$imalatci->unvan}}</a></p><hr/>
                                                    <h4><span><abbr title="Firma Yetkilisi">{{$imalatci->yetkili}}</abbr></span></h4>

                                                    <div class="alert alert-info">
                                                        <p><b>Cep</b> {{$imalatci->yetkili_cep}}</p>
                                                        <hr class="divider">
                                                        <p><b>İş</b> {{$imalatci->telefon}}</p>
                                                    </div>
                                                    <p>
                                                        <abbr title="İl/İlçe">
                                                            {{App\City::find($imalatci->city_code)->city}}/
                                                            {{$imalatci->city_town==0 ? "MERKEZ" : App\CityTown::find($imalatci->city_town)->name}}
                                                        </abbr>
                                                    </p>
                                                    <div class="options">

                                                        <form action="/messages/index" method="GET">
                                                            {!! csrf_field() !!}
                                                            <input type="hidden" name="product_id" value="{{$product->id}}"/>
                                                            <div class="cart-button button-group">
                                                                <button type="submit" class="btn btn-warning">
                                                                    <i class="fa fa-envelope-o"></i>
                                                                    Firmayla İletişime Geç
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <div class="price">
                                                <span class="price-new">{{$product->price}} {{App\Currency::find($product->currency_id)->name}}</span>
                                            </div>
                                            <div class="cart-button button-group">
                                                <a  href="/productfull/{{$product->id}}" class="btn btn-cart" target="_blank">
                                                    Ürün Detaylarını İncele
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                @endif


                                        <!-- Product #1 Ends -->
                    </div>
                    @endif
                            <!-- Product List Display Ends -->

                    <!-- Product Grid Display Starts -->
                    @if($listtype==2)
                        <div class="row">
                            <!-- Product #1 Starts -->
                            @if($ordertype==1)
                                @foreach((App\Product::where(['imalatci_id'=>$imalatci_id,'verified'=>1,'published'=>1])->paginate(6)) as $product)
                                    <div class="col-md-4 col-sm-6">
                                        <div class="product-col">
                                            <div class="image">
                                                @foreach(App\ProductImage::where(['product_id'=>$product->id,'verified'=>1,'image_type'=>1])->get() as $image)
                                                    {!! HTML::image($image->path,null,array('class'=>'img-responsive')) !!}
                                                @endforeach
                                            </div>
                                            <div class="caption">
                                                <h4><a href="/productfull/{{$product->id}}" target="_blank">{{$product->title}}</a></h4>
                                                <div class="description">{{App\User::find($product->imalatci_id)->unvan}}</div>
                                                <div class="description">
                                                    {{App\City::find(App\User::find($product->imalatci_id)->city_code)->city}}/
                                                    {{App\User::find($product->imalatci_id)->city_town==0 ? "MERKEZ" : App\CityTown::find(App\User::find($product->imalatci_id)->city_town)->name}}
                                                </div>
                                                <div class="price">
                                                    <span class="price-new">{{$product->price}} {{App\Currency::find($product->currency_id)->name}}</span>
                                                </div>
                                                <div class="cart-button button-group">
                                                    <a  href="/productfull/{{$product->id}}" class="btn btn-cart" target="_blank">
                                                        Ürün Detaylarını İncele
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @elseif($ordertype==2)
                                @foreach((App\Product::where(['imalatci_id'=>$imalatci_id,'verified'=>1,'published'=>1])->orderBy('price','asc')->paginate(6)) as $product)
                                    <div class="col-md-4 col-sm-6">
                                        <div class="product-col">
                                            <div class="image">
                                                @foreach(App\ProductImage::where(['product_id'=>$product->id,'verified'=>1,'image_type'=>1])->get() as $image)
                                                    {!! HTML::image($image->path,null,array('class'=>'img-responsive')) !!}
                                                @endforeach
                                            </div>
                                            <div class="caption">
                                                <h4><a href="/productfull/{{$product->id}}" target="_blank">{{$product->title}}</a></h4>
                                                <div class="description">{{App\User::find($product->imalatci_id)->unvan}}</div>
                                                <div class="description">
                                                    {{App\City::find(App\User::find($product->imalatci_id)->city_code)->city}}/
                                                    {{App\User::find($product->imalatci_id)->city_town==0 ? "MERKEZ" : App\CityTown::find(App\User::find($product->imalatci_id)->city_town)->name}}
                                                </div>
                                                <div class="price">
                                                    <span class="price-new">{{$product->price}} {{App\Currency::find($product->currency_id)->name}}</span>
                                                </div>
                                                <div class="cart-button button-group">
                                                    <a  href="/productfull/{{$product->id}}" class="btn btn-cart" target="_blank">
                                                        Ürün Detaylarını İncele
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @elseif($ordertype==3)
                                @foreach((App\Product::where(['imalatci_id'=>$imalatci_id,'verified'=>1,'published'=>1])->orderBy('price','desc')->paginate(6)) as $product)
                                    <div class="col-md-4 col-sm-6">
                                        <div class="product-col">
                                            <div class="image">
                                                @foreach(App\ProductImage::where(['product_id'=>$product->id,'verified'=>1,'image_type'=>1])->get() as $image)
                                                    {!! HTML::image($image->path,null,array('class'=>'img-responsive')) !!}
                                                @endforeach
                                            </div>
                                            <div class="caption">
                                                <h4><a href="/productfull/{{$product->id}}" target="_blank">{{$product->title}}</a></h4>
                                                <div class="description">{{App\User::find($product->imalatci_id)->unvan}}</div>
                                                <div class="description">
                                                    {{App\City::find(App\User::find($product->imalatci_id)->city_code)->city}}/
                                                    {{App\User::find($product->imalatci_id)->city_town==0 ? "MERKEZ" : App\CityTown::find(App\User::find($product->imalatci_id)->city_town)->name}}
                                                </div>
                                                <div class="price">
                                                    <span class="price-new">{{$product->price}} {{App\Currency::find($product->currency_id)->name}}</span>
                                                </div>
                                                <div class="cart-button button-group">
                                                    <a  href="/productfull/{{$product->id}}" class="btn btn-cart" target="_blank">
                                                        Ürün Detaylarını İncele
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    @endif
                                            <!-- Product #1 Ends -->
                        </div>


                        @endif
                                <!-- Product Grid Display Ends -->

                        <!-- End Category Grid-->
                        <!-- Pagination & Results Starts -->
                        <div class="row">
                            <!-- Pagination Starts -->
                            <div class="col-sm-6 pagination-block">
                                {!!  App\Product::where(['imalatci_id'=>$imalatci_id,'verified'=>1,'published'=>1])->paginate(6)->render() !!}
                            </div>
                            <!-- Pagination Ends -->
                        </div>
                        <!-- Pagination & Results Ends -->
            </div>
            <!-- Primary Content Ends -->
        </div>
    </div>
    <!-- Main Container Ends -->

@endsection