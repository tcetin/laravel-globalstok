
<div id="container">
    <div class="row product-info full">
        <h2>Şikayet/Tavsiye</h2>
        <div class="col-sm-12 product-details">
            <div class="panel-smart">
                <h2>Gönderici Bilgisi</h2>
                <ul class="list-unstyled manufacturer">
                    @foreach(App\User::where('id',$perakendeci_id)->get() as $perakendeci)
                        <li><span>Ad Soyad:{{$perakendeci->yetkili}}</span></li>
                        <li><span>Eposta:{{$perakendeci->email}}</span></li>
                        <li><span>Telefon:{{$perakendeci->telefon}}</span></li>
                        <li><span>İl/İlçe:
                                {{App\City::find($perakendeci->city_code)->city}}/
                                {{$perakendeci->city_town==0 ? "MERKEZ" : App\CityTown::find($perakendeci->city_town)->name}}
                                </span>
                        </li>
                        <li><span>Adres:{{$perakendeci->adres}}</span></li>
                    @endforeach
                </ul>

                <h2>İmalatçı/Tedarikçi Bilgisi</h2>
                <ul class="list-unstyled manufacturer">
                    @foreach(App\User::where('id',$imalatci_id)->get() as $imalatci)
                        <li><span>Ad Soyad:{{$imalatci->yetkili}}</span></li>
                        <li><span>Eposta:{{$imalatci->email}}</span></li>
                        <li><span>Telefon:{{$imalatci->telefon}}</span></li>
                        <li><span>İl/İlçe:
                                {{App\City::find($imalatci->city_code)->city}}/
                                {{$imalatci->city_town==0 ? "MERKEZ" : App\CityTown::find($perakendeci->city_town)->name}}
                                </span>
                        </li>
                        <li><span>Adres:{{$imalatci->adres}}</span></li>
                    @endforeach
                </ul>
                <hr />
                <h2>Şikayet / Tavsiye Mesajı</h2>
                {!!$mesaj!!}
                <!-- Available Options Starts -->
                <!-- Available Options Ends -->
            </div>
        </div>
        <!-- Right Ends -->
    </div>
</div>
