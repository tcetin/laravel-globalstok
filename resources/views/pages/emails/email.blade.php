

<div id="container">
    <div class="row product-info full">
        <!-- Right Starts -->
        @foreach(App\Product::where('id',$product_id)->get() as $product)
            <div class="col-sm-8 product-details">
                <div class="panel-smart">
                    <!-- Product Name Starts -->
                    <h2>{{$product->title}}</h2>
                    <!-- Product Name Ends -->
                    <hr />
                    <!-- Manufacturer Starts -->
                    <ul class="list-unstyled manufacturer">
                        @foreach(App\User::where('id',$product->imalatci_id)->get() as $imalatci)
                            <li>
                                <span>Firma:</span> <a href="firma/{{$imalatci->id}}/{{$imalatci->unvan}}" target="_blank">{{$imalatci->unvan}}</a>
                            </li>
                            <li><span>Ürün Kategorisi:</span>{{App\Category::find($product->category_id)->name}} /{{App\SubCategory::find($product->subcategory_id)->name}}</li>
                        @endforeach

                        <li>
                            <div class="price">
                                <span class="price-head">Ürün Fiyatı :</span>
                                <span class="price-new">{{$product->price}} {{ App\Currency::find($product->currency_id)->name }}</span>
                                <!--<span class="price-old">$249.50</span>-->
                            </div>
                        </li>
                         <li>
                               <span>Ödeme Şekli:
                                   @foreach($product->payment_methods as $payment)
                                       <strong class="label label-info">{{$payment->name}}</strong>
                                   @endforeach
                             </span>
                         </li>
                        <li>
                            <span>Almak İstenilen Miktar:{{$urun_miktar}} {{App\AmountType::find($product->amount_type)->name}}</span>
                        </li>
                        <li>
                            <span>Perakendecinin Ödeyeceği Tutar: {{$odenecek}} {{ App\Currency::find($product->currency_id)->name }}</span>
                        </li>
                    </ul>
                    <!-- Manufacturer Ends -->
                    <hr />
                    <h2>Perakendeci Bilgisi</h2>
                    <ul class="list-unstyled manufacturer">
                        @foreach(App\User::where('id',$perakendeci_id)->get() as $perakendeci)
                        <li><span>Ad Soyad:{{$perakendeci->yetkili}}</span></li>
                        <li><span>Eposta:{{$perakendeci->email}}</span></li>
                        <li><span>Telefon:{{$perakendeci->telefon}}</span></li>
                        <li><span>İl/İlçe:
                                {{App\City::find($perakendeci->city_code)->city}}/
                                {{$perakendeci->city_town==0 ? "MERKEZ" : App\CityTown::find($perakendeci->city_town)->name}}
                                </span>
                        </li>
                        <li><span>Adres:{{$perakendeci->adres}}</span></li>
                        @endforeach
                    </ul>
                    <hr />
                    <h2>Perakendeci Mesajı</h2>
                    {!!$mesaj!!}
                    <!-- Available Options Starts -->
                                <!-- Available Options Ends -->
                </div>
            </div>
            @endforeach
                    <!-- Right Ends -->
    </div>
</div>
