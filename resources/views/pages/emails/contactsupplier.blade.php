@extends('app')
@section('content')

    <div class="container">

     <div class="row">

        <div class="col-md-3">
            @include('partials.categories_sidebar')
        </div>

        <div class="col-md-9">

            <ol class="breadcrumb">
                <li><a href="/">Anasayfa</a></li>
                <li class="active"><a href="/messages">Firmayla İletişime Geç</a></li>
            </ol>

            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                </div>
            </div>

        @foreach(App\Product::where('id',$product_id)->get() as $product)
            <form action="/messages/sendmessage"  role="form" method="post">
                {!! csrf_field();!!}
            <div class="panel panel-smart">
                <div class="panel-heading">
                   <p> <i class="fa fa-user"></i> {{App\User::find($product->imalatci_id)->yetkili}} | <a href="" target="_blank">{{App\User::find($product->imalatci_id)->unvan}}</a> </p>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-4">
                        @foreach(App\ProductImage::where(['product_id'=>$product_id,'image_type'=>1,'verified'=>1])->get() as $img)
                            <a href="/{{$img->path}}">
                                <img src="/{{$img->path}}" alt="" class="img-responsive thumbnail" style="max-height: 200px;max-width: 200px" />
                            </a>
                        @endforeach
                    </div>
                    <div class="col-sm-8">
                        <h5><label>Ürün:</label> <a href="/productfull/{{$product->id}}" target="_blank">{{$product->title}}</a></h5>
                        <div class="price">
                            <h5><label>Ürün Fiyatı :</label>
                            <span class="price-new">{{$product->price}} {{ App\Currency::find($product->currency_id)->name }}</span>
                            </h5>
                        </div>

                          <h5><label>Ürün Stok Miktarı:<span class="badge">{{$product->amount}} {{App\AmountType::find($product->amount_type)->name}}</span></label></h5>


                            <h5><label>Miktar({{App\AmountType::find($product->amount_type)->name}}):</label></h5>
                            <div class="input-group">
                                  <span class="input-group-btn">
                                      <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[2]">
                                          <span class="glyphicon glyphicon-minus"></span>
                                      </button>
                                  </span>
                                    <input type="text" name="quant[2]" id="quant" class="form-control input-number" data-tutar="{{$product->price}}" value="1" min="1" max="{{$product->amount}}">
                                  <span class="input-group-btn">
                                      <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[2]">
                                          <span class="glyphicon glyphicon-plus"></span>
                                      </button>
                                  </span>
                            </div>

                        <h5><label>Asgari Sipariş Miktarı:<span class="badge">{{$product->min_order}}</span></label></h5>
                        <h5>
                            <span>Ödeme Şekli:
                                @foreach($product->payment_methods as $payment)
                                    <strong class="label label-info">{{$payment->name}}</strong>
                                @endforeach
                             </span>
                        </h5>


                        <div class="price">
                               <h5><span class="price-head"><label>Alış Tutarı :</label></span>
                                <span id="tutar" class="price-new">{{$product->price}}</span><span class="price-new">{{ App\Currency::find($product->currency_id)->name }}</span>
                                <input type="hidden" name="odenecek" id="odenecek" value="{{$product->price}}" />
                               </h5>
                         </div>

                    </div>
                </div>
                <hr class="divider"/>
                <div class="panel-body">


                        <input type="hidden" name="imalatci_email" value="{{App\User::find($product->imalatci_id)->email}}"/>
                        <input type="hidden" name="product_id" value="{{$product->id}}"/>
                        <input type="hidden" name="perakendeci_id" value="{{Auth::user()->id}}"/>


                        <div class="form-group">
                            <label for="email" class="control-label">Perakendeci Eposta</label>
                             <input type="email" class="form-control" name="email" id="email" value="{{Auth::user()->email}}" readonly>
                        </div>

                        <div class="form-group">
                            <label for="email" class="control-label">Konu</label>
                            <input type="text" name="subject" class="form-control" value="{{$product->title}} ürününüzü satın almak istiyorum."/>
                        </div>

                        <div style="display: none">
                            <textarea name="msg" id="msg"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="email" class="control-label">Mesaj</label>
                            <textarea class="summernote" name="mesaj" id="mesaj" cols="30" rows="10"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="email" class="control-label">
                                <input type="checkbox" name="kartvizit" checked/> <abbr title="Ad Soyad,Email ve Adres Bilgileri"><b>Kartvizit</b></abbr> bilgilerimi imalatçı/tedarikçiyle paylaşmayı kabul ediyorum.
                            </label>

                        </div>


                        <div class="price pull-right">
                            <p class="alert alert-warning"><span class="price-head"><label>Tutar :</label></span>
                                <span id="tutar" class="price-new">{{$product->price}}</span><span class="price-new">{{ App\Currency::find($product->currency_id)->name }}</span>
                                <input type="hidden" name="odenecek" id="odenecek" value="{{$product->price}}" />
                                <input type="hidden" name="currency" value="{{ App\Currency::find($product->currency_id)->name }}"/>
                            </p>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-warning text-uppercase pull-right">
                                    <i class="fa fa-envelope-o"></i>
                                    Mesajı Firmaya İlet
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @endforeach
        </div>
        <!-- Contact Form Ends -->
    </div>
    </div>
@endsection
