@extends('master_admin')

@section('content')

    <!-- Page heading -->
    <div class="page-head">
        <!-- Page heading -->
        <h2 class="pull-left">Dashboard</h2>

        <div class="clearfix"></div>
    </div><!--/ Page heading ends -->

    <div class="matter"  style="max-width: 1200px;margin-left: 50px;">

            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center">Genel Veriler</h2><hr>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <!-- List starts -->
                    <ul class="today-datas">

                        <li class="bgreen">
                            <!-- Graph -->
                            <div class="pull-left"><i class="fa fa-group"></i></div>
                            <!-- Text -->
                            <div class="datas-text pull-right"><span class="bold">{{  App\User::all()->count()}}</span> Toplam Kullanıcı</div>

                            <div class="clearfix"></div>
                        </li>

                        <!-- List #1 -->
                        <li class="bgreen">
                            <!-- Graph -->
                            <div class="pull-left"><i class="fa fa-group"></i></div>
                            <!-- Text -->
                            <div class="datas-text pull-right"><span class="bold">{{  App\User::where('rol',2)->count()}}</span> Toplam İmalatçı</div>

                            <div class="clearfix"></div>
                        </li>
                        <li class="bgreen">
                            <!-- Graph -->
                            <div class="pull-left"><i class="fa fa-group"></i></div>
                            <!-- Text -->
                            <div class="datas-text pull-right"><span class="bold">{{  App\User::where(['rol'=>2,'verified'=>1])->count()}}</span> Onaylanan İmalatçı</div>

                            <div class="clearfix"></div>
                        </li>
                        <li class="bgreen">
                            <!-- Graph -->
                            <div class="pull-left"><i class="fa fa-group"></i></div>
                            <!-- Text -->
                            <div class="datas-text pull-right"><span class="bold">{{  App\User::where('rol',3)->count()}}</span> Toplam Tedarikçi</div>

                            <div class="clearfix"></div>
                        </li>
                        <li class="bgreen">
                            <!-- Graph -->
                            <div class="pull-left"><i class="fa fa-group"></i></div>
                            <!-- Text -->
                            <div class="datas-text pull-right"><span class="bold">{{  App\User::where(['rol'=>3,'verified'=>1])->count()}}</span> Onaylanan Tedarikçi</div>

                            <div class="clearfix"></div>
                        </li>


                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <!-- List starts -->
                    <ul class="today-datas">

                        <li class="blightblue">
                            <!-- Graph -->
                            <div class="pull-left"><i class="fa fa-bars"></i></div>
                            <!-- Text -->
                            <div class="datas-text pull-right"><span class="bold">{{App\Product::all()->count()}}</span> Toplam Ürün</div>

                            <div class="clearfix"></div>
                        </li>
                        <li class="blightblue">
                            <!-- Graph -->
                            <div class="pull-left"><i class="fa fa-check-square-o"></i></div>
                            <!-- Text -->
                            <div class="datas-text pull-right"><span class="bold">{{App\Product::where('verified',1)->count()}}</span> Onaylanan Ürün</div>

                            <div class="clearfix"></div>
                        </li>
                        <li class="bred">
                            <!-- Graph -->
                            <div class="pull-left"><i class="fa fa-minus-square"></i></div>
                            <!-- Text -->
                            <div class="datas-text pull-right"><span class="bold">{{App\Product::where('verified',0)->count()}}</span> Onay Bekleyen Ürün</div>

                            <div class="clearfix"></div>
                        </li>



                        <!-- List #1 -->
                        <li class="bgreen">
                            <!-- Graph -->
                            <div class="pull-left"><i class="fa fa-check-square-o"></i></div>
                            <!-- Text -->
                            <div class="datas-text pull-right"><span class="bold">{{  App\Product::where('published',1)->count()}}</span> Yayınlanan Ürün</div>

                            <div class="clearfix"></div>
                        </li>
                        <li class="bred">
                            <!-- Graph -->
                            <div class="pull-left"><i class="fa fa-minus-square"></i></div>
                            <!-- Text -->
                            <div class="datas-text pull-right"><span class="bold">{{  App\Product::where('published',0)->count()}}</span> Yayınlanmayan Ürün</div>

                            <div class="clearfix"></div>
                        </li>

                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center">Kategori Verileri</h2><hr>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                <div id="kategoriler"></div>
                </div>
            </div>
    </div>

    <!--highcharts-->
    {!! HTML::script('//code.jquery.com/jquery-1.11.3.min.js')!!}




    <script>
        $(function () {
            // Create the chart
            $('#kategoriler').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Kategorilere Göre ürün Sayıları'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: 'Sayılar'
                    }

                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y}'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
                },

                series: [{
                    name: 'Ürün',
                    colorByPoint: true,
                    data: {!!json_encode($data)!!}
                }],
                drilldown: {
                    series: {!!json_encode($drill)!!}
                }
            });
        });
    </script>


@endsection