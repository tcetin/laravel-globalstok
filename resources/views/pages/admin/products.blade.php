@extends('master_admin')

@section('content')

    <!-- Page heading -->
    <div class="page-head">
        <!-- Page heading -->
        <h2 class="pull-left">Ürün İşlemleri</h2>

        <div class="clearfix"></div>
    </div><!--/ Page heading ends -->

    <div class="matter" style="max-width: 1200px;margin-left: 50px;">

            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="widget wred">
                        <div class="widget-head">
                            <div class="pull-left">Ürünler</div>
                            <div class="widget-icons pull-right">
                                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="widget-content">
                            <div class="table-responsive">
                                <table class="table table-bordered ">
                                    <thead>
                                    <tr>
                                       <th>Ürün Başlığı</th>
                                       <th>Ürün Açıklaması</th>
                                       <th>Ürün Fiyatı</th>
                                       <th>Ürünün Miktarı</th>
                                       <th>Ürün Kategorisi</th>
                                       <th>Ürün Alt Kategorisi</th>
                                       <th>Ürünün İmalatçı Firması</th>
                                       <th>Ürünün Stok Durumu</th>
                                       <th>Ürünün Onay Durumu</th>
                                       <th>Ürünün Yayınlanma Durumu</th>
                                        <th>Vitrinde Yayınlanma Durumu </th>
                                       <th>Ürünün Oluşturulma Tarihi</th>
                                       <th>Ürünün En Son Güncellenme Tarihi</th>
                                       <th>Ürün İle İlgili Resimler</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach(App\Product::all() as $product)
                                        <tr>
                                            <td>{{$product->title}}</td>
                                            <td>{{$product->description}}</td>
                                            <td>{{$product->price}}({{App\Currency::find($product->currency_id)->name}})</td>
                                            <td>{{ $product->amount}} {{ App\AmountType::find($product->amount_type)->name }}</td>
                                            <td>{{ App\Category::find($product->category_id)->name }}</td>
                                            <td>{{ App\SubCategory::find($product->subcategory_id)->name }}</td>
                                            <td>{{ App\User::find($product->imalatci_id)->unvan }}</td>
                                            <td>
                                                {{ $product->avaliability == 1 ? "Stokta Var" : "Stokta Yok" }}
                                            </td>
                                            
                                             @if($product->verified == 1)
                                                <td>
                                                    <div class="alert alert-success"><i class="fa fa-check"></i>Onaylı</div>
                                                    <form action="/admin/unverifyproduct" method="POST">
                                                        {!! csrf_field()!!}
                                                        <input type="hidden" name="product_id" value="{{$product->id}}"/>
                                                        <button type="submit" class="btn btn-danger btn-xs">Onayı Kaldır</button>
                                                    </form>
                                                </td>
                                             @else
                                                <td>
                                                    <div class="alert alert-danger">Onay Bekliyor</div>
                                                    <form action="/admin/verifyproduct" method="POST">
                                                        {!! csrf_field()!!}
                                                        <input type="hidden" name="product_id" value="{{$product->id}}"/>
                                                        <button type="submit" class="btn btn-success btn-xs">Onayla</button>
                                                    </form>
                                                </td>
                                             @endif

                                            @if($product->published == 1)
                                                <td>
                                                    <div class="alert alert-success"><i class="fa fa-check"></i>Yayında</div>
                                                    <form action="/admin/unpublishedproduct" method="POST">
                                                        {!! csrf_field()!!}
                                                        <input type="hidden" name="product_id" value="{{$product->id}}"/>
                                                        <button type="submit" class="btn btn-danger btn-xs">Yayından Kaldır</button>
                                                    </form>
                                                </td>
                                            @else
                                                <td>
                                                    <div class="alert alert-danger">Yayında Değil</div>
                                                    <form action="/admin/publishproduct" method="POST">
                                                        {!! csrf_field()!!}
                                                        <input type="hidden" name="product_id" value="{{$product->id}}"/>
                                                        <button type="submit" class="btn btn-success btn-xs">Yayınla</button>
                                                    </form>
                                                </td>
                                            @endif

                                            @if($product->vitrin== 1)
                                                <td>
                                                    <div class="alert alert-success"><i class="fa fa-check"></i>Vitrinde Yayınlanıyor</div>
                                                    <form action="/admin/unvitrinproduct" method="POST">
                                                        {!! csrf_field()!!}
                                                        <input type="hidden" name="product_id" value="{{$product->id}}"/>
                                                        <button type="submit" class="btn btn-danger btn-xs">Yayından Kaldır</button>
                                                    </form>
                                                </td>
                                            @else
                                                <td>
                                                    <div class="alert alert-danger">Vitrinde Yayınlanmıyor</div>
                                                    <form action="/admin/vitrinproduct" method="POST">
                                                        {!! csrf_field()!!}
                                                        <input type="hidden" name="product_id" value="{{$product->id}}"/>
                                                        <button type="submit" class="btn btn-success btn-xs">Vitrinde Yayınla</button>
                                                    </form>
                                                </td>
                                            @endif
                                            
                                            <td>{{$product->created_at}}</td>
                                            <td>{{$product->updated_at}}</td>
                                            <td>
                                                <button data-target="#product_{{$product->id}}" data-toggle="modal" class="btn btn-success btn-xs">Resimler</button>
                                                <div id="product_{{$product->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                								<div class="modal-dialog">
                                                								  <div class="modal-content">
                                                								  <div class="modal-header">
                                                									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                									<h4 class="modal-title">{{$product->title}}</h4>
                                                								  </div>
                                                								  <div class="modal-body">

                                                                                      <form method="post" class="form-horizontal">
                                                                                          {!! csrf_field() !!}

                                                                                       <h2>Ürünün Ana Resmi</h2>
                                                                                       <div class="form-group">
                                                                                              @foreach(App\ProductImage::where(['product_id'=>$product->id,'image_type'=>1])->get() as $image)
                                                                                                   {!! HTML::image($image->path,null,array('class'=>'col-sm-6')) !!}
                                                                                                   @if($image->verified==1)
                                                                                                       <div class="col-sm-6">
                                                                                                        <div data-resim="{{$image->id}}" class="alert alert-success"><i class="fa fa-check"></i> Onaylı Resim</div>
                                                                                                        <button type="button" data-resim="{{$image->id}}" class="btn btn-danger btn-xs resim_onay_kaldir">Onayı Kaldır</button>
                                                                                                       </div>
                                                                                                   @else
                                                                                                   <div class="col-sm-6">
                                                                                                       <div data-resim="{{$image->id}}" class="alert alert-danger"><i class="fa fa-times"></i> Resim Onay Bekliyor</div>
                                                                                                       <button type="button" data-resim="{{$image->id}}" class="btn btn-success btn-xs resim_onayla">Onayla</button>
                                                                                                   </div>
                                                                                                   @endif
                                                                                               @endforeach
                                                                                       </div>

                                                                                       <h2>Ürün İle İlgili Diğer Resimler</h2>
                                                                                          <div class="form-group">

                                                                                                  @foreach(App\ProductImage::where(['product_id'=>$product->id,'image_type'=>2])->get() as $image)
                                                                                                  <div class="row">
                                                                                                      <div class="col-sm-6">
                                                                                                          {!! HTML::image($image->path,null,array('class'=>'img-thumbnail')) !!}
                                                                                                      </div>
                                                                                                      <div class="col-sm-6">
                                                                                                        @if($image->verified==1)
                                                                                                                  <div data-resim="{{$image->id}}" class="alert alert-success"><i class="fa fa-check"></i> Onaylı Resim</div>
                                                                                                                  <button type="button" data-resim="{{$image->id}}" class="btn btn-danger btn-xs resim_onay_kaldir">Onayı Kaldır</button>

                                                                                                          @else
                                                                                                                  <div data-resim="{{$image->id}}" class="alert alert-danger"><i class="fa fa-times"></i> Resim Onay Bekliyor</div>
                                                                                                                  <button type="button" data-resim="{{$image->id}}" class="btn btn-success btn-xs resim_onayla">Onayla</button>

                                                                                                          @endif
                                                                                                      </div>
                                                                                                  </div>

                                                                                                  @endforeach

                                                                                          </div>
                                                                                      </form>

                                                                                  </div>
                                                								  <div class="modal-footer">
                                                									<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Kapat</button>
                                                								  </div>
                                                								</div>
                                                								</div>
                                                								</div>
                                            </td>        
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>
            </div>

    </div>

@endsection