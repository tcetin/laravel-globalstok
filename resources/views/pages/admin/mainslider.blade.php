@extends('master_admin')

@section('content')

        <!--Slider Durumu-->

        <div class="row">
            <div class="col-md-12">

                <h2>Ana Slider Yönetimi</h2><hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include('flash::message')
            </div>
        </div>

        <div class="container">

            @if(App\MainsliderStatu::count() > 0)
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-info">

                                @foreach($mainslider_statu as $sl)
                                    @if($sl->statu==1)
                                        Slider Yayınlanma Durumu Aktif.
                                    @else
                                        Slider Yayınlanma Durumu Pasif.
                                    @endif
                                @endforeach

                        </div>
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    @if($errors->has())
                        <div id="form-errors">
                            <p>Bazı Hatalar Oluştu:</p>
                            <ul>
                                @foreach($errors->all() as $error)
                                    <div class="alert alert-danger">{{ $error }}</div>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <h2>Slider Durumu</h2><hr>
                    {!! Form::open(array('url'=>'admin/mainslidersliderstatu','class'=>'form-horizontal')) !!}
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Slider Durumu</label>
                        <div class="col-sm-10">
                           {!! Form::select('statu',['1'=>'Aktif','0'=>'Pasif'],null,['class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                             {!! Form::button('Slider Durumunu Kaydet', array('type' => 'submit', 'class' => 'btn btn-info'))!!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <h2>Yeni Slide</h2><hr>
                </div>
            </div>

               <div class="row">
                   <div class="col-md-12">
                    {!! Form::open(array('url'=>'admin/mainslidercreate','class'=>'form-horizontal','files'=>true)) !!}

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Slide Başlığı</label>
                            <div class="col-sm-10">
                                {!! Form::text('baslik',null,array('class'=>'form-control','placeholder'=>'Slide Başlığı')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Slide Metni</label>
                            <div class="col-sm-10">
                                {!! Form::textarea('metin',null,array('class'=>'form-control','placeholder'=>'Slide Metni')) !!}
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Resim Ekle</label>
                            <div class="col-sm-10">
                                {!! Form::file('image','',array('class'=>'form-control')) !!}
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Devamını Oku Buton Texti</label>
                            <div class="col-sm-10">
                                {!! Form::text('read_more',null,array('class'=>'form-control','placeholder'=>'Devamını Oku Buton textini giriniz')) !!}
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Kaydet</button>
                            </div>
                        </div>
                    {!! Form::close() !!}

                   </div>

                </div>

                <div class="row">
                    <div class="col-md-12">
                        <h2>Eklenen Slidelar</h2><hr>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion">

                            @foreach($mainslider as $slider)

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#{{ $slider->id }}">
                                            {{ $slider->baslik }}
                                        </a>
                                    </h4>
                                    {!! Form::open(array('url'=>'admin/mainsliderdestroy','class'=>'form-inline','style'=>'text-align: right;')) !!}
                                    {!! Form::hidden('id',$slider->id) !!}
                                    {!! Form::button('<i class="fa fa-times"></i>', array('type' => 'submit', 'class' => 'btn btn-danger btn-xs'))!!}
                                    {!! Form::close() !!}
                                </div>
                                <div id="{{ $slider->id }}" class="panel-collapse collapse">
                                    <div class="panel-body">

                                        {!! Form::open(array('url'=>'admin/mainsliderupdate','class'=>'form-horizontal','files'=>true)) !!}

                                            {!! Form::hidden('id',$slider->id) !!}

                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-2 control-label">Slide Başlığı</label>
                                                <div class="col-sm-10">
                                                    {!! Form::text('baslik',$slider->baslik,array('class'=>'form-control')) !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-2 control-label">Slide Metni</label>
                                                <div class="col-sm-10">
                                                    {!! Form::textarea('metin',$slider->metin,array('class'=>'form-control')) !!}
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-2 control-label">Resim</label>
                                                <div class="col-sm-3">

                                                    {!! HTML::image($slider->photo_path,'image',array('width'=>'256','height'=>'256')) !!}

                                                </div>
                                                <div class="col-sm-7">
                                                    {!! Form::file('image','',array('class'=>'form-control')) !!}
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-2 control-label">Devamını Oku Buton Texti</label>
                                                <div class="col-sm-10">
                                                    {!! Form::text('read_more',$slider->button_text,array('class'=>'form-control')) !!}
                                                </div>
                                            </div>

                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-2 control-label">Slide Yayınlanma Durumu</label>
                                            <div class="col-sm-10">
                                                    @if($slider->state==1)
                                                        {!! Form::select('state',['1'=>'Aktif','0'=>'Pasif'],1,['class'=>'form-control']) !!}
                                                    @elseif($slider->state==0)
                                                        {!! Form::select('state',['1'=>'Aktif','0'=>'Pasif'],0,['class'=>'form-control']) !!}
                                                    @endif
                                                </div>
                                            </div>



                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="submit" class="btn btn-default">Kaydet</button>
                                                </div>
                                            </div>
                                        {!! Form::close() !!}

                                    </div>
                                </div>
                            </div>

                            @endforeach

                        </div>
                    </div>
                </div>


            </div>



        {!! HTML::style('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css') !!}

@endsection