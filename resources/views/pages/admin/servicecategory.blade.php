@extends('master_admin')

@section('content')


    <div class="row">
        <div class="col-md-12">
            <h2>Hizmet Kategori İşlemleri</h2>
            <hr/>
        </div>
    </div>

    <div class="container">

        <div class="row">
            <div class="col-md-12">
                @include('flash::message')
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <h2>Yeni Kategori</h2><hr>
                <div class="row">
                    <div class="col-md-12">
                        @if($errors->has())
                            <div id="form-errors">
                                <p>Bazı Hatalar Oluştu:</p>
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <div class="alert alert-danger">{{ $error }}</div>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>



                <div class="row">
                    <div class="col-md-12">
                        {!! Form::open(array('url'=>'admin/servicecategorycreate','class'=>'form-inline')) !!}
                        <div class="form-group">
                            {!! Form::label('Kategori Adı:') !!}
                            {!! Form::text('name',null,array('class'=>'form-control')) !!}
                            {!! Form::button('<i class="fa fa-plus"></i>',array('type'=>'submit','class'=>'btn btn-success')) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>

                <h2>Mevcut Kategoriler</h2><hr>

                <div class="row">
                    <div class="col-md-12">

                        <div class="panel-group" id="accordion">

                            @foreach($categories as $category)

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#{{$category->id}}">
                                                <h3>{{ $category->name }}</h3>
                                            </a>
                                            <div class="form-group">
                                                {!! Form::open(array('url'=>'admin/servicecategorydestroy','class'=>'form-inline','style'=>'text-align: right;')) !!}

                                                {!! Form::button('<i class="fa fa-pencil-square-o"></i>', array('type' => 'button',
                                                'data-toggle'=>'modal',
                                                'data-target'=>"#editModal_$category->id",'class' => 'btn btn-info'))!!}
                                                {!! Form::button('<i class="fa fa-times"></i>', array('type' => 'button', 'class' => 'btn btn-danger','data-toggle'=>'modal','data-target'=>'#deleteCat_'.$category->id))!!}

                                                {!! Form::close() !!}
                                            </div>
                                            <!-- Modal -->
                                            <div id="editModal_{{$category->id}}" class="modal fade" role="dialog">
                                                <div class="modal-dialog">

                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Kategori Adı Güncelleme</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            {!! Form::open(array('url'=>'admin/servicecategoryupdate','class'=>'form-inline')) !!}
                                                            <div class="form-group">
                                                                {!! Form::hidden('id',$category->id) !!}
                                                                {!! Form::label('Kategori Adı') !!}
                                                                {!! Form::text('name',$category->name,array('class'=>'form-control')) !!}
                                                            </div>

                                                        </div>
                                                        <div class="modal-footer">
                                                            {!! Form::button('Kaydet', array('type' => 'submit', 'class' => 'btn btn-info'))!!}
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>

                                                        </div>
                                                        {!! Form::close() !!}
                                                    </div>

                                                </div>
                                            </div>



                                            <div id="deleteCat_{{$category->id}}" class="modal fade" role="dialog">
                                                <div class="modal-dialog">

                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Kategori Silme İşlemi</h4>
                                                        </div>
                                                        <div class="modal-body">

                                                            <p class="alert alert-danger">Bu Hizmet Kategorisini Sildiğiniz takdirde kategoriye ait alt kategorileri veri tabanından silmiş olacaksınız!</p>

                                                            <h2 class="alert alert-danger">Yine de bu kategoriyi silmek istediğinize emin misiniz?</h2>

                                                            <p class="alert alert-danger">Evet butonuna tıkladığınızda ilgili kategori silinecektir.</p>

                                                        </div>
                                                        <div class="modal-footer">
                                                            {!! Form::open(array('url'=>'admin/servicecategorydestroy')) !!}
                                                            {!! Form::hidden('id',$category->id) !!}

                                                            <button type="button" class="btn btn-default btn-lg" data-dismiss="modal">Kapat</button>
                                                            {!! Form::button('Evet', array('type' => 'submit', 'class' => 'btn btn-danger btn-lg'))!!}

                                                        </div>
                                                        {!! Form::close() !!}
                                                    </div>

                                                </div>
                                            </div>







                                        </h4>
                                    </div>
                                    <div id="{{$category->id}}" class="panel-collapse collapse">

                                        <div class="panel-body">
                                            <h2>Yeni Alt Kategori</h2><hr>

                                            {!! Form::open(array('url'=>'admin/servicesubcategorycreate','class'=>'form-inline')) !!}
                                            <div class="form-group">
                                                {!! Form::hidden('cat_id',$category->id) !!}
                                                {!! Form::label('Alt Kategori Adı:') !!}
                                                {!! Form::text('name',null,array('class'=>'form-control')) !!}
                                                {!! Form::button('<i class="fa fa-plus"></i>',array('type'=>'submit','class'=>'btn btn-success')) !!}
                                            </div>
                                            {!! Form::close() !!}

                                            <h4>Mevcut Alt Kategoriler</h4><hr>

                                            <table class="table table-bordered table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>Alt Kategori</th>
                                                    <th>#Güncelleme</th>
                                                    <th>#Silme</th>
                                                </tr>
                                                </thead>
                                                <tbody>


                                                @foreach(App\ServiceCategory::find($category->id)->serviceSubcategories as $subcategory)


                                                    {!! Form::open(array('url'=>'admin/servicesubcategorydelete','class'=>'form-inline')) !!}
                                                    {!! Form::hidden('id',$subcategory->id) !!}

                                                    <tr>
                                                        <td> {{  $subcategory->name }}</td>
                                                        <td>
                                                            {!! Form::button('<i class="fa fa-pencil-square-o"></i>', array('type' => 'button',
                                                            'data-toggle'=>'modal',
                                                            'data-target'=>"#editSubModal_$subcategory->id",'class' => 'btn btn-info btn-xs'))!!}
                                                        </td>
                                                        <td>{!! Form::button('<i class="fa fa-times"></i>', array('type' => 'button', 'class' => 'btn btn-danger btn-xs','data-toggle'=>'modal','data-target'=>'#deleteSubCat_'.$subcategory->id))!!}</td>
                                                    </tr>

                                                    {!! Form::close() !!}

                                                    <div id="editSubModal_{{$subcategory->id}}" class="modal fade" role="dialog">
                                                        <div class="modal-dialog">

                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                    <h4 class="modal-title">Alt Kategori Güncelleme</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    {!! Form::open(array('url'=>'admin/servicesubcategoryupdate','class'=>'form-inline')) !!}
                                                                    <div class="form-group">
                                                                        {!! Form::hidden('id',$subcategory->id) !!}
                                                                        {!! Form::label('Alt Kategori Adı') !!}
                                                                        {!! Form::text('name',$subcategory->name,array('class'=>'form-control')) !!}
                                                                    </div>

                                                                </div>
                                                                <div class="modal-footer">
                                                                    {!! Form::button('Kaydet', array('type' => 'submit', 'class' => 'btn btn-info'))!!}
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>

                                                                </div>
                                                                {!! Form::close() !!}
                                                            </div>

                                                        </div>
                                                    </div>


                                                    <div id="deleteSubCat_{{$subcategory->id}}" class="modal fade" role="dialog">
                                                        <div class="modal-dialog">

                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                    <h4 class="modal-title">Alt Kategori Silme İşlemi</h4>
                                                                </div>
                                                                <div class="modal-body">

                                                                    <p class="alert alert-danger">Bu hizmet alt kategorisini Sildiğiniz takdirde alt kategoriye ait tüm hizmet duyurularını veri tabanından silmiş olacaksınız! Bu durum geri dönüşü olmayan bir durumdur.</p>

                                                                    <h2 class="alert alert-danger">Yine de bu alt kategoriyi silmek istediğinize emin misiniz?</h2>

                                                                    <p class="alert alert-danger">Evet butonuna tıkladığınızda ilgili alt kategori silinecektir.</p>

                                                                </div>
                                                                <div class="modal-footer">

                                                                    {!! Form::open(array('url'=>'admin/servicesubcategorydelete')) !!}

                                                                    {!! Form::hidden('id',$subcategory->id) !!}

                                                                    <button type="button" class="btn btn-default btn-lg" data-dismiss="modal">Kapat</button>
                                                                    {!! Form::button('Evet', array('type' => 'submit', 'class' => 'btn btn-danger btn-lg'))!!}

                                                                    {!! Form::close() !!}

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>



                                                @endforeach

                                                </tbody>
                                            </table>








                                        </div>
                                    </div>
                                </div>

                            @endforeach

                        </div>
                    </div>
                </div>

            </div>
        </div>


    </div>








    {!! HTML::style('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css') !!}

@endsection