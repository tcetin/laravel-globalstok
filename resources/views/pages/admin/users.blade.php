@extends('master_admin')

@section('content')

    <!-- Page heading -->
    <div class="page-head">
        <!-- Page heading -->
        <h2 class="pull-left">Kullanıcı İşlemleri</h2>

        <div class="clearfix"></div>
    </div><!--/ Page heading ends -->

    <div class="matter">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="widget wred">
                        <div class="widget-head">
                            <div class="pull-left">İmalatçılar</div>
                            <div class="widget-icons pull-right">
                                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="widget-content">
                            <div class="table-responsive">
                                    <table class="table table-bordered " id="imalatcilar"  data-show-export="true">
                                    <thead>
                                    <tr>
                                        <th>Vergi Numarası</th>
                                        <th>Ticaret Ünvanı</th>
                                        <th>İl</th>
                                        <th>İlçe</th>
                                        <th>Adres</th>
                                        <th>Telefon</th>
                                        <th>Fax</th>
                                        <th>Eposta</th>
                                        <th>Web Sitesi</th>
                                        <th>Kullanıcı Durumu</th>
                                        <th>Firma Doğrulama Durumu</th>
                                        <th>Kullanıcı Onay Durumu</th>
                                        <th>Yetkili Ad Soyad</th>
                                        <th>Yetkili Resim</th>
                                        <th>Şirketin Sahip Olduğu Sertifikalar</th>
                                        <th>Şirkete Ait Resimler</th>
                                        <th>Şirket Kuruluş Tarihi</th>
                                        <th>Şirketteki Çalışan Sayısı</th>
                                        <th>Şirket Profili</th>
                                        <th>Üyelik Tarihi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach(App\User::where('rol',2)->get() as $imalatci)
                                        <tr>
                                          <td>{{$imalatci->vergino}}</td>
                                          <td>{{$imalatci->unvan}}</td>
                                          <td>
                                          @foreach(App\City::where('city_code',$imalatci->city_code)->get() as $city)
                                           {{ $city->city }}
                                          @endforeach
                                          </td>
                                           @if($imalatci->city_town==0)
                                               <td>MERKEZ</td>
                                           @else
                                          <td>{{App\CityTown::find($imalatci->city_town)->name}}</td>
                                           @endif
                                          <td>{{$imalatci->adres}}</td>
                                          <td>{{$imalatci->telefon}}</td>
                                          <td>{{$imalatci->fax}}</td>
                                          <td>{{$imalatci->email}}</td>
                                          <td>{{$imalatci->web}}</td>
                                            @if($imalatci->active==1)
                                                <td>
                                                    <div id="pasif_yap_{{$imalatci->id}}" class="alert alert-success"><i class="fa fa-check"></i>Aktif</div>
                                                    <form action="/admin/deactiveduser" method="GET">
                                                        {!!csrf_field() !!}
                                                        <input type="hidden" name="user_id" value="{{$imalatci->id}}"/>
                                                        <button type="button" data-user = "{{$imalatci->id}}" class="btn btn-danger btn-xs pasif_yap"><i class="fa fa-times"></i> Pasif Yap</button>
                                                    </form>
                                               </td>
                                            @else
                                                <td>
                                                    <div id="aktif_yap_{{$imalatci->id}}" class="alert alert-danger">Pasif</div>
                                                    <form action="/admin/activeduser" method="GET">
                                                        {!!csrf_field() !!}
                                                        <input type="hidden" name="user_id" value="{{$imalatci->id}}"/>
                                                    <button type="button" data-user = "{{$imalatci->id}}" class="btn btn-success btn-xs aktif_yap"><i class="fa fa-check"></i> Aktif Yap</button>
                                                    </form>
                                                </td>
                                            @endif
                                          @if($imalatci->verified==1)
                                              <td>
                                                  <div id="dogrulama_{{$imalatci->id}}" class="alert alert-success"><i class="fa fa-star"></i> <strong>Doğrulanmış Firma</strong></div>
                                                  <form action="/admin/unverifyuser" method="GET">
                                                      {!!csrf_field() !!}
                                                      <input type="hidden" name="user_id" value="{{$imalatci->id}}"/>
                                                    <button type="button" data-user = "{{$imalatci->id}}" class="btn btn-danger btn-xs dogrulama"><i class="fa fa-times"></i> Doğrulamayı İptal Et</button>
                                                  </form>
                                              </td>
                                          @else
                                                <td>
                                                    <div id="dogrula_{{$imalatci->id}}" class="alert alert-danger">Doğrulanmamış Firma</div>
                                                    <form action="/admin/verifyuser" method="GET">
                                                        {!!csrf_field() !!}
                                                        <input type="hidden" name="user_id" value="{{$imalatci->id}}"/>
                                                        <button type="button" data-user = "{{$imalatci->id}}" class="btn btn-success btn-xs dogrula"><i class="fa fa-check"></i> Firmayı Doğrula</button>
                                                    </form>
                                                </td>
                                          @endif
                                          <td>
                                              @if($imalatci->confirmed==1)
                                                  <div id="onay_kaldir_{{$imalatci->id}}" class="alert alert-success"><i class="fa fa-check"></i> <strong>Onaylı</strong></div>
                                                  <form action="/admin/unconfirmuser" method="POST">
                                                      {!!csrf_field() !!}
                                                      <input type="hidden" name="user_id" value="{{$imalatci->id}}"/>
                                                      <button type="button" data-user = "{{$imalatci->id}}" class="btn btn-danger btn-xs onay_kaldir"><i class="fa fa-times"></i> Onayı Kaldır</button>
                                                  </form>
                                              @else
                                                  <div id="onay_{{$imalatci->id}}" class="alert alert-danger">Onay Bekliyor</div>
                                                  <form action="/admin/confirmuser" method="GET">
                                                      {!!csrf_field() !!}
                                                      <input type="hidden" name="user_id" value="{{$imalatci->id}}"/>
                                                      <button type="button" data-user = "{{$imalatci->id}}" class="btn btn-success btn-xs onayla"><i class="fa fa-check"></i> Onayla</button>
                                                  </form>
                                              @endif
                                          </td>
                                          <td>{{$imalatci->yetkili}}</td>

                                          <td>
                                              @if($imalatci->yetkili_resim or "")

                                                      <img src="/{{$imalatci->yetkili_resim}}" alt=""/>

                                                      @if($imalatci->yetkili_resim_verified==1)
                                                          <div id="y_resim_onay_kaldir_{{$imalatci->id}}" class="alert alert-success"><i class="fa fa-check"></i>Onaylı</div>
                                                          <form action="/admin/unverifyyetkiliresim" method="GET">
                                                              {!!csrf_field() !!}
                                                              <input type="hidden" name="user_id" value="{{$imalatci->id}}"/>
                                                              <button type="button" data-user = "{{$imalatci->id}}" class="btn btn-danger btn-xs y_resim_onay_kaldir"><i class="fa fa-times"></i> Onayı Kaldır</button>
                                                          </form>
                                                      @else
                                                          <div id="y_resim_onayla_{{$imalatci->id}}" class="alert alert-danger">Onay Bekliyor</div>
                                                          <form action="/admin/verifyyetkiliresim" method="GET">
                                                              {!!csrf_field() !!}
                                                              <input type="hidden" name="user_id" value="{{$imalatci->id}}"/>
                                                              <button type="button" data-user = "{{$imalatci->id}}" class="btn btn-success btn-xs y_resim_onayla"><i class="fa fa-check"></i> Onayla</button>
                                                          </form>
                                                      @endif
                                               @else
                                               <div class="alert alert-warning">Yetkili resmi henüz yüklenmemiş.</div>
                                               @endif


                                          </td>
                                          <td>
                                              <button type="button" data-target="#sertifika_{{$imalatci->id}}" data-toggle="modal" class="btn btn-success btn-xs">Sertifikaları Görüntüle</button>
                                              <div id="sertifika_{{$imalatci->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                              								<div class="modal-dialog modal-lg">
                                              								  <div class="modal-content">
                                              								  <div class="modal-header">
                                              									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                              									<h4 class="modal-title">Şirketin Mevcut Sertifikaları</h4>
                                              								  </div>
                                              								  <div class="modal-body">

                                              									@foreach(App\CertificateImage::where('imalatci_id',$imalatci->id)->get() as $image)
                                                                                <div class="row">
                                                                                     <div class="col-xs-6">
                                                                                        <img src="/{{$image->path}}" alt="Image" style="max-height: 600px;max-width: 400px" class="img-responsive" />
                                                                                     </div>
                                                                                     <div class="col-xs-6">
                                                                                         @if($image->verified==1)
                                                                                             <form>
                                                                                                 {!!csrf_field() !!}
                                                                                             <div id="onayli_sertifika_{{$image->id}}" class="alert alert-success"> Onaylı</div>
                                                                                             <button type="button" data-sertifika = "{{$image->id}}" class="btn btn-danger btn-xs sertifika_onay_kaldir"><i class="fa fa-times"></i> Onayı Kaldır</button>
                                                                                             </form>
                                                                                         @else
                                                                                             <form>
                                                                                                 {!!csrf_field() !!}
                                                                                             <div id="onaysiz_sertifika_{{$image->id}}" class="alert alert-danger">Onay Bekliyor</div>
                                                                                             <button type="button" data-sertifika = "{{$image->id}}" class="btn btn-success btn-xs sertifika_onayla"><i class="fa fa-check"></i> Onayla</button>
                                                                                             </form>
                                                                                         @endif

                                                                                     </div>
                                                                                </div>
                                                                                <hr/>
                                              									@endforeach

                                              								  </div>
                                              								  <div class="modal-footer">
                                              									<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Kapat</button>
                                              								  </div>
                                              								</div>
                                              								</div>
                                              								</div>
                                          </td>
                                          <td>
                                              <button type="button" data-target="#resim_{{$imalatci->id}}" data-toggle="modal" class="btn btn-success btn-xs">Resimleri Görüntüle</button>
                                              <div id="resim_{{$imalatci->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                  <div class="modal-dialog modal-lg">
                                                      <div class="modal-content">
                                                          <div class="modal-header">
                                                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                              <h4 class="modal-title">Şirketin Resimleri</h4>
                                                          </div>
                                                          <div class="modal-body">

                                                              @foreach(App\CompanyImage::where('imalatci_id',$imalatci->id)->get() as $image)
                                                                  <div class="row">
                                                                      <div class="col-xs-6">
                                                                          <img src="/{{$image->path}}" alt="Image" style="max-height: 600px;max-width: 400px" class="img-responsive" />
                                                                      </div>
                                                                      <div class="col-xs-6">
                                                                          @if($image->verified==1)
                                                                              <form>
                                                                                  {!!csrf_field() !!}
                                                                                  <div id="onayli_resim_{{$image->id}}" class="alert alert-success"> Onaylı</div>
                                                                                  <button type="button" data-resim = "{{$image->id}}" class="btn btn-danger btn-xs resim_onay_kaldir"><i class="fa fa-times"></i> Onayı Kaldır</button>
                                                                              </form>
                                                                          @else
                                                                              <form>
                                                                                  {!!csrf_field() !!}
                                                                                  <div id="onaysiz_resim_{{$image->id}}" class="alert alert-danger">Onay Bekliyor</div>
                                                                                  <button type="button" data-resim = "{{$image->id}}" class="btn btn-success btn-xs resim_onayla"><i class="fa fa-check"></i> Onayla</button>
                                                                              </form>
                                                                          @endif

                                                                      </div>
                                                                  </div>
                                                                  <hr/>
                                                              @endforeach

                                                          </div>
                                                          <div class="modal-footer">
                                                              <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Kapat</button>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>

                                          </td>
                                          <td>{{$imalatci->established_year}}</td>
                                          <td>{{$imalatci->employees_number}}</td>
                                          <td>
                                              <button type="button" data-target="#profil_{{$imalatci->id}}" data-toggle="modal" class="btn btn-success btn-xs">Profili Görüntüle</button>
                                              <div id="profil_{{$imalatci->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                  <div class="modal-dialog modal-lg">
                                                      <div class="modal-content">
                                                          <div class="modal-header">
                                                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                              <h4 class="modal-title">Şirketin Profili</h4>
                                                          </div>
                                                          <div class="modal-body">{{$imalatci->imalatci_profil}}</div>
                                                          <div class="modal-footer">
                                                              <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Kapat</button>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>

                                          </td>
                                          <td>{{$imalatci->created_at}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>



            <div class="row">
                <div class="col-md-12">
                    <div class="widget wred">
                        <div class="widget-head">
                            <div class="pull-left">Hizmet Firmaları</div>
                            <div class="widget-icons pull-right">
                                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="widget-content">
                            <div class="table-responsive">
                                <table class="table table-bordered " id="service"  data-show-export="true">
                                    <thead>
                                    <tr>
                                        <th>Vergi Numarası</th>
                                        <th>Ticaret Ünvanı</th>
                                        <th>İl</th>
                                        <th>İlçe</th>
                                        <th>Adres</th>
                                        <th>Telefon</th>
                                        <th>Fax</th>
                                        <th>Eposta</th>
                                        <th>Web Sitesi</th>
                                        <th>Kullanıcı Durumu</th>
                                        <th>Firma Doğrulama Durumu</th>
                                        <th>Kullanıcı Onay Durumu</th>
                                        <th>Yetkili Ad Soyad</th>
                                        <th>Yetkili Resim</th>
                                        <th>Şirketin Sahip Olduğu Sertifikalar</th>
                                        <th>Şirkete Ait Resimler</th>
                                        <th>Şirket Kuruluş Tarihi</th>
                                        <th>Şirketteki Çalışan Sayısı</th>
                                        <th>Şirket Profili</th>
                                        <th>Üyelik Tarihi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach(App\User::where('rol',4)->get() as $service)
                                        <tr>
                                            <td>{{$service->vergino}}</td>
                                            <td>{{$service->unvan}}</td>
                                            <td>
                                                @foreach(App\City::where('city_code',$service->city_code)->get() as $city)
                                                    {{ $city->city }}
                                                @endforeach
                                            </td>
                                            @if($service->city_town==0)
                                                <td>MERKEZ</td>
                                            @else
                                                <td>{{App\CityTown::find($service->city_town)->name}}</td>
                                            @endif
                                            <td>{{$service->adres}}</td>
                                            <td>{{$service->telefon}}</td>
                                            <td>{{$service->fax}}</td>
                                            <td>{{$service->email}}</td>
                                            <td>{{$service->web}}</td>
                                            @if($service->active==1)
                                                <td>
                                                    <div id="pasif_yap_{{$service->id}}" class="alert alert-success"><i class="fa fa-check"></i>Aktif</div>
                                                    <form action="/admin/deactiveduser" method="GET">
                                                        {!!csrf_field() !!}
                                                        <input type="hidden" name="user_id" value="{{$service->id}}"/>
                                                        <button type="button" data-user = "{{$service->id}}" class="btn btn-danger btn-xs pasif_yap"><i class="fa fa-times"></i> Pasif Yap</button>
                                                    </form>
                                                </td>
                                            @else
                                                <td>
                                                    <div id="aktif_yap_{{$service->id}}" class="alert alert-danger">Pasif</div>
                                                    <form action="/admin/activeduser" method="GET">
                                                        {!!csrf_field() !!}
                                                        <input type="hidden" name="user_id" value="{{$service->id}}"/>
                                                        <button type="button" data-user = "{{$service->id}}" class="btn btn-success btn-xs aktif_yap"><i class="fa fa-check"></i> Aktif Yap</button>
                                                    </form>
                                                </td>
                                            @endif
                                            @if($service->verified==1)
                                                <td>
                                                    <div id="dogrulama_{{$service->id}}" class="alert alert-success"><i class="fa fa-star"></i> <strong>Doğrulanmış  Firma</strong></div>
                                                    <form action="/admin/unverifyuser" method="GET">
                                                        {!!csrf_field() !!}
                                                        <input type="hidden" name="user_id" value="{{$service->id}}"/>
                                                        <button type="button" data-user = "{{$service->id}}" class="btn btn-danger btn-xs dogrulama"><i class="fa fa-times"></i> Doğrulamayı İptal Et</button>
                                                    </form>
                                                </td>
                                            @else
                                                <td>
                                                    <div id="dogrula_{{$service->id}}" class="alert alert-danger">Doğrulanmamış Firma</div>
                                                    <form action="/admin/verifyuser" method="GET">
                                                        {!!csrf_field() !!}
                                                        <input type="hidden" name="user_id" value="{{$service->id}}"/>
                                                        <button type="button" data-user = "{{$service->id}}" class="btn btn-success btn-xs dogrula"><i class="fa fa-check"></i> Firmayı Doğrula</button>
                                                    </form>
                                                </td>
                                            @endif
                                            <td>
                                                @if($service->confirmed==1)
                                                    <div id="onay_kaldir_{{$service->id}}" class="alert alert-success"><i class="fa fa-check"></i> <strong>Onaylı</strong></div>
                                                    <form action="/admin/unconfirmuser" method="POST">
                                                        {!!csrf_field() !!}
                                                        <input type="hidden" name="user_id" value="{{$service->id}}"/>
                                                        <button type="button" data-user = "{{$service->id}}" class="btn btn-danger btn-xs onay_kaldir"><i class="fa fa-times"></i> Onayı Kaldır</button>
                                                    </form>
                                                @else
                                                    <div id="onay_{{$service->id}}" class="alert alert-danger">Onay Bekliyor</div>
                                                    <form action="/admin/confirmuser" method="GET">
                                                        {!!csrf_field() !!}
                                                        <input type="hidden" name="user_id" value="{{$service->id}}"/>
                                                        <button type="button" data-user = "{{$service->id}}" class="btn btn-success btn-xs onayla"><i class="fa fa-check"></i> Onayla</button>
                                                    </form>
                                                @endif
                                            </td>
                                            <td>{{$service->yetkili}}</td>

                                            <td>
                                                @if($service->yetkili_resim or "")

                                                    <img src="/{{$service->yetkili_resim}}" alt=""/>

                                                    @if($service->yetkili_resim_verified==1)
                                                        <div id="y_resim_onay_kaldir_{{$service->id}}" class="alert alert-success"><i class="fa fa-check"></i>Onaylı</div>
                                                        <form action="/admin/unverifyyetkiliresim" method="GET">
                                                            {!!csrf_field() !!}
                                                            <input type="hidden" name="user_id" value="{{$service->id}}"/>
                                                            <button type="button" data-user = "{{$service->id}}" class="btn btn-danger btn-xs y_resim_onay_kaldir"><i class="fa fa-times"></i> Onayı Kaldır</button>
                                                        </form>
                                                    @else
                                                        <div id="y_resim_onayla_{{$service->id}}" class="alert alert-danger">Onay Bekliyor</div>
                                                        <form action="/admin/verifyyetkiliresim" method="GET">
                                                            {!!csrf_field() !!}
                                                            <input type="hidden" name="user_id" value="{{$service->id}}"/>
                                                            <button type="button" data-user = "{{$service->id}}" class="btn btn-success btn-xs y_resim_onayla"><i class="fa fa-check"></i> Onayla</button>
                                                        </form>
                                                    @endif
                                                @else
                                                    <div class="alert alert-warning">Yetkili resmi henüz yüklenmemiş.</div>
                                                @endif


                                            </td>
                                            <td>
                                                <button type="button" data-target="#sertifika_{{$service->id}}" data-toggle="modal" class="btn btn-success btn-xs">Sertifikaları Görüntüle</button>
                                                <div id="sertifika_{{$service->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                <h4 class="modal-title">Şirketin Mevcut Sertifikaları</h4>
                                                            </div>
                                                            <div class="modal-body">



                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Kapat</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="button" data-target="#resim_{{$service->id}}" data-toggle="modal" class="btn btn-success btn-xs">Resimleri Görüntüle</button>
                                                <div id="resim_{{$service->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                <h4 class="modal-title">Şirketin Resimleri</h4>
                                                            </div>
                                                            <div class="modal-body">



                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Kapat</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </td>
                                            <td>{{$service->established_year}}</td>
                                            <td>{{$service->employees_number}}</td>
                                            <td>
                                                <button type="button" data-target="#profil_{{$service->id}}" data-toggle="modal" class="btn btn-success btn-xs">Profili Görüntüle</button>
                                                <div id="profil_{{$service->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                <h4 class="modal-title">Şirketin Profili</h4>
                                                            </div>
                                                            <div class="modal-body">{!!$service->imalatci_profil!!}</div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Kapat</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </td>
                                            <td>{{$service->created_at}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="widget wblue">
                        <div class="widget-head">
                            <div class="pull-left">Tedarikçiler</div>
                            <div class="widget-icons pull-right">
                                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="widget-content">
                            <div class="table-responsive">
                                <table class="table table-bordered " id="tedarikciler">
                                    <thead>
                                    <tr>
                                        <th>Eposta</th>
                                        <th>İl</th>
                                        <th>İlçe</th>
                                        <th>Adres</th>
                                        <th>Telefon</th>
                                        <th>Kullanıcı Durumu</th>
                                        <th>Üyelik Tarihi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach(App\User::where('rol',3)->get() as $tedarikci)
                                        <tr>
                                            <td>{{$tedarikci->email}}</td>
                                            <td>
                                                @foreach(App\City::where('city_code',$tedarikci->city_code)->get() as $city)
                                                    {{ $city->city }}
                                                @endforeach
                                            </td>
                                            @if($tedarikci->city_town==0)
                                                <td>MERKEZ</td>
                                            @else
                                                <td>{{App\CityTown::find($tedarikci->city_town)->name}}</td>
                                            @endif
                                            <td>{{$tedarikci->adres}}</td>
                                            <td>{{$tedarikci->telefon}}</td>
                                            @if($tedarikci->active==1)
                                                <td>
                                                    <div id="pasif_yap_{{$tedarikci->id}}" class="alert alert-success"><i class="fa fa-check"></i>Aktif</div>
                                                    <form action="/admin/deactiveduser" method="GET">
                                                        {!!csrf_field() !!}
                                                        <input type="hidden" name="user_id" value="{{$tedarikci->id}}"/>
                                                        <button type="button" data-user = "{{$tedarikci->id}}" class="btn btn-danger btn-xs pasif_yap"><i class="fa fa-times"></i> Pasif Yap</button>
                                                    </form>
                                                </td>
                                            @else
                                                <td>
                                                    <div id="aktif_yap_{{$tedarikci->id}}" class="alert alert-danger">Pasif</div>
                                                    <form action="/admin/activeduser" method="GET">
                                                        {!!csrf_field() !!}
                                                        <input type="hidden" name="user_id" value="{{$tedarikci->id}}"/>
                                                        <button type="button" data-user = "{{$tedarikci->id}}" class="btn btn-success btn-xs aktif_yap"><i class="fa fa-check"></i> Aktif Yap</button>
                                                    </form>
                                                </td>
                                            @endif

                                            <td>{{$tedarikci->created_at}}</td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-alpha1/jquery.min.js"></script>

    <script>
        $(document).on('click','button.sertifika_onayla',function(){
            var element = $(this);
            var resim_id = $(this).attr('data-sertifika');
            var response_div3 = $('div#onayli_sertifika_'+resim_id);
            $('div#onayli_sertifika_'+resim_id).remove();
            response_div3.html('<img src="/images/ajax-loader.gif" style="max-width=80px;max-height=80px"/>');
            $.ajax({
                type: 'GET',
                url: '/admin/verifysertifika',
                data: {id: resim_id},
                success: function (msg) {
                    alert(msg);
                    response_div3.html(msg);

                    element.removeClass('btn btn-success btn-xs sertifika_onayla');
                    element.addClass('btn btn-danger btn-xs sertifika_onay_kaldir');

                    element.text("Onayı Kaldır");

                    response_div3.removeClass('alert alert-danger');
                    response_div3.addClass('alert alert-success');

                }
            });
        });


        $(document).on('click','button.sertifika_onay_kaldir',function(){
            var element = $(this);
            var resim_id = $(this).attr('data-sertifika');
            var response_div2 = $('#onaysiz_sertifika_'+resim_id);
            $('#onaysiz_sertifika_'+resim_id).remove();
            response_div2.html('<img src="/images/ajax-loader.gif" style="max-width=80px;max-height=80px"/>');
            $.ajax({
                type: 'GET',
                url: '/admin/unverifysertifika',
                data: {id: resim_id},
                success: function (msg) {
                    alert(msg);
                    response_div2.html(msg);

                    element.removeClass('btn btn-danger btn-xs sertifika_onay_kaldir');
                    element.addClass('btn btn-success btn-xs sertifika_onayla');

                    element.text("Onayla");

                    response_div2.removeClass('alert alert-success');
                    response_div2.addClass('alert alert-danger');
                }
            });
        });


        $(document).on('click','button.resim_onayla',function(){
            var element = $(this);
            var resim_id = $(this).attr('data-resim');
            var response_div3 = $('div#onayli_resim_'+resim_id);
            response_div3.html('<img src="/images/ajax-loader.gif" style="max-width=80px;max-height=80px"/>');
            $.ajax({
                type: 'GET',
                url: '/admin/verifyresim',
                data: {id: resim_id},
                success: function (msg) {
                    alert(msg);
                    response_div3.html(msg);

                    element.removeClass('btn btn-success btn-xs resim_onayla');
                    element.addClass('btn btn-danger btn-xs resim_onay_kaldir');

                    element.text("Onayı Kaldır");

                    response_div3.removeClass('alert alert-danger');
                    response_div3.addClass('alert alert-success');

                }
            });
        });


        $(document).on('click','button.resim_onay_kaldir',function(){
            var element = $(this);
            var resim_id = $(this).attr('data-resim');
            var response_div2 = $('#onaysiz_resim_'+resim_id);
            $('#onaysiz_resim_'+resim_id).remove();
            response_div2.html('<img src="/images/ajax-loader.gif" style="max-width=80px;max-height=80px"/>');
            $.ajax({
                type: 'GET',
                url: '/admin/unverifyresim',
                data: {id: resim_id},
                success: function (msg) {
                    alert(msg);
                    response_div2.html(msg);

                    element.removeClass('btn btn-danger btn-xs resim_onay_kaldir');
                    element.addClass('btn btn-success btn-xs resim_onayla');

                    element.text("Onayla");

                    response_div2.removeClass('alert alert-success');
                    response_div2.addClass('alert alert-danger');
                }
            });
        });


        // ----KULLANICIYI ONAYLE YA DA ONAY KALDIR

        $(document).on('click','button.onayla', function () {

            var imalatci_id = $(this).attr('data-user');

            var responseDiv = $('div#onay_'+imalatci_id);

            var element = $(this);

            responseDiv.html('<img src="/images/AjaxLoader.gif" style="max-width=80px;max-height=80px;"/>');

            $.ajax({
                type: 'GET',
                url: '/admin/confirmuser',
                data: {imalatci_id:imalatci_id},
                success: function (msg) {

                    alert(msg);
                    responseDiv.toggleClass("alert-danger alert-success");

                    responseDiv.html('<i class="fa fa-check"></i> <strong>Onaylı</strong>');
                    responseDiv.attr('id','onay_kaldir_'+imalatci_id);

                    element.toggleClass('btn btn-success btn-xs onayla btn btn-danger btn-xs onay_kaldir');

                    element.html('<i class="fa fa-times"></i> Onay Kaldır');
                }
            });

        });

        $(document).on('click','button.onay_kaldir', function () {

            var imalatci_id = $(this).attr('data-user');

            var responseDiv = $('div#onay_kaldir_' + imalatci_id);

            var element = $(this);

            responseDiv.html('<img src="/images/AjaxLoader.gif" style="max-width=80px;max-height=80px;"/>');

            $.ajax({
                type: 'GET',
                url: '/admin/unconfirmuser',
                data: {imalatci_id: imalatci_id},
                success: function (msg) {

                    alert(msg);
                    responseDiv.toggleClass("alert-danger alert-success");

                    responseDiv.html('<strong>Onay Bekliyor</strong>');

                    responseDiv.attr('id', 'onay_' + imalatci_id);


                    element.toggleClass('btn btn-danger btn-xs onay_kaldir btn btn-success btn-xs onayla');

                    element.html('<i class="fa fa-check"></i> Onayla');
                }
            });

        });

        //----


         //---FİRMAYI DOĞRULA YA DA DOĞRULAMAYI KALDIR

         $(document).on('click','button.dogrula', function () {

                var imalatci_id = $(this).attr('data-user');

                var responseDiv = $('div#dogrula_'+imalatci_id);

                var element = $(this);

                responseDiv.html('<img src="/images/AjaxLoader.gif" style="max-width=80px;max-height=80px;"/>');

                $.ajax({
                    type: 'GET',
                    url: '/admin/verifyuser',
                    data: {imalatci_id:imalatci_id},
                    success: function (msg) {

                        alert(msg);
                        responseDiv.toggleClass("alert-danger alert-success");

                        responseDiv.html('<i class="fa fa-star"></i> <strong>Doğrulanmış Firma</strong>');
                        responseDiv.attr('id','dogrulama_'+imalatci_id);

                        element.toggleClass('btn btn-success btn-xs dogrula btn btn-danger btn-xs dogrulama');

                        element.html('<i class="fa fa-times"></i> Doğrulamayı İptal Et');
                    }
                });

            });

         $(document).on('click','button.dogrulama', function () {

                var imalatci_id = $(this).attr('data-user');

                var responseDiv = $('div#dogrulama_'+imalatci_id);

                var element = $(this);

                responseDiv.html('<img src="/images/AjaxLoader.gif" style="max-width=80px;max-height=80px;"/>');

                $.ajax({
                    type: 'GET',
                    url: '/admin/unverifyuser',
                    data: {imalatci_id:imalatci_id},
                    success: function (msg) {

                        alert(msg);
                        responseDiv.toggleClass("alert-success alert-danger");

                        responseDiv.html('<strong>Doğrulanmamış Firma</strong>');

                        responseDiv.attr('id','dogrula_'+imalatci_id);


                        element.toggleClass('btn btn-danger btn-xs dogrulama btn btn-success btn-xs dogrula');

                        element.html('<i class="fa fa-check"></i> Firmayı Dogrula');
                    }
                });
            });
         //-----


        //---- KULLANICIYI AKTİF ET YA DA PASİF ET
        $(document).on('click','button.aktif_yap', function () {

            var imalatci_id = $(this).attr('data-user');

            var responseDiv = $('div#aktif_yap_'+imalatci_id);

            var element = $(this);

            responseDiv.html('<img src="/images/AjaxLoader.gif" style="max-width=80px;max-height=80px;"/>');

            $.ajax({
                type: 'GET',
                url: '/admin/activeduser',
                data: {imalatci_id:imalatci_id},
                success: function (msg) {

                    alert(msg);
                    responseDiv.toggleClass("alert-danger alert-success");

                    responseDiv.html('<i class="fa fa-check"></i> <strong>Aktif</strong>');
                    responseDiv.attr('id','pasif_yap_'+imalatci_id);

                    element.toggleClass('btn btn-success btn-xs aktif_yap btn btn-danger btn-xs pasif_yap');

                    element.html('<i class="fa fa-times"></i> Pasif Yap');
                }
            });

        });

        $(document).on('click','button.pasif_yap', function () {

            var imalatci_id = $(this).attr('data-user');

            var responseDiv = $('div#pasif_yap_'+imalatci_id);

            var element = $(this);

            responseDiv.html('<img src="/images/AjaxLoader.gif" style="max-width=80px;max-height=80px;"/>');

            $.ajax({
                type: 'GET',
                url: '/admin/deactiveduser',
                data: {imalatci_id:imalatci_id},
                success: function (msg) {

                    alert(msg);
                    responseDiv.toggleClass("alert-success alert-danger");

                    responseDiv.html('<strong>Pasif</strong>');

                    responseDiv.attr('id','aktif_yap_'+imalatci_id);


                    element.toggleClass('btn btn-danger btn-xs pasif_yap btn btn-success btn-xs aktif_yap');

                    element.html('<i class="fa fa-check"></i> Aktif Yap');
                }
            });
        });
        //----


        //---- YETKİLİ RESMİNİ ONAYLA YA DA ONAYLAMA
        $(document).on('click','button.y_resim_onayla', function () {

            var imalatci_id = $(this).attr('data-user');

            var responseDiv = $('div#y_resim_onayla_'+imalatci_id);

            var element = $(this);

            responseDiv.html('<img src="/images/AjaxLoader.gif" style="max-width=80px;max-height=80px;"/>');

            $.ajax({
                type: 'GET',
                url: '/admin/verifyyetkiliresim',
                data: {imalatci_id:imalatci_id},
                success: function (msg) {

                    alert(msg);
                    responseDiv.toggleClass("alert-danger alert-success");

                    responseDiv.html('<i class="fa fa-check"></i> <strong>Onaylı</strong>');
                    responseDiv.attr('id','y_resim_onay_kaldir_'+imalatci_id);

                    element.toggleClass('btn btn-success btn-xs y_resim_onayla btn btn-danger btn-xs y_resim_onay_kaldir');

                    element.html('<i class="fa fa-times"></i> Onay Kaldır');
                }
            });

        });

        $(document).on('click','button.y_resim_onay_kaldir', function () {

            var imalatci_id = $(this).attr('data-user');

            var responseDiv = $('div#y_resim_onay_kaldir_'+imalatci_id);

            var element = $(this);

            responseDiv.html('<img src="/images/AjaxLoader.gif" style="max-width=80px;max-height=80px;"/>');

            $.ajax({
                type: 'GET',
                url: '/admin/unverifyyetkiliresim',
                data: {imalatci_id:imalatci_id},
                success: function (msg) {

                    alert(msg);
                    responseDiv.toggleClass("alert-success alert-danger");

                    responseDiv.html('<strong>Onay Bekliyor</strong>');

                    responseDiv.attr('id','y_resim_onayla_'+imalatci_id);


                    element.toggleClass('btn btn-danger btn-xs y_resim_onay_kaldir btn btn-success btn-xs y_resim_onayla');

                    element.html('<i class="fa fa-check"></i> Onayla');
                }
            });
        });
        //----

    </script>

@endsection