@extends('app')

@section('content')
        <!-- Main Container Starts -->
<div id="main-container" class="container">
    <!-- Breadcrumb Starts -->
    <ol class="breadcrumb">
        <li><a href="/">Anasayfa</a></li>
        <li class="active">Giriş</li>
    </ol>
    <!-- Breadcrumb Ends -->
    <!-- Main Heading Starts -->
    <h2 class="main-heading text-center">
        Giriş Yap ya da Kayıt Ol
    </h2>

    @include('flash::message')

    <div class="row">
        <div class="col-md-12">
            @if($errors->has())
                <div id="form-errors">
                    <p>Bazı Hatalar Oluştu:</p>
                    <ul>
                        @foreach($errors->all() as $error)
                            <div class="alert alert-danger">{{ $error }}</div>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
    <!-- Main Heading Ends -->
    <!-- Login Form Section Starts -->
    <section class="login-area">
        <div class="row">
            <div class="col-sm-6">
                <!-- Login Panel Starts -->
                <div class="panel panel-smart">
                    <div class="panel-heading">
                        <h3 class="panel-title">Perakendeci Girişi</h3>
                    </div>
                    <div class="panel-body">
                        <p>
                            Var olan perakendeci kaydınızla buradan giriş yapabilirsiniz.
                        </p>
                        <!-- Login Form Starts -->
                        {!! Form::open(array('url'=>'user/loginperakendeci','class'=>'form-inline')) !!}

                              {!! csrf_field() !!}

                            <div class="form-group">
                                <label class="sr-only" for="exampleInputEmail2"></label>
                                <input type="text" class="form-control" name="email" placeholder="Eposta Adresi">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="exampleInputPassword2"></label>
                                <input type="password" class="form-control" name="password" placeholder="Şifre">
                            </div>

                            <button type="submit" class="btn btn-black">
                                Giriş Yap
                            </button>
                        {!! Form::close() !!}
                        <!-- Login Form Ends -->
                    </div>
                </div>
                <!-- Login Panel Ends -->
            </div>
            <div class="col-sm-6">
                <!-- Account Panel Starts -->
                <div class="panel panel-smart">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Herhangi bir giriş hesabım yok?
                        </h3>
                    </div>
                    <div class="panel-body">
                        <p>
                            İmalatçı(Tedarikçi/Toptancı) ya da Perakendeci olarak giriş yapabilmeniz için  kayıt olmanız gerekmektedir.Kayıt olmak için <b>Kayıt Ol</b> butonuna tıklayınız.
                        </p>
                        <a href="/register" class="btn btn-black">
                            Kayıt Ol
                        </a>
                    </div>
                </div>
                <!-- Account Panel Ends -->
            </div>
        </div>
    </section>
    <!-- Login Form Section Ends -->
</div>
<!-- Main Container Ends -->
@endsection