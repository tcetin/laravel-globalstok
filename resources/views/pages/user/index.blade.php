@extends('app')

@section('content')

        <!-- Main Container Starts -->
<div id="main-container" class="container">
    <!-- Breadcrumb Starts -->
    <ol class="breadcrumb">
        <li><a href="/">Anasayfa</a></li>
        <li class="active">Giriş</li>
    </ol>



    <section class="login-area">
        <div class="row">
            <div class="col-sm-12">
                <!-- Login Panel Starts -->
                <div class="panel panel-smart">
                    <div class="panel-heading">
                        <h3 class="panel-title">Kullanıcı Girişi</h3>
                    </div>
                    <div class="panel-body">

                        {!! Form::open(array('url'=>'/user/login','class'=>'form-horizontal')) !!}

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Eposta Adresi</label>
                                <div class="col-sm-9">
                                    {!! Form::text('email',null,array('class'=>'form-control','placeholder'=>'Eposta Adresinizi Giriniz.')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Şifre</label>
                                <div class="col-sm-9">
                                    {!! Form::password('password',array('class'=>'form-control','placeholder'=>'Şifre Giriniz.')) !!}
                                </div>
                            </div>
                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-black btn-lg">
                                    Giriş Yap
                                </button>
                            </div>


                        {!! Form::close() !!}

                        @include('flash::message')
                        <!-- Login Form Ends -->
                    </div>
                </div>
                <!-- Login Panel Ends -->
            </div>
        </div>
    </section>
    <!-- Login Form Section Ends -->
</div>
<!-- Main Container Ends -->


@endsection