@extends('app')

@section('content')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" media="screen">

{!! HTML::style('css/datepicker.css')!!}

{!! HTML::style('css/custom_register_tab.css')!!}
        <!-- Main Container Starts -->
<div id="main-container" class="container">
    <!-- Breadcrumb Starts -->
    <ol class="breadcrumb">
        <li><a href="/">Anasayfa</a></li>
        <li class="active">Kayıt Ol</li>
    </ol>
    <!-- Breadcrumb Ends -->

    <div class="row">
        <div class="col-md-12">
            @if($errors->has())
                <div id="form-errors">
                    <p>Bazı Hatalar Oluştu:</p>
                    <ul>
                        @foreach($errors->all() as $error)
                            <div class="alert alert-danger">{{ $error }}</div>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

    @include('flash::message')

    <!-- Main Heading Starts -->
    <h2 class="main-heading text-center">
        Kayıt Ol <br />
    <span><i class="fa fa-user"></i> Yeni Kayıt Oluştur</span>
    </h2>
    <!-- Main Heading Ends -->
    <!-- Registration Section Starts -->
    <section class="registration-area">
        <div class="row">
            <div class="col-sm-12">

                <div role="tabpanel" id="custom-tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs custom-nav" role="tablist">
                        <li role="presentation" class="active col-md-4 custom-tab">
                            <a href="#mapa1" aria-controls="home" role="tab" data-toggle="tab">
                                <div class="col-md-4 col-md-offset-3 text-center">
                                    <i class="fa fa-thumbs-o-up fa-5x"></i>
                                    <p><h2 style="color:#fff9fc">İmalatçı <br/>(Toptancı/Tedarikçi)</h2></p>
                                </div>
                            </a>
                        </li>
                        <li role="presentation" class="col-md-4 custom-tab">
                            <a href="#mapa2" aria-controls="profile" role="tab" data-toggle="tab">
                                <div class="col-md-4 col-md-offset-3 text-center">
                                    <i class="fa fa-thumbs-o-up fa-5x"></i>
                                    <p><h2 style="color:#fff9fc">Perakendeci</h2></p>
                                </div>
                            </a>
                        </li>
                        <li role="presentation" class="col-md-4 custom-tab">
                            <a href="#mapa3" aria-controls="profile" role="tab" data-toggle="tab">
                                <div class="col-md-4 col-md-offset-3 text-center">
                                    <i class="fa fa-thumbs-o-up fa-5x"></i>
                                    <p><h2 style="color:#fff9fc">Hizmet Firması</h2></p>
                                </div>
                            </a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="custom-tab-pane tab-pane active" id="mapa1">
                            <div class="panel panel-smart">
                                <div class="panel-heading">
                                    <h3 class="panel-title">İmalatçı(Toptancı/Tedarikçi) Yeni Kayıt</h3>
                                </div>
                                <div class="panel-body">
                                    <!-- İmalatçı Yeni Kayı -->
                                    {!! Form::open(array('url'=>'user/createimalatci','class'=>'form-horizontal')) !!}

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">Vergi Numarası :</label>
                                        <div class="col-sm-10">
                                            {!! Form::text('vergino',null,array('class'=>'form-control','placeholder'=>'Firma vergi numaranızı giriniz.')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">Vergi Dairesi :</label>
                                        <div class="col-sm-10">
                                            {!! Form::text('vergi_dairesi',null,array('class'=>'form-control','placeholder'=>'Firma ilgili vergi dairesini giriniz.')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">Ticaret Ünvanı :</label>
                                        <div class="col-sm-10">
                                            {!! Form::text('unvan',null,array('class'=>'form-control','placeholder'=>'Ticaret ünvanınız firma adınızdır.Örnek:Firma A.Ş')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Yetkili Ad Soyad :</label>
                                        <div class="col-sm-10">
                                            {!! Form::text('yetkili',null,array('class'=>'form-control','placeholder'=>'Yetkili Ad Soyad Giriniz')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Yetkili Cep Telefonu :</label>
                                        <div class="col-sm-10">
                                            {!! Form::text('yetkili_cep',null,array('class'=>'form-control','placeholder'=>'Yetkilinin Cep Telefonunu Giriniz.')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">İl :</label>
                                        <div class="col-sm-10">
                                            <select name="city" id="city_imalatci" class="form-control city">
                                                <option value="">İl Seçiniz</option>
                                                @foreach($cities as $city)
                                                    <option value="{{$city->city_code}}">{{ $city->city }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">İlçe :</label>
                                        <div class="col-sm-10" id="city_town_imalatci">
                                            <select name="city_town" class="form-control">
                                                <option value="0">MERKEZ</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">Açık Adres :</label>
                                        <div class="col-sm-10">
                                            {!! Form::textarea('adres',null,array('class'=>'form-control','placeholder'=>'Adresinizi giriniz.')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">Telefon :</label>
                                        <div class="col-sm-10">
                                            {!! Form::text('telefon',null,array('class'=>'form-control','placeholder'=>'Telefon numaranızı giriniz.')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">Fax :</label>
                                        <div class="col-sm-10">
                                            {!! Form::text('fax',null,array('class'=>'form-control','placeholder'=>'Fax numaranızı giriniz.')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">Eposta :</label>
                                        <div class="col-sm-10">
                                            {!! Form::text('email',null,array('class'=>'form-control','placeholder'=>'Eposta adresini giriniz.')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">Web Sitesi :</label>
                                        <div class="col-sm-10">
                                            {!! Form::text('web',null,array('class'=>'form-control','placeholder'=>'Web siteniz varsa adresini giriniz.')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">Şirketin Kurulduğu Tarih :</label>
                                        <div class="col-sm-10">
                                            <input class="form-control" type="text" value="12-02-2012" name="established_year" id="dpYears">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Şirketteki Çalışan Sayısı</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="employees_number" placeholder="Şirketteki Toplam Çalışan Sayısı"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Şirket Profili</label>
                                        <div class="col-sm-10">
                                            <textarea name="imalatci_profil" id="imalatci_profil" class="form-control" cols="30" rows="10" placeholder="Şirketin Genel Profilini Yazınız"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">Şifre :</label>
                                        <div class="col-sm-10">
                                            {!! Form::password('password', array('class' => 'form-control','placeholder'=>'Kullanıcı Şifrenizi belirleyiniz.')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label"> Şifre(Tekrar) :</label>
                                        <div class="col-sm-10">
                                            {!! Form::password('password_confirmation', array('class' => 'form-control','placeholder'=>'Kullanıcı Şifrenizi tekrar giriniz.')) !!}
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            {!! Form::button('Kayıt Ol', array('type' => 'submit', 'class' => 'btn btn-black'))!!}
                                        </div>
                                    </div>


                                    {!! Form::close() !!}


                                    <!-- Registration Form Starts -->
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="custom-tab-pane  tab-pane" id="mapa2">
                            <div class="panel panel-smart">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Perakendeci Yeni Kayıt</h3>
                                </div>
                                <div class="panel-body">

                                    <!--Tedarikçi Yeni kayıt-->

                                    {!! Form::open(array('url'=>'user/createperakendeci','class'=>'form-horizontal')) !!}

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">Ad Soyad :</label>
                                        <div class="col-sm-10">
                                            {!! Form::text('fullname',null,array('class'=>'form-control','placeholder'=>'Ad Soyadınızı giriniz.')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">Eposta :</label>
                                        <div class="col-sm-10">
                                            {!! Form::text('email',null,array('class'=>'form-control','placeholder'=>'Eposta adresiniz aynı zamanda kullanıcı adınız olacaktır.')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">İl :</label>
                                        <div class="col-sm-10">
                                            <select name="city" id="city" class="form-control city">
                                                <option value="">İl Seçiniz</option>
                                                @foreach($cities as $city)
                                                    <option value="{{$city->city_code}}">{{ $city->city }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group" >
                                        <label for="inputFname" class="col-sm-2 control-label">İlçe Seçiniz :</label>
                                        <div class="col-sm-10">
                                            <select name="city_town" class="form-control">
                                                <option value="0">MERKEZ</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">Açık Adres :</label>
                                        <div class="col-sm-10">
                                            {!! Form::textarea('adres',null,array('class'=>'form-control','placeholder'=>'Adresinizi giriniz.')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">Telefon :</label>
                                        <div class="col-sm-10">
                                            {!! Form::text('telefon',null,array('class'=>'form-control','placeholder'=>'Telefon numaranızı giriniz.')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">Şifre :</label>
                                        <div class="col-sm-10">
                                            {!! Form::password('password', array('class' => 'form-control','placeholder'=>'Kullanıcı Şifrenizi belirleyiniz.')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label"> Şifre(Tekrar) :</label>
                                        <div class="col-sm-10">
                                            {!! Form::password('password_confirmation', array('class' => 'form-control','placeholder'=>'Kullanıcı Şifrenizi tekrar giriniz.')) !!}
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            {!! Form::button('Kayıt Ol', array('type' => 'submit', 'class' => 'btn btn-black'))!!}
                                        </div>
                                    </div>


                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>

                        <div role="tabpanel" class="custom-tab-pane tab-pane active" id="mapa3">
                            <div class="panel panel-smart">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Hizmet Firması Yeni Kayıt</h3>
                                </div>
                                <div class="panel-body">
                                    <!-- İmalatçı Yeni Kayı -->
                                    {!! Form::open(array('url'=>'user/createhizmetfirmasi','class'=>'form-horizontal')) !!}

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">Vergi Numarası :</label>
                                        <div class="col-sm-10">
                                            {!! Form::text('vergino',null,array('class'=>'form-control','placeholder'=>'Firma vergi numaranızı giriniz.')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">Vergi Dairesi :</label>
                                        <div class="col-sm-10">
                                            {!! Form::text('vergi_dairesi',null,array('class'=>'form-control','placeholder'=>'Firma ilgili vergi dairesini giriniz.')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">Ticaret Ünvanı :</label>
                                        <div class="col-sm-10">
                                            {!! Form::text('unvan',null,array('class'=>'form-control','placeholder'=>'Ticaret ünvanınız firma adınızdır.Örnek:Firma A.Ş')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Yetkili Ad Soyad :</label>
                                        <div class="col-sm-10">
                                            {!! Form::text('yetkili',null,array('class'=>'form-control','placeholder'=>'Yetkili Ad Soyad Giriniz')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Yetkili Cep Telefonu :</label>
                                        <div class="col-sm-10">
                                            {!! Form::text('yetkili_cep',null,array('class'=>'form-control','placeholder'=>'Yetkilinin Cep Telefonunu Giriniz.')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">İl :</label>
                                        <div class="col-sm-10">
                                            <select name="city" id="city_imalatci" class="form-control city">
                                                <option value="">İl Seçiniz</option>
                                                @foreach($cities as $city)
                                                    <option value="{{$city->city_code}}">{{ $city->city }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">İlçe :</label>
                                        <div class="col-sm-10" id="city_town_imalatci">
                                            <select name="city_town" class="form-control">
                                                <option value="0">MERKEZ</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">Açık Adres :</label>
                                        <div class="col-sm-10">
                                            {!! Form::textarea('adres',null,array('class'=>'form-control','placeholder'=>'Adresinizi giriniz.')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">Telefon :</label>
                                        <div class="col-sm-10">
                                            {!! Form::text('telefon',null,array('class'=>'form-control','placeholder'=>'Telefon numaranızı giriniz.')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">Fax :</label>
                                        <div class="col-sm-10">
                                            {!! Form::text('fax',null,array('class'=>'form-control','placeholder'=>'Fax numaranızı giriniz.')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">Eposta :</label>
                                        <div class="col-sm-10">
                                            {!! Form::text('email',null,array('class'=>'form-control','placeholder'=>'Eposta adresini giriniz.')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">Web Sitesi :</label>
                                        <div class="col-sm-10">
                                            {!! Form::text('web',null,array('class'=>'form-control','placeholder'=>'Web siteniz varsa adresini giriniz.')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">Hizmet Kategorisi:</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="service_category" id="service_category">
                                                <option value="">Hizmet Kategorisini Seçiniz</option>
                                                @foreach(App\ServiceCategory::all() as $service_category)
                                                    <option value="{{$service_category->id}}">{{$service_category->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">Hizmet Alt Kategorisi:</label>
                                        <div class="col-sm-10">
                                            <div id="service_sub">
                                                <select class="form-control" name="service_subcategory[]" id="service_subcategory" multiple>
                                                    <option value="">Hizmet Alt Kategorisini Seçiniz</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">Şirketin Kurulduğu Tarih :</label>
                                        <div class="col-sm-10">
                                            <input class="form-control" type="text" value="12-02-2012" name="established_year" id="dpYears2">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Şirketteki Çalışan Sayısı</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="employees_number" placeholder="Şirketteki Toplam Çalışan Sayısı"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Şirket Profili</label>
                                        <div class="col-sm-10">
                                            <textarea name="imalatci_profil" id="imalatci_profil" class="form-control" cols="30" rows="10" placeholder="Şirketin Genel Profilini Yazınız"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label">Şifre :</label>
                                        <div class="col-sm-10">
                                            {!! Form::password('password', array('class' => 'form-control','placeholder'=>'Kullanıcı Şifrenizi belirleyiniz.')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputFname" class="col-sm-2 control-label"> Şifre(Tekrar) :</label>
                                        <div class="col-sm-10">
                                            {!! Form::password('password_confirmation', array('class' => 'form-control','placeholder'=>'Kullanıcı Şifrenizi tekrar giriniz.')) !!}
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            {!! Form::button('Kayıt Ol', array('type' => 'submit', 'class' => 'btn btn-black'))!!}
                                        </div>
                                    </div>


                                    {!! Form::close() !!}


                                    <!-- Registration Form Starts -->
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                </div>
                <!-- Registration Block Ends -->
            </div>

        </div>
    </section>
    <!-- Registration Section Ends -->
</div>
<!-- Main Container Ends -->

@endsection