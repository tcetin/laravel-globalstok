@extends('app')

@section('content')
        <!-- Main Container Starts -->
<div id="main-container" class="container">
    <!-- Breadcrumb Starts -->
    <ol class="breadcrumb">
        <li><a href="/">Anasayfa</a></li>
        <li class="active">Giriş</li>
    </ol>
    <!-- Breadcrumb Ends -->
    <!-- Main Heading Starts -->
    <h2 class="main-heading text-center">
        Giriş Yap ya da Kayıt Ol
    </h2>
    <!-- Main Heading Ends -->
    <!-- Login Form Section Starts -->
    <section class="login-area">
        <div class="row">
            <div class="col-sm-6">
                <!-- Login Panel Starts -->
                <div class="panel panel-smart">
                    <div class="panel-heading">
                        <h3 class="panel-title">İmalatçı Girişi</h3>
                    </div>
                    <div class="panel-body">
                        <p>
                            Var olan imalatçı kaydınızla buradan giriş yapabilirsiniz.
                        </p>
                        <!-- Login Form Starts -->
                        <form class="form-inline" role="form" action="/login/imalatcilogin">

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            {!! csrf_field() !!}

                            <div class="form-group">
                                <label class="sr-only" for="exampleInputEmail2"></label>
                                <input type="text" class="form-control" id="exampleInputEmail2" placeholder="Vergi Numarası">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="exampleInputPassword2">Password</label>
                                <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Şifre">
                            </div>
                            <button type="submit" class="btn btn-black">
                               Giriş Yap
                            </button>
                        </form>
                        <!-- Login Form Ends -->
                    </div>
                </div>

                <div class="panel panel-smart">
                    <div class="panel-heading">
                        <h3 class="panel-title">Tedarikçi(Toptancı) Girişi</h3>
                    </div>
                    <div class="panel-body">
                        <p>
                            Var olan tedarikçi kaydınızla buradan giriş yapabilirsiniz.
                        </p>
                        <!-- Login Form Starts -->
                        <form class="form-inline" role="form">
                            <div class="form-group">
                                <label class="sr-only" for="exampleInputEmail2">Username</label>
                                <input type="text" class="form-control" id="exampleInputEmail2" placeholder="Username">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="exampleInputPassword2">Password</label>
                                <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password">
                            </div>
                            <button type="submit" class="btn btn-black">
                                Login
                            </button>
                        </form>
                        <!-- Login Form Ends -->
                    </div>
                </div>
                <!-- Login Panel Ends -->
            </div>
            <div class="col-sm-6">
                <!-- Account Panel Starts -->
                <div class="panel panel-smart">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Herhangi bir giriş hesabım yok?
                        </h3>
                    </div>
                    <div class="panel-body">
                        <p>
                            İmalatçı ya da tedarikçi(toptancı) olarak giriş yapabilmeniz için  kayıt olmanız gerekmektedir.Kayıt olmak için <b>Kayıt Ol</b> butonuna tıklayınız.
                        </p>
                        <a href="/register" class="btn btn-black">
                            Kayıt Ol
                        </a>
                    </div>
                </div>
                <!-- Account Panel Ends -->
            </div>
        </div>
    </section>
    <!-- Login Form Section Ends -->
</div>
<!-- Main Container Ends -->
@endsection