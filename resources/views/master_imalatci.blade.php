<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="_token" content="{!! csrf_token() !!}"/>

    <title>Global Stokçu İmalatçı Paneli</title>

    <!-- Styles -->
    <!-- Bootstrap CSS -->
    {!! HTML::style('metro_admin/css/bootstrap.min.css') !!}
    <!-- jQuery UI -->
    {!! HTML::style('metro_admin/css/jquery-ui.css') !!}
    <!-- Font awesome CSS -->
    {!! HTML::style('metro_admin/css/font-awesome.min.css') !!}
    <!-- Custom CSS -->
    {!! HTML::style('metro_admin/css/style.css') !!}
    <!-- Widgets stylesheet -->
    {!! HTML::style('metro_admin/css/widgets.css') !!}

    {!! HTML::style('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css') !!}

    <!--file input plugin-->
    {!! HTML::style('admin_assets/bootstrap-fileinput-master/css/fileinput.css') !!}

    <!--Fancybox-->

    {!! HTML::style('admin_assets/fancybox/source/jquery.fancybox.css') !!}

    <!--datatables css-->
    {!! HTML::style('admin_assets/bower_components/datatables/media/css/jquery.dataTables.min.css') !!}


    {!! HTML::style('admin_assets/bower_components/summernote-master/dist/summernote.css')!!}

    <style>

        .delete {
            position: relative;
            vertical-align: middle;
            display: inline-block;
        }

        .delete:after {
            position:absolute;
            top:0;
            right:0;
            height: 20px;
            width: 20px;
        }

        .delete .delete-button {
            position:absolute;
            top:0;
            right:0;
            height: 20px;
            width: 20px;
        }


        .delete button.not-verified,button.verified{
            position:absolute;
            top:0;
            left:0;
            height: 20px;
            width: 20px;
            text-align: center;
        }
    </style>


</head>

<body>
 @include('partials.imalatci.navbar_toplinks')

 @include('partials.imalatci.sidebar')

 <div class="mainbar">

    @yield('content')

 </div>
    <!-- /#page-wrapper -->
 <span class="totop" style="display: none;"><a href="#"><i class="fa fa-chevron-up"></i></a></span>
</div>
<!-- /#wrapper -->

    <!-- jQuery -->
    {!! HTML::script('admin_assets/bower_components/jquery/dist/jquery.min.js')!!}

    <!-- Javascript files -->
    <!-- jQuery -->
    {!! HTML::script('metro_admin/js/jquery.js')!!}
    <!-- Bootstrap JS -->
    {!! HTML::script('metro_admin/js/bootstrap.min.js')!!}
    <!-- jQuery UI -->
    {!! HTML::script('metro_admin/js/jquery-ui.min.js')!!}
    <!-- jQuery Flot -->
    {!! HTML::script('metro_admin/js/excanvas.min.js')!!}

    {!! HTML::script('metro_admin/js/sparklines.js')!!}
    <!-- Respond JS for IE8 -->
    {!! HTML::script('metro_admin/js/respond.min.js')!!}
    <!-- HTML5 Support for IE -->
    {!! HTML::script('metro_admin/js/html5shiv.js')!!}
    <!-- Custom JS -->
    {!! HTML::script('metro_admin/js/custom.js')!!}

    <!-- Script for this page -->
    {!! HTML::script('metro_admin/js/sparkline-index.js')!!}
    <!--Select2-->
    {!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js')!!}

    <!--Fancybox-->
    {!! HTML::script('admin_assets/fancybox/source/jquery.fancybox.js') !!}

    <!--file input plg-->
    {!! HTML::script('admin_assets/bootstrap-fileinput-master/js/plugins/canvas-to-blob.js')!!}
    {!! HTML::script('admin_assets/bootstrap-fileinput-master/js/fileinput.js')!!}
    {!! HTML::script('admin_assets/bootstrap-fileinput-master/js/fileinput_locale_tr.js')!!}


   <!--datatables js-->
    {!! HTML::script('admin_assets/bower_components/datatables/media/js/jquery.dataTables.min.js')!!}


    <!-- Mask Money-->
    {!! HTML::script('admin_assets/bower_components/jquery-maskmoney-master/dist/jquery.maskMoney.min.js')!!}

 {!! HTML::script('admin_assets/bower_components/summernote-master/dist/summernote.js')!!}

 {!! HTML::script('admin_assets/bower_components/summernote-master/lang/summernote-tr-TR.js')!!}




     <script>

         $(function(){

             $.ajaxSetup({
                 headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
             });


             $("#product_main_img").fileinput({
                 language:"tr",
                 uploadUrl: "",
                 allowedFileExtensions: ["jpeg","jpg", "png", "gif"],
                 maxImageWidth: 250,
                 maxImageHeight:280,
                 maxFileCount: 1,
                 resizeImage: true,
                 showUpload:false
             }).on('filepreupload', function() {
             }).on('fileuploaded', function(event, data) {
                 $('#kv-success-box').append(data.response.link);
                 $('#kv-success-modal').modal('show');
             });

             $("input.up").each(function() {
                 var $input = $(this);
                 $input.fileinput({
                     language: "tr",
                     uploadUrl: "/imalatci/products/updatemainimage",
                     allowedFileExtensions: ["jpeg", "jpg", "png", "gif"],
                     maxImageWidth: 250,
                     maxImageHeight: 280,
                     maxFileCount: 1,
                     resizeImage: true,
                     uploadExtraData: function() {
                         return { product_id: $input.attr('id') };
                     }
                 }).on('filepreupload', function (event, data) {
                 }).on('fileuploaded', function (event, data,response) {

                     var obj = data.response;

                     if(obj.res == 1) {
                         if(obj.verified==1) {
                             $('div#main-gallery').html(
                                     '<div  data-image="' + obj.id + '" class="icon-remove blue delete delete-ana">' +
                                     '<button type="button" data-toggle="tooltip" title="Resim yönetici tarafından onaylanmış."  class="btn btn-success btn-xs verified"><i class="fa fa-check"></i></button>'+
                                     '<button type="button" data-image="' + obj.id + '" class="btn btn-danger btn-xs delete-button delete-anaurun"><i class="fa fa-times"></i></button>' +
                                     '<a href="/' + obj.image_path + '" class="fancybox">' +
                                     '<img src="/' + obj.image_path + '" style="max-width:80px;max-height:80px"/>' +
                                     '</a>' +
                                     '</div>');
                         }else{
                             $('div#main-gallery').html(
                                     '<div  data-image="' + obj.id + '" class="icon-remove blue delete delete-ana">' +
                                     '<button type="button" data-toggle="tooltip" title="Resim yönetici tarafından onay bekliyor."  class="btn btn-danger btn-xs not-verified"><i class="fa fa-exclamation"></i></button>'+
                                     '<button type="button" data-image="' + obj.id + '" class="btn btn-danger btn-xs delete-button delete-anaurun"><i class="fa fa-times"></i></button>' +
                                     '<a href="/' + obj.image_path + '" class="fancybox">' +
                                     '<img src="/' + obj.image_path + '" style="max-width:80px;max-height:80px"/>' +
                                     '</a>' +
                                     '</div>');

                         }
                     }

                     alert(obj.result);
                 });
             });




             $("input.ups").each(function() {
                 var $input = $(this);
                 $input.fileinput({
                     uploadUrl: "/imalatci/products/uploadimages",
                     allowedFileExtensions: ["jpeg","jpg", "png", "gif"],
                     maxImageWidth: 500,
                     maxImageHeight: 400,
                     maxFileCount: 5,
                     resizeImage: true,
                     language:"tr",
                     uploadExtraData: function() {
                         return { product_id: $input.attr('product_id') };
                     }
                 }).on('filepreupload', function() {

                 }).on('fileuploaded', function(event, data,response) {

                    var obj = data.response;

                     if(obj.res == 1) {
                         if(obj.verified==1) {

                             $('div#my-gallery').append('<div data-image="' + obj.img_id + '" class="icon-remove blue delete delete-img">' +
                             '<button type="button" data-toggle="tooltip" title="Resim yönetici tarafından onaylanmış."  class="btn btn-success btn-xs verified"><i class="fa fa-check"></i></button>'+
                             ' <button type="button" data-image="' + obj.img_id + '" class="btn btn-danger btn-xs delete-button delete-urun-resmi "><i class="fa fa-times"></i></button>' +
                             '<a href="/' + obj.img_path + '" class="fancybox" data-fancybox-group="gallery-1">' +
                             '<img src="/' + obj.img_path + '" style="max-width:80px;max-height:80px"/>' +
                             '</a>');
                         }else{
                             $('div#my-gallery').append('<div data-image="' + obj.img_id + '" class="icon-remove blue delete delete-img">' +
                             '<button type="button" data-toggle="tooltip" title="Resim yönetici tarafından onay bekliyor."  class="btn btn-danger btn-xs not-verified"><i class="fa fa-exclamation"></i></button>'+
                             ' <button type="button" data-image="' + obj.img_id + '" class="btn btn-danger btn-xs delete-button delete-urun-resmi "><i class="fa fa-times"></i></button>' +
                             '<a href="/' + obj.img_path + '" class="fancybox" data-fancybox-group="gallery-1">' +
                             '<img src="/' + obj.img_path + '" style="max-width:80px;max-height:80px"/>' +
                             '</a>');
                         }

                     }

                     alert(obj.result);
                 })

             });


             //ürün resmi silme

             $(document).on('click','button.delete-anaurun',function(){
                 var id = $(this).attr('data-image');

                 $('div.delete-ana[data-image='+id+']').html('<img src="/images/ajax-loader.gif" style="max-width:80px;max-height:80px"/>');

                 $.ajax({
                     type:'GET',
                     url:'/imalatci/products/deletemainimage',
                     data:{id:id},
                     success:function(res){
                         $('div.delete-ana[data-image='+id+']').html(res.result);
                     }
                 });


             });

             $(document).on('click','button.delete-urun-resmi',function(){
                 var img_id = $(this).attr('data-image');

                 $('div.delete-img[data-image='+img_id+']').html('<img src="/images/ajax-loader.gif" style="max-width:80px;max-height:80px"/>');

                 $.ajax({
                     type:'GET',
                     url:'/imalatci/products/deleteimage',
                     data:{id:img_id},
                     success:function(res){
                         $('div.delete-img[data-image='+img_id+']').html(res.result);
                     }
                 });


             });


             //logo

             $("input.logo").each(function() {
                 var $input = $(this);
                 $input.fileinput({
                     uploadUrl: "/imalatci/uploadlogo",
                     allowedFileExtensions: ["jpeg","jpg", "png", "gif"],
                     maxImageWidth: 64,
                     maxImageHeight: 64,
                     maxFileCount: 1,
                     resizeImage: true,
                     language:"tr",
                     uploadExtraData: function() {
                         return { imalatci_id: $input.attr('data-imalatci') };
                     }
                 }).on('filepreupload', function() {

                 }).on('fileuploaded', function(event, data,response) {

                        var obj = data.response;


                             $('div#logo').html('<a href="/' + obj.img_path + '" class="fancybox" data-fancybox-group="gallery-1">' +
                             '<img src="/' + obj.img_path + '" style="max-width:80px;max-height:80px"/>' +
                             '</a>');



                        alert(obj.result);
                 })

             });

             //yetkili resim

             $("input.yetkili_resim").each(function() {
                 var $input = $(this);
                 $input.fileinput({
                     uploadUrl: "/imalatci/uploadyetkiliresim",
                     allowedFileExtensions: ["jpeg","jpg", "png", "gif"],
                     maxImageWidth: 64,
                     maxImageHeight: 64,
                     maxFileCount: 1,
                     resizeImage: true,
                     language:"tr",
                     uploadExtraData: function() {
                         return { imalatci_id: $input.attr('data-imalatci') };
                     }
                 }).on('filepreupload', function() {

                 }).on('fileuploaded', function(event, data,response) {

                     var obj = data.response;


                     $('div#yetkiliresim').html('<a href="/' + obj.img_path + '" class="fancybox" data-fancybox-group="gallery-1">' +
                     '<img src="/' + obj.img_path + '" style="max-width:80px;max-height:80px"/>' +
                     '</a>');



                     alert(obj.result);
                 })

             });

             $("input.sertifika").each(function() {
                 var $input = $(this);
                 $input.fileinput({
                     uploadUrl: "/imalatci/uploadsertifika",
                     allowedFileExtensions: ["jpeg","jpg", "png", "gif"],
                     maxImageWidth: 500,
                     maxImageHeight: 400,
                     maxFileCount: 5,
                     resizeImage: true,
                     language:"tr",
                     uploadExtraData: function() {
                         return { imalatci_id: $input.attr('data-imalatci') };
                     }
                 }).on('filepreupload', function() {

                 }).on('fileuploaded', function(event, data,response) {

                     var obj = data.response;

                     if(obj.res == 1) {
                         if(obj.verified==1) {

                             $('div#my-gallery').append('<div data-image="' + obj.img_id + '" class="icon-remove blue delete delete-img">' +
                             '<button type="button" data-toggle="tooltip" title="Resim yönetici tarafından onaylanmış."  class="btn btn-success btn-xs verified"><i class="fa fa-check"></i></button>'+
                             ' <button type="button" data-image="' + obj.img_id + '" class="btn btn-danger btn-xs delete-button delete-urun-resmi "><i class="fa fa-times"></i></button>' +
                             '<a href="/' + obj.img_path + '" class="fancybox" data-fancybox-group="gallery-1">' +
                             '<img src="/' + obj.img_path + '" style="max-width:80px;max-height:80px"/>' +
                             '</a>');
                         }else{
                             $('div#my-gallery').append('<div data-image="' + obj.img_id + '" class="icon-remove blue delete delete-img">' +
                             '<button type="button" data-toggle="tooltip" title="Resim yönetici tarafından onay bekliyor."  class="btn btn-danger btn-xs not-verified"><i class="fa fa-exclamation"></i></button>'+
                             ' <button type="button" data-image="' + obj.img_id + '" class="btn btn-danger btn-xs delete-button delete-urun-resmi "><i class="fa fa-times"></i></button>' +
                             '<a href="/' + obj.img_path + '" class="fancybox" data-fancybox-group="gallery-1">' +
                             '<img src="/' + obj.img_path + '" style="max-width:80px;max-height:80px"/>' +
                             '</a>');
                         }

                     }

                     alert(obj.result);
                 })

             });

             $("input.resim").each(function() {
                 var $input = $(this);
                 $input.fileinput({
                     uploadUrl: "/imalatci/uploadresim",
                     allowedFileExtensions: ["jpeg","jpg", "png", "gif"],
                     maxImageWidth: 500,
                     maxImageHeight: 400,
                     maxFileCount: 5,
                     resizeImage: true,
                     language:"tr",
                     uploadExtraData: function() {
                         return { imalatci_id: $input.attr('data-imalatci') };
                     }
                 }).on('filepreupload', function() {

                 }).on('fileuploaded', function(event, data,response) {

                     var obj = data.response;

                     if(obj.res == 1) {
                         if(obj.verified==1) {

                             $('div#resim-gallery').append('<div data-image="' + obj.img_id + '" class="icon-remove blue delete delete-img">' +
                             '<button type="button" data-toggle="tooltip" title="Resim yönetici tarafından onaylanmış."  class="btn btn-success btn-xs verified"><i class="fa fa-check"></i></button>'+
                             ' <button type="button" data-image="' + obj.img_id + '" class="btn btn-danger btn-xs delete-button delete-urun-resmi "><i class="fa fa-times"></i></button>' +
                             '<a href="/' + obj.img_path + '" class="fancybox" data-fancybox-group="gallery-1">' +
                             '<img src="/' + obj.img_path + '" style="max-width:80px;max-height:80px"/>' +
                             '</a>');
                         }else{
                             $('div#resim-gallery').append('<div data-image="' + obj.img_id + '" class="icon-remove blue delete delete-img">' +
                             '<button type="button" data-toggle="tooltip" title="Resim yönetici tarafından onay bekliyor."  class="btn btn-danger btn-xs not-verified"><i class="fa fa-exclamation"></i></button>'+
                             ' <button type="button" data-image="' + obj.img_id + '" class="btn btn-danger btn-xs delete-button delete-urun-resmi "><i class="fa fa-times"></i></button>' +
                             '<a href="/' + obj.img_path + '" class="fancybox" data-fancybox-group="gallery-1">' +
                             '<img src="/' + obj.img_path + '" style="max-width:80px;max-height:80px"/>' +
                             '</a>');
                         }

                     }

                     alert(obj.result);
                 })

             });


             $(document).on('click','button.delete-sertifika',function(){
                 var img_id = $(this).attr('data-image');

                 $('div.delete-img[data-image='+img_id+']').html('<img src="/images/ajax-loader.gif" style="max-width:80px;max-height:80px"/>');

                 $.ajax({
                     type:'GET',
                     url:'/imalatci/deletecertificate',
                     data:{id:img_id},
                     success:function(res){
                         $('div.delete-img[data-image='+img_id+']').html(res.result);
                     }
                 });


             });

             $(document).on('click','button.delete-resim',function(){
                 var img_id = $(this).attr('data-image');

                 $('div.delete-img[data-image='+img_id+']').html('<img src="/images/ajax-loader.gif" style="max-width:80px;max-height:80px"/>');

                 $.ajax({
                     type:'GET',
                     url:'/imalatci/deleteresim',
                     data:{id:img_id},
                     success:function(res){
                         $('div.delete-img[data-image='+img_id+']').html(res.result);
                     }
                 });


             });

             $(document).on('change','select.city',function(){

                 var city_code = $(this).val();

                 var town_container ='<option value="0">MERKEZ</option>';

                 $.ajax({
                     type: 'GET',
                     url: '/fpage/citytown',
                     data: {city_code:city_code},
                     success: function (msg) {

                         $.each($.parseJSON(msg), function(idx, obj) {
                             town_container +='<option value="'+ obj.id+'">'+ obj.name+'</option>';
                         });

                         $("select[name='city_town']").html(town_container);

                     }
                 });
             });

             $(document).on('change','select#subcategory',function(){

                 $('div#subprop').html('<img src="/images/AjaxLoader.gif" style="max-width=80px;max-height=80px;margin-left: 200px"/>');

                 var subcat_id = $(this).val();

                 $.ajax({
                     type: 'GET',
                     url: '/fpage/subprops',
                     data: {id:subcat_id},
                     success: function (msg) {
                         $('div#subprop').html(msg);
                     }
                 });
             });

             $('textarea#imalatci_profil').summernote({
                 focus: true,                // set focus to editable area after initializing summernote
                 height:300,
                 lang: 'tr-TR',
             });



         });

     </script>

 <script>
     $(function(){

         $('select#subcategory,select#city,select#subproperty,select#payment_method').select2({width:'100%'});
         $('input.price').maskMoney({
             thousands:'',
             decimal:'.'
         });
         $('.fancybox').fancybox();

         $('table#productList').dataTable({
             language :{
                 "sInfo": "Toplam _TOTAL_ kayıttan  _START_  ile  _END_  arası kayıt gösteriliyor.",
                 "infoEmpty": "Herhangi bir kayıt bulunamadı.",
                 "sSearch" : "Ara:",
                 "sZeroRecords": "Herhangi bir kayıt bulunamadı.",
                 "paginate": {
                     "first":      "İlk Kayıt",
                     "last":       "Son kayıt",
                     "next":       "Sonraki",
                     "previous":   "Önceki"
                 },
                 "sLengthMenu": " _MENU_ "

             }

         });


         // Group images by gallery using `data-fancybox-group` attributes
         var galleryId = 1;
         $('.editable-gallery').each( function() {
             $(this).find('a').attr('data-fancybox-group', 'gallery-' + galleryId++);
         });
         // Initialize Fancybox
         $('.editable-gallery a').fancybox({
             // Use the `alt` attribute for captions per http://fancyapps.com/fancybox/#useful
             beforeShow: function() {
                 var alt = this.element.find('img').attr('alt');
                 this.inner.find('img').attr('alt', alt);
                 this.title = alt;
             }
         });

         $('[data-toggle="tooltip"]').tooltip();

     });
 </script>




</body>

</html>
