<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_images',function(Blueprint $table){
            $table->increments('id');
            $table->integer('imalatci_id')->unsigned();
            $table->string('path');
            $table->boolean('verified')->default(0);
            $table->timestamps();

            $table->foreign('imalatci_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company_images');
    }
}
