<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceSubcategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_subcategories',function(Blueprint $table){
            $table->increments('id');
            $table->integer('service_category_id')->unsigned();
            $table->text('name');
            $table->timestamps();

            $table->foreign('service_category_id')
                ->references('id')
                ->on('service_categories')
                ->onDelete('cascade');
        });

        Schema::create('service_sub_category_user',function(Blueprint $table){
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('service_sub_category_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('service_sub_category_user',function(Blueprint $table){

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('service_sub_category_id')
                ->references('id')
                ->on('service_subcategories')
                ->onDelete('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('service_subcategories');
        Schema::drop('service_sub_category_user');
    }
}
