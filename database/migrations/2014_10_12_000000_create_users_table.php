<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->string('rol');
            $table->boolean('active')->default(1);
            $table->string('vergino')->nullable();
            $table->text('vergi_dairesi')->nullable();
            $table->text('unvan')->nullable();
            $table->integer('city_code');
            $table->integer('city_town');
            $table->text('adres')->nullable();
            $table->string('telefon')->nullable();
            $table->string('fax')->nullable();
            $table->string('web')->nullable();
            $table->string('logo')->nullable();
            $table->boolean('logo_verified')->default(0);
            $table->string('yetkili')->nullable();
            $table->string('yetkili_cep')->nullable();
            $table->string('yetkili_resim')->nullable();
            $table->string('yetkili_resim_verified')->default(0);
            $table->string('established_year')->nullable();
            $table->integer('employees_number')->nullable();
            $table->string('working_hours')->nullable();
            $table->text('imalatci_profil')->nullable();
            $table->boolean('verified')->default(0);
            $table->boolean('confirmed')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
