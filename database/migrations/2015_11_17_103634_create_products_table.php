<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories',function($table){
            $table->engine = "InnoDB";
            $table->increments('id');
            $table->string('name')->unique();
            $table->timestamps();
        });

        Schema::create('subcategories',function($table){
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('subproperties',function(Blueprint $table){
            $table->increments('id');
            $table->integer('subcategory_id')->unsigned();
            $table->text('name');
            $table->timestamps();
        });

        Schema::create('payment_method',function(Blueprint $table){
            $table->increments('id');
            $table->text('name');
            $table->timestamps();
        });

        Schema::create('payment_method_product',function(Blueprint $table){
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->integer('payment_method_id')->unsigned();
            $table->timestamps();
        });


        Schema::create('products', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->integer('subcategory_id')->unsigned();
            $table->integer('subprop_id')->unsigned();
            $table->integer('imalatci_id')->unsigned();
            $table->string('title');
            $table->text('description');
            $table->decimal('price',13,2);
            $table->integer('currency_id');
            $table->string('amount_type');
            $table->integer('amount');
            $table->integer('min_order');
            $table->boolean('avaliability')->default(1);
            $table->boolean('verified')->default(0);
            $table->boolean('published')->default(0);
            $table->boolean('vitrin')->default(0);
            $table->timestamps();

        });

        Schema::create('product_images',function(Blueprint $table){
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->string('path');
            $table->integer('image_type');
            $table->boolean('verified')->default(0);
            $table->timestamps();

        });

        Schema::create('product_ratings',function(Blueprint $table){
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->string('email');
            $table->string('name');
            $table->text('review');
            $table->integer('rating');
            $table->timestamps();
        });


        Schema::table('products',function(Blueprint $table){
            $table->foreign('category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('cascade');

            $table->foreign('subcategory_id')
                ->references('id')
                ->on('subcategories')
                ->onDelete('cascade');

            $table->foreign('imalatci_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('subprop_id')
                ->references('id')
                ->on('subproperties')
                ->onDelete('cascade');

        });

        Schema::table('subcategories',function(Blueprint $table){

            $table->foreign('category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('cascade');
        });

        Schema::table('subproperties',function(Blueprint $table){

            $table->foreign('subcategory_id')
                ->references('id')
                ->on('subcategories')
                ->onDelete('cascade');
        });

        Schema::table('product_images',function(Blueprint $table){

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');
        });

        Schema::table('product_ratings',function(Blueprint $table){

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');
        });


        Schema::table('payment_method_product',function(Blueprint $table){

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');

            $table->foreign('payment_method_id')
                ->references('id')
                ->on('payment_method')
                ->onDelete('cascade');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
        Schema::drop('categories');
        Schema::drop('subcategories');
        Schema::drop('subproperties');
        Schema::drop('product_images');
        Schema::drop('product_ratings');
        Schema::drop('payment_method');
        Schema::drop('payment_method_product');
    }
}
