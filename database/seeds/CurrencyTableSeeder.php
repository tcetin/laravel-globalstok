<?php

use Illuminate\Database\Seeder;

class CurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('currency')->insert([
            'name'=>'₺',
            'desc'=>'Türk Lirası'
        ]);

        DB::table('currency')->insert([
            'name'=>'$',
            'desc'=>'Amerikan Doları'
        ]);

        DB::table('currency')->insert([
            'name'=>'€',
            'desc'=>'Euro'
        ]);
    }
}
