<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //admin
        DB::table('users')->insert([
                'email'=>'gbstok@gmail.com',
                'password'=>Hash::make('bky_T@et!n+*'),
                'rol' => 1,
                'verified' => 1
        ]);

        //dummy imalatci

        DB::table('users')->insert([

                'email'=>'imalatci@gmail.com',
                'password'=>Hash::make('12345678'),
                'rol' => 2,
                'vergino' => '1234567890',
                'unvan' => 'deneme A.Ş',
                'city_code' => 1,
                'adres' => 'Adana Test Adresi',
                'telefon' => '5066060088',
                'fax' => '5066060088',
                'web' => 'webyok.com',
                'verified' => 1,
                'established_year'=>'02/02/2015',
                'employees_number'=>12,
                'imalatci_profil'=>'Deneme Profili'

        ]);

        //dummy perakendeci

        DB::table('users')->insert([

            'email'=>'tedarikci@gmail.com',
            'password'=>Hash::make('12345678'),
            'rol' => 3,
            'city_code' => 1,
            'adres' => 'Adana Test Adresi',
            'telefon' => '5066060088',
            'verified' => 1

        ]);
    }
}
