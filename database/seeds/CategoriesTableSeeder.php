<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //clear our tables

        $cat = Category::create(array(
            'name'         => 'Giysi',
        ));

        $this->command->info('categories tablosune ekleme yapıldı.');
    }
}
