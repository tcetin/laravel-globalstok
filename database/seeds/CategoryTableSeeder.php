<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name'=>'Giysi'
        ]);

        DB::table('categories')->insert([
            'name'=>'Kozmetik ve Kişisel Bakım'
        ]);

    }
}
