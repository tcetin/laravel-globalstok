<?php

use Illuminate\Database\Seeder;

class AmountTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('amount_types')->insert([
            'name' => 'Kg'
        ]);

        DB::table('amount_types')->insert([
            'name' => 'Adet'
        ]);
    }
}
