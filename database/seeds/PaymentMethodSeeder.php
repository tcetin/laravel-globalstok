<?php

use Illuminate\Database\Seeder;

class PaymentMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_method')->insert([
            'name'=>'PEŞİN',
        ]);

        DB::table('payment_method')->insert([
            'name'=>'ÇEK',
        ]);

        DB::table('payment_method')->insert([
            'name'=>'HAVALE',
        ]);

        DB::table('payment_method')->insert([
            'name'=>'EFT',
        ]);

        DB::table('payment_method')->insert([
            'name'=>'KREDİ KARTI',
        ]);
    }
}
