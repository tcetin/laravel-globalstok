<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'title'=>'Laptop-1',
            'description'=>'Lorem ipsum dolor sit amet, eam autem dicam forensibus in.',
            'price' => '2.000,50',
            'avaliability' => 1,
            'amount'=>10,
            'verified'=>1,
            'published'=>1,
            'vitrin'=>1,
            'category_id'=>1,
            'subcategory_id'=>1,
            'imalatci_id'=>2,
            'amount_type'=>2
        ]);

        DB::table('products')->insert([
            'title'=>'Laptop-2',
            'description'=>'Lorem ipsum dolor sit amet, eam autem dicam forensibus in.',
            'price' => '2.000,50',
            'avaliability' => 1,
            'amount'=>10,
            'verified'=>1,
            'published'=>1,
            'vitrin'=>1,
            'category_id'=>1,
            'subcategory_id'=>1,
            'imalatci_id'=>2,
            'amount_type'=>2
        ]);

        DB::table('products')->insert([
            'title'=>'Laptop-3',
            'description'=>'Lorem ipsum dolor sit amet, eam autem dicam forensibus in.',
            'price' => '2.000,50',
            'avaliability' => 1,
            'amount'=>10,
            'verified'=>1,
            'published'=>1,
            'vitrin'=>1,
            'category_id'=>1,
            'subcategory_id'=>1,
            'imalatci_id'=>2,
            'amount_type'=>2
        ]);

        DB::table('products')->insert([
            'title'=>'Laptop-3',
            'description'=>'Lorem ipsum dolor sit amet, eam autem dicam forensibus in.',
            'price' => '2.000,50',
            'avaliability' => 1,
            'amount'=>10,
            'verified'=>1,
            'published'=>1,
            'vitrin'=>1,
            'category_id'=>1,
            'subcategory_id'=>1,
            'imalatci_id'=>2,
            'amount_type'=>2
        ]);

        DB::table('products')->insert([
            'title'=>'Laptop-4',
            'description'=>'Lorem ipsum dolor sit amet, eam autem dicam forensibus in.',
            'price' => '2.000,50',
            'avaliability' => 1,
            'amount'=>10,
            'verified'=>1,
            'published'=>1,
            'vitrin'=>1,
            'category_id'=>1,
            'subcategory_id'=>1,
            'imalatci_id'=>2,
            'amount_type'=>2
        ]);

        DB::table('products')->insert([
            'title'=>'Laptop-5',
            'description'=>'Lorem ipsum dolor sit amet, eam autem dicam forensibus in.',
            'price' => '2.000,50',
            'avaliability' => 1,
            'amount'=>10,
            'verified'=>1,
            'published'=>1,
            'vitrin'=>1,
            'category_id'=>1,
            'subcategory_id'=>1,
            'imalatci_id'=>2,
            'amount_type'=>2

        ]);
    }
}
