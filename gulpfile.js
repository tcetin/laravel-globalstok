var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

 mix.sass('app.scss');

 mix.styles([
  'bootstrap.css',
  'magnific-popup.css',
  'responsive.css'
 ],null,'public/css');

 mix.scripts([
  'bootstrap-hover-dropdown.min.js',
  'custom.js',
  'jquery.magnific-popup.min.js',
  'map.js',
 ],null,'public/js');




});