<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected  $table = "subcategories";

    protected $fillable = ['name','category_id'];

    public static $rules = array('name'=>'required|min:3');

    public static $messages = array(
        'required' => ':attribute alanı boş bırakılamaz.',
        'min' => ':attribute alanı en az :min karakterden oluşmalı.'
    );

    public function categories()
    {
        $this->belongsTo('App\Category');
    }

    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function subprops()
    {
        return $this->hasMany('App\SubProperty');
    }
}
