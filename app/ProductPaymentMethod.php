<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPaymentMethod extends Model
{
    protected $table = 'payment_method_product';

    protected $fillable = ['product_id','payment_method_id'];

}
