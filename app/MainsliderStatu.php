<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainsliderStatu extends Model
{
    protected $table = "mainslider_statu";

    protected $fillable = ['statu'];

    public static $rules = ['statu'=>'required'];

    public static $messages = array(
        'required' => ':attribute alanı boş bırakılamaz'
    );
}
