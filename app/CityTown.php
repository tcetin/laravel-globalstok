<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityTown extends Model
{
    protected  $table = 'city_town';

    protected $fillable = ['city_code','name'];
}
