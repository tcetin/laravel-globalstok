<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redirect;

class MustBeUserToSendEmail
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check()  && auth()->user()->verified ==1 && auth()->user()->active == 1){
            return $next($request);
        }else{
            return view('pages.user.perakendecigiris');
        }
    }
}
