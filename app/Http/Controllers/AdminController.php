<?php

namespace App\Http\Controllers;

use App\Category;
use App\CertificateImage;
use App\CompanyImage;
use App\Mainslider;
use App\MainsliderStatu;
use App\Product;
use App\ProductImage;
use App\ServiceCategory;
use App\ServiceSubCategory;
use App\SubCategory;
use App\SubProperty;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Laracasts\Commander\CommanderTrait;

class AdminController extends Controller
{
    public function __construct() {
        $this->middleware('admin');
    }

    public function getIndex(){

       $data = DB::table('products')
                    ->join('categories', 'categories.id', '=', 'products.category_id')
                    ->join('subcategories','subcategories.id','=','products.subcategory_id')
                    ->select(DB::raw('count(products.id) as data, categories.name,subcategories.name as sub'))
                    ->groupBy('subcategories.name')
                    ->get();

        //dd($data);

        $category_name = Category::all('name');

        $c_name = array();

        foreach ($category_name as $c) {
            $c_name[]=$c['name'];
        }

        $series = array();
        $series_drill = array();

        $data =(array)$data;

        foreach ($data as $d) {
            $series[] = array(
                              "name" => $d->name,
                              "y" => $d->data,
                              "drilldown" => $d->sub
            );

            $series_drill[] = array(
                                "name" => $d->sub,
                                "id" => $d->sub,
                                "data" => array(
                                       array( $d->sub ,$d->data )
                                )
                            );
        }




        return view('pages.admin.index')
            ->with('data',$series)
            ->with('drill',$series_drill)
            ->with('category_name',$c_name);
    }

    ################## CATEGORY METODLARI ##################

    public function getCategory(){

        $categories = Category::all();

        return view('pages.admin.category')
            ->with('categories',$categories);
    }

    public function postCategorycreate()
    {
        $validator = Validator::make(Input::all(),Category::$rules,Category::$messages);

        if(!$validator->fails()){

            $category = new Category();
            $category->name=Input::get('name');

            $category->save();

            flash()->success('Kategori oluşturuldu.');

            return Redirect::to('admin/category');

        }

        flash()->important('Hata Oluştu');

        return Redirect::to('admin/category')
            ->withErrors($validator)
            ->withInput();
    }

    public function postSubcategorycreate()
    {
        $validator = Validator::make(Input::all(),SubCategory::$rules,SubCategory::$messages);

        if(!$validator->fails()){

            $subcategory = new SubCategory();
            $subcategory->name=Input::get('name');
            $subcategory->category_id=Input::get('cat_id');
            $subcategory->save();

            flash()->success('Alt Kategori oluşturuldu.');

            return Redirect::to('admin/category');

        }

        flash()->important('Hata Oluştu');

        return Redirect::to('admin/category')
            ->withErrors($validator)
            ->withInput();

    }

    public function postSubcategorydelete()
    {
        $subcategory = SubCategory::find(Input::get('id'));

        if($subcategory){
            $subcategory->delete();
            flash()->success('Alt Kategori Silindi');
            return Redirect::to('admin/category');
        }

        flash()->important('Hata Oluştu');
        return Redirect::to('admin/category');
    }

    public function postSubcategoryupdate()
    {
        $validator = Validator::make(Input::all(),SubCategory::$rules,SubCategory::$messages);

        $subcategory = SubCategory::find(Input::get('id'));

        $subcategory->name=Input::get('name');

        if($validator->fails()){
            flash()->important('Hata Oluştu');
            return Redirect::to('admin/category')
                ->withErrors($validator)
                ->withInput();
        }else{

            $subcategory->save();

            flash()->success('Alt Kategori güncelleme işlemi başarılı.');

            return Redirect::to('admin/category');

        }
    }



    /**
     * @return mixed
     */



    public function postCategoryupdate()
    {
        $validator = Validator::make(Input::all(),Category::$rules);

        $category = Category::find(Input::get('id'));

        $category->name=Input::get('name');

        if($validator->fails()){
            flash()->important('Hata Oluştu');
            return Redirect::to('admin/category')
                ->withErrors($validator)
                ->withInput();
        }else{

            $category->save();

            flash()->success('Kategori güncelleme işlemi başarılı.');

            return Redirect::to('admin/category');

        }

    }





    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postCategorydestroy()
    {
        $category = Category::find(Input::get('id'));

        if($category){
            $category->delete();
            flash()->success('Kategori Silindi');
            return Redirect::to('admin/category');
        }

        flash()->important('Hata Oluştu');
        return Redirect::to('admin/category');
    }

    ########### MAİNSLİDER METODLARI #############

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getMainslider()
    {
        $mainslider = Mainslider::all();

        $mainslider_statu = MainsliderStatu::all();


        return view('pages.admin.mainslider')
            ->with('mainslider',$mainslider)
            ->with('mainslider_statu',$mainslider_statu);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postMainslidercreate()
    {
        $validator = Validator::make(Input::all(),Mainslider::$rules,Mainslider::$messages);

        if(!$validator->fails()){

            $slide = new Mainslider();
            $slide->baslik = Input::get('baslik');
            $slide->metin = Input::get('metin');
            $image = Input::file('image');
            $filename = date('Y_m_d_H_i_s')."_".$image->getClientOriginalName();
            Image::make($image->getRealPath())->resize(256,256)->save('images/mainslider/'.$filename);
            $slide->photo_path = 'images/mainslider/'.$filename;

            $slide->button_text = Input::get('read_more');

            $slide->state = 1;

            $slide->save();

            flash()->success('Slide Oluşturuldu.');

            return Redirect::to('admin/mainslider');

        }

        flash()->important('Hata Oluştu');

        return Redirect::to('admin/mainslider')
            ->withErrors($validator)
            ->withInput();
    }

    public function postMainslidersliderstatu()
    {
        $validator = Validator::make(Input::all(),MainsliderStatu::$rules,MainsliderStatu::$messages);

        $slider = new MainsliderStatu();

        if(!$validator->fails()){

            if(empty($slider->count())){



                $slider->statu = Input::get('statu');

                $slider->save();

                flash()->success('Slider Durumu Değiştirildi.');

                return Redirect::to('admin/mainslider');

            }else{

                $maxId = DB::table('mainslider_statu')->max('id');

                $slide = MainsliderStatu::find($maxId);

                $slide->statu = Input::get('statu');

                $slide->save();

                flash()->success('Slider Durumu Değiştirildi.');

                return Redirect::to('admin/mainslider');


            }
        }

        flash()->important('Hata Oluştu');

        return Redirect::to('admin/mainslider')
            ->withErrors($validator)
            ->withInput();
    }

    public function postMainsliderupdate()
    {
        $validator = Validator::make(Input::all(),Mainslider::$rules,Mainslider::$messages);


        if($validator->fails()){
            flash()->important('Hata Oluştu');
            return Redirect::to('admin/mainslider')
                ->withErrors($validator)
                ->withInput();
        }else{


            $slide = Mainslider::find(Input::get('id'));;
            $slide->baslik = Input::get('baslik');
            $slide->metin = Input::get('metin');
            $image = Input::file('image');


            $filename = date('Y-m-d-H:i:s')."_".$image->getClientOriginalName();
            Image::make($image->getRealPath())->resize(256,256)->save('images/mainslider/'.$filename);
            $slide->photo_path = 'images/mainslider/'.$filename;

            $slide->button_text = Input::get('read_more');

            $slide->state =  Input::get('state');

            $slide->save();

            flash()->success('Slide Güncelleme İşlemi Başarılı.');

            return Redirect::to('admin/mainslider');


        }
    }

    public function postMainsliderdestroy()
    {
        $slider = Mainslider::find(Input::get('id'));

        if($slider){
            $slider->delete();
            flash()->success('Slider Silindi');
            return Redirect::to('admin/mainslider');
        }

        flash()->important('Hata Oluştu');
        return Redirect::to('admin/mainslider');
    }


    ############# KULLANICI İŞLEMLERİ #######

    public function getUsers(){
        return view('pages.admin.users');
    }

    public function getDeactiveduser(Request $request)
    {
        $imalatci = User::find($request->imalatci_id);

        $imalatci->active = 0;

        $imalatci->save();

        return 'Kullanıcı Pasif Yapıldı.';

    }

    public function getActiveduser(Request $request)
    {

        $imalatci = User::find($request->imalatci_id);

        $imalatci->active = 1;

        $imalatci->save();

        return 'Kullanıcı Aktif Yapıldı.';
    }

    public function getUnverifyuser(Request $request)
    {
        $imalatci = User::find($request->imalatci_id);

        $imalatci->verified= 0;

        $imalatci->save();


        return 'Firma Doğrulama Kaldırıldı.';

    }

    public function getVerifyuser(Request $request)
    {
        $imalatci = User::find($request->imalatci_id);

        $imalatci->verified= 1;

        $imalatci->save();

        return 'Firma Doğrulandı.';
    }

    ############### ÜRÜN İŞLEMLERİ ###########

    public function getProducts()
    {
        return view('pages.admin.products');
    }

    public function postUnverifyproduct()
    {
        $id = Input::get('product_id');

        $product = Product::find($id);

        $product->verified = 0;

        $product->save();

        flash()->success('Ürün onayı kaldırıldı.');
        return Redirect::to('admin/products');
    }

    public function postVerifyproduct()
    {

        $id = Input::get('product_id');



        $product = Product::find($id);

        $imalatci = User::find($product->imalatci_id);

        if($imalatci->confirmed==1){

            $product->verified = 1;

            $product->save();

            flash()->success('Ürün onaylandı.');
            return Redirect::to('admin/products');

        }else{
            flash()->error('Ürün onayı başarısız.Ürün imlatçısı/tedarikçisi onaylanmamış.');
            return Redirect::to('admin/products');
        }

    }

    public function postUnpublishedproduct()
    {
        $id = Input::get('product_id');

        $product = Product::find($id);

        $product->published = 0;

        $product->save();

        flash()->success('Ürün yayından kaldırıldı.');
        return Redirect::to('admin/products');
    }

    public function postPublishproduct()
    {
        $id = Input::get('product_id');

        $product = Product::find($id);

        $product->published = 1;

        $product->save();

        flash()->success('Ürün yayınlandı.');
        return Redirect::to('admin/products');
    }

    public function getVerifyimage(Request $request){

        $id = $request->id;

        $image = ProductImage::find($id);

        $image->verified = 1;

        $image->save();

        return '<i class="fa fa-check"></i> Resim onaylandı.';


    }

    public function getUnverifyimage(Request $request){

        $id = $request->id;

        $image = ProductImage::find($id);

        $image->verified = 0;

        $image->save();

        return '<i class="fa fa-times"></i> Resim onayı kaldırıldı.';


    }

    public function postUnvitrinproduct(){

        $id = Input::get('product_id');

        $image = Product::find($id);

        $image->vitrin = 0;

        $image->save();

        flash()->important('Ürün vitrinden kaldırıldı.');
        return Redirect::to('admin/products');

    }

    public function postVitrinproduct(){

        $id = Input::get('product_id');

        $image = Product::find($id);

        $image->vitrin = 1;

        $image->save();

        flash()->success('Ürün vitrinde yayınlandı.');
        return Redirect::to('admin/products');

    }


    /**
     * Alt Kategoriye özellik ekle
     * @return mixed
     */
    public function postAddsubprop()
    {
        $validator = Validator::make(Input::all(),SubProperty::$rules,SubProperty::$messages);

        if(!$validator->fails()) {

            $subcategory_id = Input::get('subcategory_id');
            $subpname = Input::get('name');

            $subproperty = new SubProperty();

            $subproperty->subcategory_id = $subcategory_id;
            $subproperty->name = $subpname;

            $subproperty->save();

            flash()->success('Alt Kategoriye Yeni Özellik Eklendi.');
            return Redirect::to('admin/category');
        }

        flash()->error('Hata oluştu.');
        return Redirect::to('admin/category')
                ->withErrors($validator)
                ->withInput();

     }


    public function getSubcategorypropdelete(Request $request)
    {
        $prop = SubProperty::find($request->id);

        if($prop){
            $prop->delete();
            return '<div class="alert alert-success">Silme işlemi başarılı.</div>';
        }

        return '<div class="alert alert-danger">Hata oluştu.</div>';


    }

    public function getSubcategorypropedit(Request $request)
    {

        if($request->name) {
            $prop = SubProperty::find($request->id);

            if ($prop) {
                $prop->name = $request->name;
                $prop->save();
                return '<div class="alert alert-success">Güncelleme işlemi başarılı.</div>';
            }
        }



        return '<div class="alert alert-danger">Hata oluştu.Tüm alanları doldurunuz.</div>'.$request->name;
    }


    public function getUnverifyyetkiliresim(Request $request)
    {
        $imalatci = User::find($request->imalatci_id);

        $imalatci->yetkili_resim_verified= 0;

        $imalatci->save();

        return 'Yetkili resim onayı kaldırıldı.';

    }

    public function getVerifyyetkiliresim(Request $request)
    {
        $imalatci = User::find($request->imalatci_id);

        $imalatci->yetkili_resim_verified= 1;

        $imalatci->save();

        return 'Yetkili resmi onaylandı.';
    }

    public function getVerifysertifika(Request $request){

        $id = $request->id;

        $image = CertificateImage::find($id);

        $image->verified = 1;

        $image->save();

        return 'Sertifika Onaylandı.';


    }

    public function getUnverifysertifika(Request $request){

        $id = $request->id;

        $image = CertificateImage::find($id);

        $image->verified = 0;

        $image->save();

        return 'Sertifika Onayı Kaldırıldı.';


    }

    public function getVerifyresim(Request $request){

        $id = $request->id;

        $image = CompanyImage::find($id);

        $image->verified = 1;

        $image->save();

        return 'Resim Onaylandı.';


    }

    public function getUnverifyresim(Request $request){

        $id = $request->id;

        $image = CompanyImage::find($id);

        $image->verified = 0;

        $image->save();

        return 'Resim Onayı Kaldırıldı.';


    }

    public function getConfirmuser(Request $request)
    {
        $imalatci = User::find($request->imalatci_id);

        $imalatci->confirmed= 1;

        $imalatci->save();


        return 'Kullanıcı Onaylandı.';
    }

    public function getUnconfirmuser(Request $request)
    {
        $imalatci = User::find($request->imalatci_id);

        $imalatci->confirmed= 0;

        $imalatci->save();

        //Onayı kaldırılan kullanıcının ürünlerinin de onayını kaldır.

        foreach(Product::where('imalatci_id',$request->imalatci_id)->get() as $product){
            $product->verified = 0;
            $product->save();
        }

        return 'Kullanıcı Onayı Kaldırıldı.';
    }

    //Hizmet firmaları kategorileri

    public function getServicecategory(){

        $servicecategories = ServiceCategory::all();

        return view('pages.admin.servicecategory')
            ->with('categories',$servicecategories);
    }

    public function postServicecategorycreate()
    {
        $validator = Validator::make(Input::all(),ServiceCategory::$rules,ServiceCategory::$messages);

        if(!$validator->fails()){

            $category = new ServiceCategory();
            $category->name=Input::get('name');

            $category->save();

            flash()->success('Kategori oluşturuldu.');

            return Redirect::to('admin/servicecategory');

        }

        flash()->important('Hata Oluştu');

        return Redirect::to('admin/servicecategory')
            ->withErrors($validator)
            ->withInput();
    }

    public function postServicecategoryupdate()
    {
        $validator = Validator::make(Input::all(),ServiceCategory::$rules);

        $category = ServiceCategory::find(Input::get('id'));

        $category->name=Input::get('name');

        if($validator->fails()){
            flash()->important('Hata Oluştu');
            return Redirect::to('admin/servicecategory')
                ->withErrors($validator)
                ->withInput();
        }else{

            $category->save();

            flash()->success('Kategori güncelleme işlemi başarılı.');

            return Redirect::to('admin/servicecategory');

        }

    }

    public function postServicecategorydestroy()
    {
        $category = ServiceCategory::find(Input::get('id'));

        if($category){
            $category->delete();
            flash()->success('Kategori Silindi');
            return Redirect::to('admin/servicecategory');
        }

        flash()->important('Hata Oluştu');
        return Redirect::to('admin/servicecategory');
    }

    public function postServicesubcategorycreate()
    {
        $validator = Validator::make(Input::all(),ServiceSubCategory::$rules,ServiceSubCategory::$messages);

        if(!$validator->fails()){

            $subcategory = new ServiceSubCategory();
            $subcategory->name=Input::get('name');
            $subcategory->service_category_id=Input::get('cat_id');
            $subcategory->save();

            flash()->success('Alt Kategori oluşturuldu.');

            return Redirect::to('admin/servicecategory');

        }

        flash()->important('Hata Oluştu');

        return Redirect::to('admin/servicecategory')
            ->withErrors($validator)
            ->withInput();

    }

    public function postServicesubcategorydelete()
    {
        $subcategory = ServiceSubCategory::find(Input::get('id'));

        if($subcategory){
            $subcategory->delete();
            flash()->success('Alt Kategori Silindi');
            return Redirect::to('admin/servicecategory');
        }

        flash()->important('Hata Oluştu');
        return Redirect::to('admin/servicecategory');
    }

    public function postServicesubcategoryupdate()
    {
        $validator = Validator::make(Input::all(),ServiceSubCategory::$rules,ServiceSubCategory::$messages);

        $subcategory = ServiceSubCategory::find(Input::get('id'));

        $subcategory->name=Input::get('name');

        if($validator->fails()){
            flash()->important('Hata Oluştu');
            return Redirect::to('admin/servicecategory')
                ->withErrors($validator)
                ->withInput();
        }else{

            $subcategory->save();

            flash()->success('Alt Kategori güncelleme işlemi başarılı.');

            return Redirect::to('admin/servicecategory');

        }
    }


}
