<?php

namespace App\Http\Controllers;

use App\City;
use App\CityTown;
use App\Imalatci;
use App\Product;
use App\ProductRating;
use App\ServiceCategory;
use App\SubProperty;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class PagesController extends Controller
{
    protected $productReviewRules = array(
        'name' => 'required|min:3',
        'email' => 'required|email',
        'review' => 'required|min:10',
        'rating' => 'required'
    );

    protected  $productReviewMessages = array(
        'required' => ':attribute alanı gereklidir.',
        'email' => ':atrribute alanına geçerli bir eposta adresi giriniz.',
        'min' => ':attribute alanına en az :min karakter girmelisiniz.'
    );

    public function index()
    {
        $product1 = DB::table('products')->where(function($query){
                $query->where('verified', '=', 1);
            })->where(function($query){
                 $query->where('published', '=', 1);
            })->take(6)
              ->orderBy('id','desc')
              ->get();

        //dd($product1);

        return view('pages.index')->with('categories',Category::all())
                                  ->with('product1',$product1);
    }


    public function mainslider()
    {
        return view('pages.admin.mainslider');
    }

    public function imalatci(){

        if(Auth::check()) {
            return view('pages.imalatci.index');
        }

        return "Bu alana giriş yetkiniz yok!";
    }

    public function login(){
        return view('pages.login.index');
    }

    public function register(){
        $city = City::all();
        return view('pages.register.index')->with('cities',$city);
    }

    public function postProductreview()
    {
        $validator = Validator::make(Input::all(),$this->productReviewRules,$this->productReviewMessages);


        $pId = intval(Input::get('product_id'));

        $email = Input::get('email');

        if(!$validator->fails()){


            $pRatingCount = ProductRating::where(['product_id'=>$pId,'email'=>$email])->count();

            if($pRatingCount == 0){

                $pRating = new ProductRating();

                $pRating->product_id = Input::get('product_id');
                $pRating->name = Input::get('name');
                $pRating->email = Input::get('email');
                $pRating->review = Input::get('review');
                $pRating->rating = Input::get('rating');

                $pRating->save();

                flash()->success('Ürün oylama işlemi başarılı.Oylamaya yaptığınız için teşekkür ederiz.');

                return Redirect::to('/productfull/'.$pId);


            }else{
                flash()->error('Ürünü daha önce oylamışsınız.');

                return Redirect::to('/productfull/'.$pId);
            }

        }

        flash()->error('Ürün oylama işlemi başarısız.');


        return Redirect::to('/productfull/'.$pId)
            ->withErrors($validator)
            ->withInput();

    }

    public function getCitytown(Request $request)
    {

        $city_code = $request->city_code;

        $city_town = CityTown::where('city_code',$city_code)->get();


        return json_encode($city_town);

    }

    public function getSubprops(Request $request)
    {
        if($request->id) {
            $subprop = SubProperty::where('subcategory_id', $request->id)->get();

            $html = "<select name='subproperty' id='subproperty' class='form-control'>";

            foreach ($subprop as $prop) {
                $html .= "<option value='{$prop->id}'>{$prop->name}</option>";
            }

            $html .= "</select>";

            return $html;
        }


    }

    public function getServicesubcategory(Request $request){

        $html = '<select class="form-control" name="service_subcategory[]" id="service_subcategory" multiple>';

        $service_category = ServiceCategory::find($request->service_category_id);

        foreach($service_category->serviceSubcategories as $subcategory){
            $html.="<option value='{$subcategory->id}'>{$subcategory->name}</option>";
        }

        $html."</select>";

        $html.="<script>
                    $('select#service_category,select#service_subcategory').select2({width:'100%'});
              </script>";

        return $html;
    }

}
