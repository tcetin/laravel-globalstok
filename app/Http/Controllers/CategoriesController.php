<?php

namespace App\Http\Controllers;

use App\Category;
use App\SubCategory;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        $categories = Category::all();

        return view('pages.admin.category')
                ->with('categories',$categories);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postCreate()
    {
        $validator = Validator::make(Input::all(),Category::$rules,Category::$messages);

        if(!$validator->fails()){

            $category = new Category();
            $category->name=Input::get('name');

            $category->save();

            flash()->success('Kategori oluşturuldu.');

            return Redirect::to('admin/category');

        }

        flash()->important('Hata Oluştu');

        return Redirect::to('admin/category')
                        ->withErrors($validator)
                        ->withInput();
    }

    public function postSubcreate()
    {
        $validator = Validator::make(Input::all(),SubCategory::$rules,SubCategory::$messages);

        if(!$validator->fails()){

            $subcategory = new SUbCategory();
            $subcategory->name=Input::get('name');
            $subcategory->category_id=Input::get('cat_id');
            $subcategory->save();

            flash()->success('Alt Kategori oluşturuldu.');

            return Redirect::to('admin/category');

        }

        flash()->important('Hata Oluştu');

        return Redirect::to('admin/category')
            ->withErrors($validator)
            ->withInput();

    }

    public function postSubdelete()
    {
        $subcategory = SubCategory::find(Input::get('id'));

        if($subcategory){
            $subcategory->delete();
            flash()->success('Alt Kategori Silindi');
            return Redirect::to('admin/category');
        }

        flash()->important('Hata Oluştu');
        return Redirect::to('admin/category');
    }

    public function postSubupdate()
    {
        $validator = Validator::make(Input::all(),SubCategory::$rules,SubCategory::$messages);

        $subcategory = SubCategory::find(Input::get('id'));

        $subcategory->name=Input::get('name');

        if($validator->fails()){
            flash()->important('Hata Oluştu');
            return Redirect::to('admin/category')
                ->withErrors($validator)
                ->withInput();
        }else{

            $subcategory->save();

            flash()->success('Alt Kategori güncelleme işlemi başarılı.');

            return Redirect::to('admin/category');

        }
    }



    /**
     * @return mixed
     */



    public function postUpdate()
    {
        $validator = Validator::make(Input::all(),Category::$rules);

        $category = Category::find(Input::get('id'));

        $category->name=Input::get('name');

        if($validator->fails()){
            flash()->important('Hata Oluştu');
            return Redirect::to('admin/category')
                ->withErrors($validator)
                ->withInput();
        }else{

            $category->save();

            flash()->success('Kategori güncelleme işlemi başarılı.');

            return Redirect::to('admin/category');

        }

    }





    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postDestroy()
    {
        $category = Category::find(Input::get('id'));

        if($category){
            $category->delete();
            flash()->success('Kategori Silindi');
            return Redirect::to('admin/category');
        }

        flash()->important('Hata Oluştu');
        return Redirect::to('admin/category');
    }
}
