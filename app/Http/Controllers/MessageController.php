<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class MessageController extends Controller
{

    public function __construct() {
        $this->middleware('messages');
    }

    public function getIndex(Request $request)
    {

        return view('pages.emails.contactsupplier')
                ->with('product_id',$request->product_id);
    }

    public function postSendmessage(Request $request)
    {

        if($request->mesaj && $request->kartvizit){

            $product = Product::find(Input::get('product_id'));

            $data =[
                'product_id' => $product->id,
                'odenecek'=>Input::get('odenecek'),
                'mesaj' => $request->mesaj,
                'perakendeci_id'=>$request->perakendeci_id,
                'urun_miktar'=>$request->quant[2]
            ];

            //imalatçıya mail gönder

            Mail::send('pages.emails.email', $data, function ($message) {

                $product = Product::find(Input::get('product_id'));

                $to = Input::get('imalatci_email');

                $message->to($to)->subject(Input::get('subject'));
            });

            //Gönderimle ilgili perakendeciye bilgi maili gönder

            /*
            $data = array(
                'product_id'=>$product->id,
                'odenecek'=>Input::get('odenecek'),
                'mesaj'=>$request->msg,
                'perakendeci_id'=>$request->perakendeci_id

            );

            Mail::send('pages.emails.bilgilendirme', $data, function ($message) {

                $product = Product::find(Input::get('product_id'));

                $to = Input::get('email');

                $message->to($to)->subject($product->title);
            });
*/

            flash()->success('Mesajınız başarıyla iletildi.');

            return Redirect::to('/messages');



        }


    }

    public function postFiyatteklifi(Request $request){

        if($request->mesaj && $request->kartvizit && $request->miktar){
            $product = Product::find(Input::get('product_id'));

            $data =[
                'product_id' => $product->id,
                'miktar'=>$request->miktar,
                'mesaj' => $request->mesaj,
                'perakendeci_id'=>$request->perakendeci_id
            ];

            //imalatçıya mail gönder

            Mail::send('pages.emails.fiyat_teklif', $data, function ($message) {

                $product = Product::find(Input::get('product_id'));

                $to = Input::get('imalatci_email');

                $message->to($to)->subject(Input::get('subject'));

            });


            flash()->success('Fiyat teklifi satıcıya başarıyla iletildi.');
            return Redirect::to('/productfull/'.$request->product_id);


        }

        flash()->error('Hata oluştu.Lütfen tüm alanları eksiksiz doldurunuz.');
        return Redirect::to('/productfull/'.$request->product_id);



   }

    public function postBayiolmsg(Request $request){

        if(!empty($request->msg) && $request->kartvizit){

            $data =[
                'mesaj' => $request->mesaj,
                'perakendeci_id'=>$request->perakendeci_id
            ];

            //imalatçıya mail gönder

            Mail::send('pages.emails.bayiol', $data, function ($message) {

                $to = Input::get('imalatci_email');

                $message->to($to)->subject(Input::get('subject'));

            });


            flash()->success('Mesaj başarıyla iletildi.');
            return Redirect::to('/firma/'.$request->imalatci_id);


        }

        flash()->error('Hata oluştu.Lütfen tüm alanları eksiksiz doldurunuz.');
        return Redirect::to('/firma/'.$request->imalatci_id);
    }

    public function postSikayet(Request $request){

        if(!empty($request->msg)){

            $data =[
                'mesaj' => $request->mesaj,
                'perakendeci_id'=>$request->perakendeci_id,
                'imalatci_id'=>$request->imalatci_id
            ];

            //imalatçıya mail gönder

            Mail::send('pages.emails.sikayet', $data, function ($message) {

                $message->to('globalstokdestek@gmail.com')->subject(Input::get('subject'));

            });


            flash()->success('Mesaj başarıyla iletildi.');
            return Redirect::to('/firma/'.$request->imalatci_id);


        }

        flash()->error('Hata oluştu.Lütfen tüm alanları eksiksiz doldurunuz.');
        return Redirect::to('/firma/'.$request->imalatci_id);
    }
}
