<?php

namespace App\Http\Controllers;

use App\Category;
use App\PaymentMethod;
use App\Product;
use App\ProductImage;
use App\ProductPaymentMethod;
use App\SubCategory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Validator;



class ProductsController extends Controller
{
    protected  $rules = array(
        'subcategory' => 'required|integer',
        'title' => 'required|max:50|min:5',
        'description' => 'required|',
        'price' =>'required',
        'amount_type' => 'required',
        'amount' => 'required|integer',
        'currency' => 'required',
        'min_order'=> 'required'
    );

    protected $messages = array(
        'required' => ':attribute alanı boş bırakılamaz.',
        'numeric'  => ':attribute alanına sadece sayı girilebilir.',
        'integer' => ':attribute alanı sadece sayı olabilir.',
        'max'     => ':attribute alanına en fazla :max karakter girebilirsiniz.',
        'min'     => ':attribute alanına an az :min karakter girmelisiniz'
    );

    public function getIndex()
    {
        $products = Product::all();

        return view('pages.imalatci.products')
            ->with('products',$products);

    }

    public function postAddproduct(Request $request)
    {

        $validator = Validator::make(Input::all(),$this->rules,$this->messages);

          if(!$validator->fails()){

                $product = new Product();
                $subcategory = SubCategory::find(Input::get('subcategory'));
                $product->category_id = $subcategory->category_id;
                $product->subcategory_id = Input::get('subcategory');
                $product->subprop_id = Input::get('subproperty');
                $product->imalatci_id = Input::get('imalatci_id');
                $product->title = Input::get('title');
                $product->description = Input::get('description');
                $product->price = Input::get('price');
                $product->currency_id = Input::get('currency');
                $product->amount_type = Input::get('amount_type');
                $product->amount = Input::get('amount');
                $product->min_order = Input::get('min_order');

                if($product->save()){
                        $product->payment_methods()->attach($request->payment_method);
                }





                flash()->success('Ürün başarıyla eklendi.Yönetici onayından sonra ürün yayınlanacaktır.');

                return Redirect::to('/imalatci/products');

            }

        flash()->error('Ürün Ekleme İşlemi Başarısız.Lütfen tüm alanları eksiksiz ve doğru bir şekilde doldurunuz.');

        return Redirect::to('/imalatci/products')
            ->withErrors($validator)
            ->withInput();
    }

    public function postUpdateproduct(Request $request)
    {
        $validator = Validator::make(Input::all(),$this->rules,$this->messages);

        if(!$validator->fails()) {
            $product = Product::find(Input::get('id'));

            $subcategory = SubCategory::find(Input::get('subcategory'));
            $product->category_id = $subcategory->category_id;
            $product->subcategory_id = Input::get('subcategory');
            $product->subprop_id = Input::get('subprop');
            $product->imalatci_id = Input::get('imalatci_id');
            $product->title = Input::get('title');
            $product->description = Input::get('description');
            $product->price = Input::get('price');
            $product->currency_id = Input::get('currency');
            $product->amount_type = Input::get('amount_type');
            $product->amount = Input::get('amount');
            $product->min_order = Input::get('min_order');
            $product->avaliability = Input::get('avaliability');
            $product->vitrin = 0;
            $product->verified = 0;

            if($product->save()){
                $product->payment_methods()->sync($request->payment_method);
            }


            flash()->success('Ürün başarıyla güncellendi.Yönetici onayından sonra ürün yayınlanacaktır.');

            return Redirect::to('/imalatci/products');
        }



        flash()->error('Ürün Güncelleme İşlemi Başarısız.Lütfen tüm alanları eksiksiz ve doğru bir şekilde doldurunuz.');

        return Redirect::to('/imalatci/products')
            ->withErrors($validator)
            ->withInput();
    }

    public function postUploadimages(Request $request )
    {
        $data = $request->all();

        $images = Input::file('images');

        $product_id = intval($data['product_id']);

        $matchThese = array(
            'product_id' => $product_id,
            'image_type' => 2
        );

        $count = ProductImage::where($matchThese)->count();

        $response = array();

        foreach ($images as  $image ) {

            $productImage = new ProductImage();

            $filename =$product_id.'_product'.'_'.date('Y_m_d_H_i_s').'_'.$image->getClientOriginalName();

            Image::make($image->getRealPath())->resize(500,400)->save('images/products/'.$filename);


            $productImage->product_id = $product_id;

            $productImage->path = 'images/products/'.$filename;

            $productImage->image_type = 2;

        }

        if($count < 5){
            if($productImage->save()){

                $response = array(
                    'res' => 1,
                    'verified' => $productImage->verified,
                    'result' => 'Resim başarıyla yüklendi',
                    'img_id' => $productImage->id,
                    'img_path' => $productImage->path
                );
            }
        }else{

            $response = array(
                'res' => 0,
                'result' => 'Resim ekleme işlemi başarısız.En fazla 5 adet resim yükleyebilirsiniz'
            );
        }

        return response()->json($response);


    }


    public function postUpdatemainimage(Request $request){

        $productImage = new ProductImage();

        $product_id = intval($request->product_id);

        $matchThese = array(
            'product_id' => $product_id,
            'image_type' => 1
        );

        $count = ProductImage::where($matchThese)->count();

        $image = Input::file('image');

        $img_response = array();

        if($count == 0) {

            if(!File::exists($productImage->path)) {

                    $filename = 'main_'.$product_id. '_' . date('Y_m_d_H_i_s') . '_' . $image->getClientOriginalName();
                    Image::make($image->getRealPath())->resize(250, 250)->save('images/products/' . $filename);


                    $productImage->path = 'images/products/' . $filename;
                    $productImage->image_type = 1;
                    $productImage->product_id =$product_id;

                    $productImage->save();


                    $img_response = array(
                        'id' => $productImage->id,
                        'verified' => $productImage->verified,
                        'res' => 1,
                        'result' => 'Resim yükleme işlemi başarılı.',
                        'image_path' => $productImage->path
                    );


            }else{
                $img_response = array(
                    'res' => 0,
                    'result' => 'İşlem başarısız!Resim daha önce yüklenmiş.',
                    'image_path' => ''
                );
            }

        }else{
            $img_response = array(
                'res' => 0,
                'result' => 'En fazla 1 adet resim yükleyebilirsiniz!',
                'image_path' => ''
            );

        }

        return response()->json($img_response);


    }

    public function getDeletemainimage(Request $request){

        $id = intval($request->id);

        $image = ProductImage::find($id);

        $result = array();

        if(File::exists($image->path)) {

            if (File::delete($image->path)) {

                $image->destroy($id);

                $result = array(
                    'result' => 'Silme işlemi başarılı'
                );

            } else {
                $result = array(
                    'result' => 'Hata oluştu!'
                );
            }
        }else{
            $result = array(
                'result' => 'Dosya bulunamadı.'
            );
        }

        return $result;

    }

    public function getDeleteimage(Request $request){

        $id = intval($request->id);

        $image = ProductImage::find($id);

        $result = array();

        if(File::exists($image->path)) {

            if (File::delete($image->path)) {


                $image->destroy($id);

                $result = array(
                    'result' => 'Silme işlemi başarılı'
                );

            } else {
                $result = array(
                    'result' => 'Hata oluştu!'
                );
            }
        }else{
            $result = array(
                'result' => 'Dosya bulunamadı.'
            );
        }

        return response()->json($result);

    }



}
