<?php

namespace App\Http\Controllers;

use App\CertificateImage;
use App\City;
use App\CompanyImage;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class ImalatciController extends Controller
{
    protected $rules = array(
        'unvan' => 'required',
        'adres' => 'required',
        'telefon' => 'required|min:9|numeric',
        'fax'=> 'required|min:9|numeric',
        'email' => 'required|email',
        'web' => 'required',
        'password' => 'required|alpha_num|between:8,12|confirmed',
        'password_confirmation' => 'required|alpha_num|between:8,12'
    );

    protected  $messages = array(
        'required' => ':attribute alanı boş geçilemez!',
        'min' => ':attribute alanı en az :min olmalıdır!',
        'numeric'=> ':attribute alanına sadece sayı girilebilir!',
        'email' => ':attribute alanına uygun bir email adresi yazınız!',
        'alpha_num' => ':attribute alanına sadece alfanümerik karakterler girilebilir.',
        'confirmed' => 'Girilen şifreler birbiriyle uyuşmamaktadır.',
        'unique' => ':attribute daha önce kayıt olmuş.',
        'min' => ':attribute alanı en az :min karakter olmalıdır.'
    );

    public function __construct() {
        $this->middleware('imalatci');
    }

    public function getIndex()
    {
        return view('pages.imalatci.index');
    }

    public function getProfile(){

        $city_code = Auth::user()->city_code;

        $city = City::where('city_code',$city_code)->get();
        $cities = City::all();

        return view('pages.imalatci.profile')
                ->with('city',$city)
                ->with('cities',$cities);

    }

    public function postUpdateprofile()
    {
        $validator = Validator::make(Input::all(),$this->rules,$this->messages);

        if(!$validator->fails()){

            $userId = Auth::user()->id;
            $user = User::find($userId);

            $user->unvan = Input::get('unvan');
            $user->city_code = Input::get('city');
            $user->city_town = Input::get('city_town');
            $user->adres = Input::get('adres');
            $user->telefon = Input::get('telefon');
            $user->fax = Input::get('fax');
            $user->email = Input::get('email');
            $user->web = Input::get('web');
            $user->employees_number = Input::get('employees_number');
            $user->working_hours = Input::get('working_hours');
            $user->password = Hash::make(Input::get('password'));
            $user->imalatci_profil = Input::get('imalatci_profil');

            $user->save();

            flash()->success('Profil başarıyla güncellendi.');

            return Redirect::to('/imalatci/profile');
         }

        flash()->error('Hata oluştu.Lütfen Tüm Alanları Eksiksiz ve Doğru Olarak Doldurunuz.');

        return Redirect::to('/imalatci/profile')
            ->withErrors($validator)
            ->withInput();



    }

    public function postUploadlogo(Request $request){

            $imalatci = User::find($request->imalatci_id);

            $image = Input::file('logo');

            if(File::exists($imalatci->logo)) {
                File::delete($imalatci->logo);
            }


            $filename =$imalatci->id.'_logo'.'_'.date('Y_m_d_H_i_s').'_'.$image->getClientOriginalName();

            Image::make($image->getRealPath())->resize(64,64)->save('images/'.$filename);

            $imalatci->logo = 'images/'.$filename;



            $imalatci->save();

            $img_response = array(
                'verified' => $imalatci->logo_verified,
                'result' => 'Logo yükleme işlemi başarılı.Yönetici onayından sonra yayınlanacaktır.',
                'img_path' => $imalatci->logo
            );

            return response()->json($img_response);

    }

    public function postUploadyetkiliresim(Request $request)
    {

        $imalatci = User::find($request->imalatci_id);

        $image = Input::file('yetkiliresim');

        if(File::exists($imalatci->yetkili_resim)) {
            File::delete($imalatci->yetkili_resim);
        }


        $filename =$imalatci->id.'_yetkili_resim'.'_'.date('Y_m_d_H_i_s').'_'.$image->getClientOriginalName();

        Image::make($image->getRealPath())->resize(64,64)->save('images/'.$filename);

        $imalatci->yetkili_resim = 'images/'.$filename;



        $imalatci->save();

        $img_response = array(
            'verified' => $imalatci->yetkili_resim_verified,
            'result' => 'Yetkili resmi yükleme işlemi başarılı.Yönetici onayından sonra yayınlanacaktır.',
            'img_path' => $imalatci->yetkili_resim
        );

        return response()->json($img_response);

    }

    public function postUploadsertifika(Request $request)
    {

        $data = $request->all();

        $images = Input::file('sertifika');

        $imalatci_id = intval($data['imalatci_id']);

        $matchThese = array(
            'imalatci_id' => $imalatci_id
        );

        $count = CertificateImage::where($matchThese)->count();

        $response = array();

        foreach ($images as  $image ) {

            $certificateImage = new CertificateImage();

            $filename =$imalatci_id.'_sertifika'.'_'.date('Y_m_d_H_i_s').'_'.$image->getClientOriginalName();

            Image::make($image->getRealPath())->resize(600,400)->save('images/certificates/'.$filename);


            $certificateImage->imalatci_id = $imalatci_id;

            $certificateImage->path = 'images/certificates/'.$filename;


        }

        if($count < 5){
            if($certificateImage->save()){

                $response = array(
                    'res' => 1,
                    'verified' => $certificateImage->verified,
                    'result' => 'Sertifika başarıyla yüklendi',
                    'img_id' => $certificateImage->id,
                    'img_path' => $certificateImage->path
                );
            }
        }else{

            $response = array(
                'res' => 0,
                'result' => 'Sertifika ekleme işlemi başarısız.En fazla 5 adet resim yükleyebilirsiniz'
            );
        }

        return response()->json($response);


    }

    public function getDeletecertificate(Request $request){

        $id = intval($request->id);

        $image = CertificateImage::find($id);

        $result = array();

        if(File::exists($image->path)) {

            if (File::delete($image->path)) {

                $image->destroy($id);

                $result = array(
                    'result' => 'Silme işlemi başarılı'
                );

            } else {
                $result = array(
                    'result' => 'Hata oluştu!'
                );
            }
        }else{
            $result = array(
                'result' => 'Dosya bulunamadı.'
            );
        }

        return $result;

    }


    public function postUploadresim(Request $request)
    {

        $data = $request->all();

        $images = Input::file('resim');

        $imalatci_id = intval($data['imalatci_id']);

        $matchThese = array(
            'imalatci_id' => $imalatci_id
        );

        $count = CompanyImage::where($matchThese)->count();

        $response = array();

        foreach ($images as  $image ) {

            $companyImage = new CompanyImage();

            $filename =$imalatci_id.'_resim'.'_'.date('Y_m_d_H_i_s').'_'.$image->getClientOriginalName();

            Image::make($image->getRealPath())->resize(600,400)->save('images/company_images/'.$filename);


            $companyImage->imalatci_id = $imalatci_id;

            $companyImage->path = 'images/company_images/'.$filename;


        }

        if($count < 5){
            if($companyImage->save()){

                $response = array(
                    'res' => 1,
                    'verified' => $companyImage->verified,
                    'result' => 'Resim başarıyla yüklendi',
                    'img_id' => $companyImage->id,
                    'img_path' => $companyImage->path
                );
            }
        }else{

            $response = array(
                'res' => 0,
                'result' => 'Resim ekleme işlemi başarısız.En fazla 5 adet resim yükleyebilirsiniz'
            );
        }

        return response()->json($response);


    }

    public function getDeleteresim(Request $request){

        $id = intval($request->id);

        $image = CompanyImage::find($id);

        $result = array();

        if(File::exists($image->path)) {

            if (File::delete($image->path)) {

                $image->destroy($id);

                $result = array(
                    'result' => 'Silme işlemi başarılı'
                );

            } else {
                $result = array(
                    'result' => 'Hata oluştu!'
                );
            }
        }else{
            $result = array(
                'result' => 'Dosya bulunamadı.'
            );
        }

        return $result;

    }

}
