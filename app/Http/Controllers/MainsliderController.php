<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Mainslider;
use App\MainsliderStatu;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;



class MainsliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        $mainslider = MainSlider::all();

        $mainslider_statu = MainsliderStatu::all();


        return view('pages.admin.mainslider')
            ->with('mainslider',$mainslider)
            ->with('mainslider_statu',$mainslider_statu);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postCreate()
    {
        $validator = Validator::make(Input::all(),Mainslider::$rules,Mainslider::$messages);

        if(!$validator->fails()){

            $slide = new Mainslider();
            $slide->baslik = Input::get('baslik');
            $slide->metin = Input::get('metin');
            $image = Input::file('image');
            $filename = date('Y_m_d_H_i_s')."_".$image->getClientOriginalName();
            Image::make($image->getRealPath())->resize(256,256)->save('images/mainslider/'.$filename);
            $slide->photo_path = 'images/mainslider/'.$filename;

            $slide->button_text = Input::get('read_more');

            $slide->state = 1;

            $slide->save();

            flash()->success('Slide Oluşturuldu.');

            return Redirect::to('admin/mainslider');

        }

        flash()->important('Hata Oluştu');

        return Redirect::to('admin/mainslider')
            ->withErrors($validator)
            ->withInput();
    }

    public function postSliderstatu()
    {
        $validator = Validator::make(Input::all(),MainsliderStatu::$rules,MainsliderStatu::$messages);

        $slider = new MainsliderStatu();

        if(!$validator->fails()){

            if(empty($slider->count())){



                $slider->statu = Input::get('statu');

                $slider->save();

                flash()->success('Slider Durumu Değiştirildi.');

                return Redirect::to('admin/mainslider');

            }else{

                $maxId = DB::table('mainslider_statu')->max('id');

                $slide = MainsliderStatu::find($maxId);

                $slide->statu = Input::get('statu');

                $slide->save();

                flash()->success('Slider Durumu Değiştirildi.');

                return Redirect::to('admin/mainslider');


            }
        }

        flash()->important('Hata Oluştu');

        return Redirect::to('admin/mainslider')
            ->withErrors($validator)
            ->withInput();
    }

    public function postUpdate()
    {
        $validator = Validator::make(Input::all(),Mainslider::$rules,Mainslider::$messages);


        if($validator->fails()){
            flash()->important('Hata Oluştu');
            return Redirect::to('admin/mainslider')
                ->withErrors($validator)
                ->withInput();
        }else{


            $slide = Mainslider::find(Input::get('id'));;
            $slide->baslik = Input::get('baslik');
            $slide->metin = Input::get('metin');
            $image = Input::file('image');


            $filename = date('Y-m-d-H:i:s')."_".$image->getClientOriginalName();
            Image::make($image->getRealPath())->resize(256,256)->save('images/mainslider/'.$filename);
            $slide->photo_path = 'images/mainslider/'.$filename;

            $slide->button_text = Input::get('read_more');

            $slide->state =  Input::get('state');

            $slide->save();

            flash()->success('Slide Güncelleme İşlemi Başarılı.');

            return Redirect::to('admin/mainslider');


        }
    }

    public function postDestroy()
    {
        $slider = Mainslider::find(Input::get('id'));

        if($slider){
            $slider->delete();
            flash()->success('Slider Silindi');
            return Redirect::to('admin/mainslider');
        }

        flash()->important('Hata Oluştu');
        return Redirect::to('admin/mainslider');
    }

}
