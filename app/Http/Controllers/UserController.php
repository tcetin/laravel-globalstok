<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Validator;


class UserController extends Controller
{
    public function getIndex()
    {
        return view('pages.user.index');
    }


    public function getImalatcigiris(){
        return view('pages.user.imalatcigiris');
    }

    public function getPerakendecigiris(){
        return view('pages.user.perakendecigiris');
    }

    public function postLogin()
    {

        //yönetici girişi

        if(Auth::attempt(array('email'=>Input::get('email'),'password'=>Input::get('password'),'active'=>1,'rol'=>1,'verified'=>1))){

            flash()->success('Giriş Başarılı.Yönetici Sayfasına Hoşgeldiniz.');

            return Redirect::to('/admin');

        }

        flash()->important('Giriş işlemi başarısız.');
        return Redirect::to('/user');

    }

    public function postCreateimalatci()
    {
        $validator = Validator::make(Input::all(),User::$rulesImalatci,User::$messages);

        if(!$validator->fails()){

            $imalatci = new User();
            $imalatci->vergino = Input::get('vergino');
            $imalatci->unvan = Input::get('unvan');
            $imalatci->city_code = Input::get('city');
            $imalatci->city_town = Input::get('city_town');
            $imalatci->adres = Input::get('adres');
            $imalatci->telefon = Input::get('telefon');
            $imalatci->fax = Input::get('fax');
            $imalatci->email = Input::get('email');
            $imalatci->web = Input::get('web');
            $imalatci->yetkili = Input::get('yetkili');
            $imalatci->yetkili_cep = Input::get('yetkili_cep');
            $imalatci->established_year = Input::get('established_year');
            $imalatci->employees_number = Input::get('employees_number');
            $imalatci->imalatci_profil = Input::get('imalatci_profil');
            $imalatci->password = Hash::make(Input::get('password'));
            $imalatci->rol = 2;
            $imalatci->save();

            flash()->success('İmalatçı kullanıcısı başarıyla oluşturuldu.Sistemi kullanabilmek için ödeme yapmanız gerekmektedir.Durumunuz onaylandıktan sonra sisteme giriş yapabilirsiniz.');

            return Redirect::to('/register');

        }

        flash()->important('Hata Oluştu');

        return Redirect::to('/register')
            ->withErrors($validator)
            ->withInput();
    }

    public function postLoginimalatci()
    {
        //İmalatçı Girişi

        if(Auth::attempt(array('email'=>Input::get('email'),'password'=>Input::get('password'),'verified'=>1,'rol'=>2,'active'=>1))){

            flash()->success('Giriş Başarılı.İmalatçı Sayfasına Hoşgeldiniz.');

            return Redirect::to('/imalatci');

        }

        flash()->important('Giriş işlemi başarısız.');
        return Redirect::to('/user/imalatcigiris');


    }

    public function postCreateperakendeci()
    {
        $validator = Validator::make(Input::all(),User::$rulesTedarikci,User::$messages);

        if(!$validator->fails()){

            $tedarikci = new User();

            $tedarikci->yetkili = Input::get('fullname');
            $tedarikci->email = Input::get('email');
            $tedarikci->password = Hash::make(Input::get('password'));
            $tedarikci->rol = 3;
            $tedarikci->city_code = Input::get('city');
            $tedarikci->city_town = Input::get('city_town');
            $tedarikci->adres = Input::get('adres');
            $tedarikci->telefon = Input::get('telefon');
            $tedarikci->verified = 1;
            $tedarikci->remember_token =Input::get('remember_token');
            $tedarikci->save();

            flash()->success('Perakendeci kullanıcısı başarıyla oluşturuldu.Sistemi kullanmaya başlayabilirsiniz.');

            return Redirect::to('/register');

        }

        flash()->important('Hata Oluştu');

        return Redirect::to('/register')
            ->withErrors($validator)
            ->withInput();
    }

    public function postLoginperakendeci()
    {

        //Tedarikci Girişi



        $remember = Input::get('rememberme');

        if(Auth::attempt(['email'=>Input::get('email'),'password'=>Input::get('password'),'rol'=>3,'active'=>1,'verified'=>1],$remember)){

            flash()->success('Giriş Başarılı.Sistemi kullanmaya başlayabilirsiniz.');

            return Redirect::to('/user/perakendecigiris');

        }

        flash()->error('Giriş işlemi başarısız.');
        return Redirect::to('/user/perakendecigiris');


    }

    public function postCreatehizmetfirmasi(Request $request)
    {
        $validator = Validator::make(Input::all(),User::$rulesService,User::$messages);

        if(!$validator->fails()){

            $service = new User();
            $service->vergino = Input::get('vergino');
            $service->unvan = Input::get('unvan');
            $service->city_code = Input::get('city');
            $service->city_town = Input::get('city_town');
            $service->adres = Input::get('adres');
            $service->telefon = Input::get('telefon');
            $service->fax = Input::get('fax');
            $service->email = Input::get('email');
            $service->web = Input::get('web');
            $service->yetkili = Input::get('yetkili');
            $service->yetkili_cep = Input::get('yetkili_cep');
            $service->established_year = Input::get('established_year');
            $service->employees_number = Input::get('employees_number');
            $service->imalatci_profil = Input::get('imalatci_profil');
            $service->password = Hash::make(Input::get('password'));
            $service->rol = 4;

            if($service->save()){
                $service->service_categories()->attach($request->service_category);
                $service->service_subcategories()->attach($request->service_subcategory);
            }

            flash()->success('Hizmet Firması kullanıcısı başarıyla oluşturuldu.Sistemi kullanabilmek için ödeme yapmanız gerekmektedir.Durumunuz onaylandıktan sonra sisteme giriş yapabilirsiniz.');

            return Redirect::to('/register');

        }

        flash()->important('Hata Oluştu');

        return Redirect::to('/register')
            ->withErrors($validator)
            ->withInput();
    }

    public function postLoginhizmetfirmasi()
    {
        //İmalatçı Girişi

        if(Auth::attempt(array('email'=>Input::get('email'),'password'=>Input::get('password'),'verified'=>1,'rol'=>2,'active'=>1))){

            flash()->success('Giriş Başarılı.İmalatçı Sayfasına Hoşgeldiniz.');

            return Redirect::to('/imalatci');

        }

        flash()->important('Giriş işlemi başarısız.');
        return Redirect::to('/user/imalatcigiris');


    }

    public function getLogout()
    {

        Auth::logout();

        return Redirect::to('/');
    }
}
