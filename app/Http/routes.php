<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//İndex sayfa
Route::get('/','PagesController@index');

//ürün ile ilgili görüşleriniz
Route::controller('/fpage','PagesController');

//ürün full sayfa
Route::get('productfull/{id}', function ($id) {

    $product = \App\Product::find($id);

    $category = \App\Category::find($product->category_id);

    $subcategory = \App\SubCategory::find($product->subcategory_id);

    $amounType = \App\AmountType::find($product->amount_type);

    $amount = $product->amount.' '.$amounType->name;

    $subprop = \App\SubProperty::find($product->subprop_id)->name;



    $related = App\Product::where(['subcategory_id'=>$product->subcategory_id,'verified'=>1,'published'=>1,'vitrin'=>1])->take(4)->get();

    $productInfo = array(
        'categoryName' => $category->name,
        'subcategoryName' => $subcategory->name,
        'amount' => $amount,
        'subprop' => $subprop
    );

    return view('pages.productfull')->with(['id'=>$id,'productInfo'=>$productInfo,'related'=>$related]);
});

//########################################product category list

Route::get('productlist/{category_id}/{ordertype}/{listtype}',function($category_id,$ordertype,$listtype){

    //listtype=1 list, listtype=2 grid

    if(isset($_GET['city'])){

        $city = Input::get('city');

        if($ordertype==1) {

            $product = DB::table('products')
                ->join('users', 'users.id', '=', 'products.imalatci_id')
                ->where(function ($query) {
                    $query->where('products.verified', '=', 1);
                })->where(function ($query) {
                    $query->where('products.published', '=', 1);
                })->where(function ($query) {
                    $query->whereIn('users.city_code',Input::get('city'));
                })->paginate(6);

        }elseif($ordertype==2){

            $product = DB::table('products')
                ->join('users', 'users.id', '=', 'products.imalatci_id')
                ->where(function ($query) {
                    $query->where('products.verified', '=', 1);
                })->where(function ($query) {
                    $query->where('products.published', '=', 1);
                })->where(function ($query) {
                    $query->whereIn('users.city_code',Input::get('city'));
                })->orderBy('products.price','asc')->paginate(6);

        }elseif($ordertype==3){

            $product = DB::table('products')
                ->join('users', 'users.id', '=', 'products.imalatci_id')
                ->where(function ($query) {
                    $query->where('products.verified', '=', 1);
                })->where(function ($query) {
                    $query->where('products.published', '=', 1);
                })->where(function ($query) {
                    $query->whereIn('users.city_code',Input::get('city'));
                })->orderBy('products.price','desc')->paginate(6);

        }

            return view('pages.productlist')
                ->with('category_id', $category_id)
                ->with('ordertype', $ordertype)
                ->with('listtype', $listtype)
                ->with('filtered_product', $product);



    }else{
        return view('pages.productlist')
            ->with('category_id', $category_id)
            ->with('ordertype', $ordertype)
            ->with('listtype', $listtype);
        }



});

//########################################
// product subcategory list

Route::get('productsublist/{category_id}/{subcategory_id}/{ordertype}/{listtype}',function($category_id,$subcategory_id,$ordertype,$listtype){

    //listtype=1 list, listtype=2 grid
    if(isset($_GET['city'])){

        $city = Input::get('city');

        if($ordertype==1) {

            $product = DB::table('products')
                ->join('users', 'users.id', '=', 'products.imalatci_id')
                ->where(function ($query) {
                    $query->where('products.verified', '=', 1);
                })->where(function ($query) {
                    $query->where('products.published', '=', 1);
                })->where(function ($query) {
                    $query->whereIn('users.city_code',Input::get('city'));
                })->paginate(6);

        }elseif($ordertype==2){

            $product = DB::table('products')
                ->join('users', 'users.id', '=', 'products.imalatci_id')
                ->where(function ($query) {
                    $query->where('products.verified', '=', 1);
                })->where(function ($query) {
                    $query->where('products.published', '=', 1);
                })->where(function ($query) {
                    $query->whereIn('users.city_code',Input::get('city'));
                })->orderBy('products.price','asc')->paginate(6);

        }elseif($ordertype==3){

            $product = DB::table('products')
                ->join('users', 'users.id', '=', 'products.imalatci_id')
                ->where(function ($query) {
                    $query->where('products.verified', '=', 1);
                })->where(function ($query) {
                    $query->where('products.published', '=', 1);
                })->where(function ($query) {
                    $query->whereIn('users.city_code',Input::get('city'));
                })->orderBy('products.price','desc')->paginate(6);

        }

        return view('pages.productsublist')
            ->with('category_id', $category_id)
            ->with('subcategory_id', $subcategory_id)
            ->with('ordertype', $ordertype)
            ->with('listtype', $listtype)
            ->with('filtered_product', $product);



    }else{
        return view('pages.productsublist')
            ->with('category_id', $category_id)
            ->with('subcategory_id', $subcategory_id)
            ->with('ordertype', $ordertype)
            ->with('listtype', $listtype);
    }




});


//##################
//product subprop

Route::get('productsubprop/{category_id}/{subcategory_id}/{ordertype}/{listtype}/{prop_id}',function($category_id,$subcategory_id,$ordertype,$listtype,$prop_id){
    //listtype=1 list, listtype=2 grid
    if(isset($_GET['city'])){

        $city = Input::get('city');

        if($ordertype==1) {

            $product = DB::table('products')
                ->join('users', 'users.id', '=', 'products.imalatci_id')
                ->where(function ($query) {
                    $query->where('products.verified', '=', 1);
                })->where(function ($query) {
                    $query->where('products.published', '=', 1);
                })->where(function ($query) {
                    $query->whereIn('users.city_code',Input::get('city'));
                })->paginate(6);

        }elseif($ordertype==2){

            $product = DB::table('products')
                ->join('users', 'users.id', '=', 'products.imalatci_id')
                ->where(function ($query) {
                    $query->where('products.verified', '=', 1);
                })->where(function ($query) {
                    $query->where('products.published', '=', 1);
                })->where(function ($query) {
                    $query->whereIn('users.city_code',Input::get('city'));
                })->orderBy('products.price','asc')->paginate(6);

        }elseif($ordertype==3){

            $product = DB::table('products')
                ->join('users', 'users.id', '=', 'products.imalatci_id')
                ->where(function ($query) {
                    $query->where('products.verified', '=', 1);
                })->where(function ($query) {
                    $query->where('products.published', '=', 1);
                })->where(function ($query) {
                    $query->whereIn('users.city_code',Input::get('city'));
                })->orderBy('products.price','desc')->paginate(6);

        }

        return view('pages.productsubprop')
            ->with('category_id', $category_id)
            ->with('subcategory_id', $subcategory_id)
            ->with('ordertype', $ordertype)
            ->with('listtype', $listtype)
            ->with('filtered_product', $product)
            ->with('prop_id',$prop_id);



    }else{
        return view('pages.productsubprop')
            ->with('category_id', $category_id)
            ->with('subcategory_id', $subcategory_id)
            ->with('ordertype', $ordertype)
            ->with('listtype', $listtype)
            ->with('prop_id',$prop_id);
    }


});


//üreticinin tüm ilanları

Route::get('product_manufacturer/{id}/{listtype}/{ordertype}',function($id,$listtype,$ordertype){
    return view('pages.product_manufacturer')
        ->with('imalatci_id',$id)
        ->with('listtype',$listtype)
        ->with('ordertype',$ordertype);
});

//Message Controller

Route::controller('messages','MessageController');

//Admin Controller
Route::controller('admin','AdminController');

Route::controller('/imalatci/products','ProductsController');

//Kullanıcı İşlemleri(Login Ekranları Dahil)

Route::controller('/user','UserController');


//imalatçı routerları


Route::controller('imalatci','ImalatciController');

//tedarikçi controller route

Route::controller('/tedarikci','TedarikciController');


//İmalatçı - Tedarikçi Yeni Kayıt

Route::get('/register','PagesController@register');

//firma

Route::get('/firma/{id}',function($id){

    $categories =DB::table('categories')
                        ->join('products', 'categories.id', '=', 'products.category_id')
                        ->where('products.imalatci_id','=',$id)
                        ->select('categories.*')
                        ->distinct()
                        ->get();

    return view('pages.firma')
            ->with('imalatci_id',$id)
            ->with('categories',$categories);

});
