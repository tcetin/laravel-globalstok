<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CertificateImage extends Model
{
    protected  $table = "certificate_images";

    protected $fillable = ['imalatci_id','path','verified'];
}
