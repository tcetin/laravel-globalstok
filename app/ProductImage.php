<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $table = 'product_images';

    protected $fillable = array('product_id','path','image_type');

    public function product(){
        $this->belongsTo('Product');
    }
}
