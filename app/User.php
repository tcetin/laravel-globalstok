<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['name', 'email'];

    public static $rulesImalatci = array(
        'vergino' => 'required|numeric|min:10|unique:users',
        'vergi_dairesi' => 'required',
        'unvan' => 'required',
        'city' => 'required',
        'city_town' => 'required',
        'adres' => 'required',
        'telefon' => 'required|min:9|numeric|unique:users',
        'fax'=> 'required|min:9|numeric|unique:users',
        'email' => 'required|email|unique:users',
        'yetkili' => 'required',
        'password' => 'required|alpha_num|between:8,12|confirmed',
        'password_confirmation' => 'required|alpha_num|between:8,12'
    );

    public static $rulesService = array(
        'vergino' => 'required|numeric|min:10|unique:users',
        'vergi_dairesi' => 'required',
        'unvan' => 'required',
        'city' => 'required',
        'city_town' => 'required',
        'adres' => 'required',
        'telefon' => 'required|min:9|numeric|unique:users',
        'fax'=> 'required|min:9|numeric|unique:users',
        'email' => 'required|email|unique:users',
        'yetkili' => 'required',
        'service_category'=>'required',
        'service_subcategory'=>'required',
        'password' => 'required|alpha_num|between:8,12|confirmed',
        'password_confirmation' => 'required|alpha_num|between:8,12'
    );

    public static $rulesTedarikci = array(
        'fullname'=>'required',
        'city' => 'required',
        'city_town' => 'required',
        'adres' => 'required',
        'telefon' => 'required|min:9|numeric|unique:users',
        'email' => 'required|email|unique:users',
        'password' => 'required|alpha_num|between:8,12|confirmed',
        'password_confirmation' => 'required|alpha_num|between:8,12'
    );

    public static $messages = array(
        'required' => ':attribute alanı boş bırakılamaz.',
        'numeric'  => ':attribute alanına sadece sayı girilebilir.',
        'between' => ':attribute alanına :min ile :max arasında karakter girişi yapılabilir.',
        'eposta' => ':attribute alanına lütfen geçerli bir eposta giriniz.',
        'alpha_num' => ':attribute alanına sadece alfanümerik karakterler girilebilir.',
        'confirmed' => 'Girilen şifreler birbiriyle uyuşmamaktadır.',
        'unique' => ':attribute daha önce kayıt olmuş.',
        'min' => ':attribute alanı en az :min karakter olmalıdır.',
        'max' => ':attribute alanına maximum :max karakter girilebilir'
    );

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function service_categories(){
        return $this->belongsToMany('App\ServiceCategory')->withTimestamps();
    }

    public function service_subcategories(){
        return $this->belongsToMany('App\ServiceSubCategory')->withTimestamps();
    }

}
