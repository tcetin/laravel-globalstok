<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AmountType extends Model
{
    protected $table = 'amount_types';

    protected $fillable = ['name'];
}
