<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceCategory extends Model
{
    protected  $table = "service_categories";

    protected $fillable = ['name'];

    public static $rules = array('name'=>'required|min:3|unique:service_categories');

    public static $messages = array(
        'required' => ':attribute alanı boş bırakılamaz.',
        'min' => ':attribute alanı en az :min karakterden oluşmalı.',
        'unique' => 'Bu kategori daha önce kaydedilmiş.'
    );

    public function serviceSubcategories()
    {

        return $this->hasMany('App\ServiceSubCategory');

    }

    public function users(){
        return $this->belongsToMany('App\User');
    }



}
