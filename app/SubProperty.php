<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubProperty extends Model
{
    protected $table = 'subproperties';

    protected $fillable = ['name','subcategory_id'];

    public static $rules = array('name'=>'required|min:3');

    public static $messages = array(
        'required' => ':attribute alanı boş bırakılamaz.',
        'min' => ':attribute alanı en az :min karakterden oluşmalı.'
    );

    public function subcategory()
    {
        $this->belongsTo('App\SubCategory');
    }

    public function product()
    {
        return $this->hasMany('Add\Product');
    }
}
