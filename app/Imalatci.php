<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imalatci extends Model
{
    protected  $table = 'imalatci';

    protected $fillable = array('vergino','unvan','city_code','adres','telefon','fax','eposta','web','verified');

    public static $rules = array(
        'vergino' => 'required|numeric|min:10|unique:imalatci',
        'unvan' => 'required',
        'city' => 'required',
        'adres' => 'required',
        'telefon' => 'required|min:9|numeric|unique:imalatci',
        'fax'=> 'required|min:9|numeric|unique:imalatci',
        'eposta' => 'required|email|unique:imalatci',
        'password' => 'required|alpha_num|between:8,12|confirmed',
        'password_confirmation' => 'required|alpha_num|between:8,12'
    );

    public static $messages = array(
        'required' => ':attribute alanı boş bırakılamaz.',
        'numeric'  => ':attribute alanına sadece sayı girilebilir.',
        'between' => ':attribute alanına :min ile :max arasında karakter girişi yapılabilir.',
        'eposta' => ':attribute alanına lütfen geçerli bir eposta giriniz.',
        'alpha_num' => ':attribute alanına sadece alfanümerik karakterler girilebilir.',
        'confirmed' => 'Girilen şifreler birbiriyle uyuşmamaktadır.',
        'unique' => ':attribute daha önce kayıt olmuş.',
        'min' => ':attribute alanı en az :min karakter olmalıdır.'
    );

    protected $hidden = array('password');

    public function products()
    {
        return $this->hasMany('Product');
    }

}
