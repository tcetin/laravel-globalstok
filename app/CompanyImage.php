<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyImage extends Model
{
    protected $table = "company_images";

    protected $fillable = ['imalatci_id','path','verified'];
}
