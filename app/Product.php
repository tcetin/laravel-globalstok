<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $table = "products";


    protected $fillable = ['category_id','subcategory_id','imalatci_id','subprop_id','title','description','price','currency_id','amount_type','amount','min_order','avaliability','verified','published'];


    public function category()
    {
        return $this->belongsTo('Category');
    }

    public function subcategory()
    {
        return $this->belongsTo('Subcategory');
    }

    public function imalatci()
    {
        return $this->belongsTo('User');
    }

    public function productImage(){
        $this->hasMany('ProductImage');
    }

    public function payment_methods(){
       return $this->belongsToMany('App\PaymentMethod')->withTimestamps();
    }

}
