<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mainslider extends Model
{
   protected $table = "mainslider";

   protected $fillable =['baslik','metin','image','read_more'];

   public static $rules = ['baslik'=>'required|min:5',
                           'metin'=>'required|min:10',
                           'image'=>'required|mimes:jpeg,jpg,gif,bmp,png',
                           'read_more'=>'required|min:3'];

    public static $messages = array(
            'required' => ':attribute alanı boş bırakılamaz',
            'min'   => ':attribute alanı en az :min karakterden oluşmalı.',
            'mimes' => 'Resim formatı jpeg,jpg,gif,bmp,png formatlarından biri olmalı.',

    );
}
