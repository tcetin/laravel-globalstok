<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected  $table = "categories";

    protected $fillable = ['name'];

    public static $rules = array('name'=>'required|min:3|unique:categories');

    public static $messages = array(
        'required' => ':attribute alanı boş bırakılamaz.',
        'min' => ':attribute alanı en az :min karakterden oluşmalı.',
        'unique' => 'Bu kategori daha önce kaydedilmiş.'
    );

    public function subcategories()

    {

        return $this->hasMany('App\SubCategory');

    }

    public function products()
    {
       return $this->hasMany('Product');
    }
}
