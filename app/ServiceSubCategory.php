<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceSubCategory extends Model
{
    protected  $table = "service_subcategories";

    protected $fillable = ['name','service_category_id'];

    public static $rules = array('name'=>'required|min:3');

    public static $messages = array(
        'required' => ':attribute alanı boş bırakılamaz.',
        'min' => ':attribute alanı en az :min karakterden oluşmalı.'
    );

    public function categories()
    {
        $this->belongsTo('App\ServiceCategory');
    }

    public function users(){
        return $this->belongsToMany('App\User');
    }


}
